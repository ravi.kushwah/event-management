<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Get all Product 
function stripeGetAllProduct($stripeSecretKey){
	require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);
    
	$allProduct = $stripe->products->all();
    // $allProduct =$stripe->prices->all();
	$d = json_decode(json_encode($allProduct),true);	
    $res = array('massege'=>$d);	
    return $res['massege']['data'];
}
function OnlyProductCreate($stripeSecretKey="",$pn="",$am="",$crr="",$des=""){	
 
    require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);
	$rs= $stripe->products->create([
		'name' => $pn
	]);
	$d = json_decode(json_encode($rs),true);
	return $d['id'];
}
//  Product Create New --
function stripeProductCreate($stripeSecretKey="",$pn="",$am="",$crr="",$des=""){	
 
    require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);
	$rs= $stripe->products->create([
		'name' => $pn,
		'description' =>$des
	]);
	$d = json_decode(json_encode($rs),true);
    /* response => [id] => prod_NhnNCFo3Vdz9n1 [object] => product [active] => 1 [attributes] => Array() [created] => 1681383228 [default_price] => [description] => [images] => Array() [livemode] => [metadata] => Array() [name] => ravi
        [package_dimensions] => [shippable] => [statement_descriptor] => [tax_code] => [type] => service [unit_label] => [updated] => 1681383228 [url] =>
    */
    $price_id = $stripe->prices->create(
		[
		  'product' =>$d['id'],
		  'unit_amount' => $am*100,
		  'currency' => $crr
		]
	  );
    $p = json_decode(json_encode($price_id),true);
    $res = array('priceData'=>$p,'productData'=>$d);
	return $res;	
}
//  Product Delete --
function stripeProductDelete($stripeSecretKey="",$npn){	
	require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);

	$allProduct = $stripe->products->all();

	$d = json_decode(json_encode($allProduct),true);	

	$isExist = false; 
	foreach($d['data'] as $v){
		if($v['name']==$npn){
			$isExist = true;
			$rs = $stripe->products->delete(
				$v['id'],
				[]
			);	
			$d = json_decode(json_encode($rs),true);
			$res = array('massege'=>$d);		
			break;
		}
	}	
    // response => [id] => prod_NhnI4lKAxykzHl [object] => product [deleted] => 1
	return $res;
}
// Product Price Create --
function stripePriceCreate($stripeSecretKey="",$pid="",$am="",$crr=""){    
	require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);
	
	$price_id = $stripe->prices->create(
		[
		  'product' =>$pid,
		  'unit_amount' => $am*100,
		  'currency' => $crr,    
		]
	  );
    $d = json_decode(json_encode($price_id),true);
    $res = array('massege'=>$d);
	return $res;
}
// Product & Price  Update --
function stripeProductUpdate($stripeSecretKey="",$pid="",$nam="",$crr=""){ // crr->Currency & nam-> New Amount

	require_once 'vendor/autoload.php';
	$stripe = new \Stripe\StripeClient($stripeSecretKey);
    $rs = $stripe->prices->create([
      'unit_amount' =>$nam*100,
      'currency' => $crr,
      'product' => $pid,
    ]);

	  $d = json_decode(json_encode($rs),true);
	return $d;
}
function priceRetrieve(){
    $stripe = new \Stripe\StripeClient(
          'sk_test_51F39j4Dn0Yc5TNXRktDp0b1CdEwNxiLIHg2e3jihOgrIKXUyNn8VzJKOdHS76RIbU8Hb4xnDbFIIfO0EJXoJook20054O5hd9t'
        );
       $res= $stripe->prices->retrieve(
          'price_1N0MIyDn0Yc5TNXROJXYAwvs',
          []
        );
    return $res;
}
// Payment IPN
function stripePayNow($stripeSecretKey="",$id=""){
	require_once 'vendor/autoload.php';
	require_once 'secrets.php';
	\Stripe\Stripe::setApiKey($stripeSecretKey);
	header('Content-Type: application/json');

	$YOUR_DOMAIN = base_url('pay-now/');

	$checkout_session = \Stripe\Checkout\Session::create([
	'line_items' => [[
		# Provide the exact Price ID (e.g. pr_1234) of the product you want to sell
		'price' => $id,
		'quantity' => 1,
	]],
	'mode' => 'payment',
// 	'receipt_email' => 'ravi.kushwah@pixelnx.com',
	'success_url' => $YOUR_DOMAIN . 'plan',
	'cancel_url' => $YOUR_DOMAIN . 'cancel',
	]);

	header("HTTP/1.1 303 See Other");

	header("Location: " . $checkout_session->url);
}
?>