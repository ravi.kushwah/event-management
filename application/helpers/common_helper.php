<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**Select Data */
function select_single_data($table, $where='',$column)
{
	  $CI = & get_instance();
	  $sql = "select $column from $table";
	   $sql .= " $where";
	    $query = $CI->db->query($sql);
	  if($query->num_rows()>0){
		 $result = $query->row();
		 return $result->$column;
	   }else{
		  return false; 
	  }
}
function checksubcriptiontime($subscriptiondate,$duration){
	$days_between = ceil(abs(time()- $subscriptiondate) / 86400);
	return $duration-$days_between+1;

}
function checksubcription(){
	$CI = & get_instance();
    $userid=$CI->session->userdata('eci_super_id');
    if($CI->session->userdata('user_type')==1){
    	$result=(object) array('eci_plan_type' =>2);
    	return $result;
    }
   
    $CI->db->select('*');
    $CI->db->from('eci_user_membership_plan as ump');
    $CI->db->join('eci_membership_plan as mp', 'ump.eci_membership_plan_id=mp.eci_plan_id');
    $CI->db->where('ump.eci_user_id',$userid);
    $CI->db->where('ump.eci_user_plan_status','Active');
    
    $query = $CI->db->get();
      
      if($query->num_rows()>0){
		 $result = $query->row();
		$remainingday=checksubcriptiontime($result->startdate,$result->eci_plan_duration);
        if($remainingday>0){
            return $result;
        }else{
        	return false;
        }
	   }else{
		  return false; 
	  }

	  
}
function get_collection($eventid,$mode=""){
	$CI = & get_instance();
	$CI->db->select('eci_payment_detail_msg');
	$CI->db->from('eci_payment_detail');
	$CI->db->where('eci_payment_detail_email',$eventid);
	$CI->db->where('eci_payment_booking_source',$mode);
	$CI->db->where('eci_payment_detail_type',4);
	$query = $CI->db->get();
	$total=0;
	 if($query->num_rows()>0){
	 	$payment=$query->result_array();
	foreach ($payment as $value) {
		$custom= json_decode($value['eci_payment_detail_msg'],true);
		
		$total=$total+$custom['mc_fee'];
	}
	 }
	 return $total;
}

function checkUserLogin(){
	$CI = & get_instance();
	$userId = $CI->session->userdata('eci_super_id');
	if($userId ==''){
		redirect(base_url().'eventadmin');
	}else{
		return $userId;
	}
	}
	function checkAdminOnly(){
	$CI = & get_instance();
	$userType = $CI->session->userdata('user_type');
	if($userType !=1){
		
		redirect(base_url().'eventadmin');
	}else{
		return $userType;
	}
	}
	function randomString($length = 6) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}

  function getemailmsg($type="",$data){
  	
     $bodyhead="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml'>
        <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Event Management</title>
        </head><body>";
       $body = '';
	       if($type=='registration'){
	       	$linktext=select_single_data("eci_website_setting","where eci_website_setting_name='registrationemail_linktext'","eci_website_setting_value");
	      $emContent =select_single_data("eci_website_setting","where eci_website_setting_name='registrationemail_text'","eci_website_setting_value");
	     $linkhref=base_url().'user/verification/'.$data['activationcode'];
	     $link = "<a href='".$linkhref."'>".$linktext."</a>";
	        $emContent = str_replace("[username]",$data['username'],$emContent);
	        $emContent = str_replace("[break]","<br/>",$emContent);
	        $emContent = str_replace("[linktext]",$link,$emContent);
	        $body .="<p>".$emContent."</p>";
	        return $bodyhead.$body;
	       }

	       if($type=='addplan'){
	       	
		      $emContent =select_single_data("eci_website_setting","where eci_website_setting_name='activeplanemail_text'","eci_website_setting_value");
		     
		        $emContent = str_replace("[username]",$data['username'],$emContent);
		        $emContent = str_replace("[plan_name]",$data['planname'],$emContent);
		        $emContent = str_replace("[plan_duration]",$data['planduration'],$emContent);
		        $emContent = str_replace("[plan_type]",$data['plantype'],$emContent);
				$emContent = str_replace("[plan_status]",$data['planstatus'],$emContent);
		        $emContent = str_replace("[break]","<br/>",$emContent);
		        $body .="<p>".$emContent."</p>";
		        return $bodyhead.$body;
	       }
	       if($type=='addnewuser'){
	       
	      $emContent =select_single_data("eci_website_setting","where eci_website_setting_name='adduseremail_text'","eci_website_setting_value");
	     $link = "<a href='".base_url()."'>Click Here</a>";
	        $emContent = str_replace("[useremail]",$data['useremail'],$emContent);
			$emContent = str_replace("[username]",$data['username'],$emContent);
	        $emContent = str_replace("[password]",$data['password'],$emContent);
	        $emContent = str_replace("[website_link]",$link,$emContent);
	        $emContent = str_replace("[break]","<br/>",$emContent);
	        
	        $body .="<p>".$emContent."</p>";
	        return $bodyhead.$body;
	       }

	       if($type=='forgetpassword'){
	       	$linktext=select_single_data("eci_website_setting","where eci_website_setting_name='forgotpwdemail_linktext'","eci_website_setting_value");
	      $emContent =select_single_data("eci_website_setting","where eci_website_setting_name='forgotpwdemail_text'","eci_website_setting_value");
	     $linkhref=base_url().'user/resetpassword/'.$data['resetcode'];
	     $link = "<a href='".$linkhref."'>".$linktext."</a>";
	        $emContent = str_replace("[username]",$data['username'],$emContent);
	        $emContent = str_replace("[break]","<br/>",$emContent);
	        $emContent = str_replace("[linktext]",$link,$emContent);
	        $body .="<p>".$emContent."</p>";
	        return $bodyhead.$body;
	       }

  }
  
  
  function get_eventname($event_name){
	  $CI = & get_instance();
	$CI->db->select('eci_event_list_name');
	$CI->db->from('eci_event_list');
	$CI->db->where('eci_event_list_name',$event_name);
	
	$query = $CI->db->get();
	$total=0;
	 if($query->num_rows()>0){
	 return 1;
	 }else{
		 return 0;
	 }
  }
?>