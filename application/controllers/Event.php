<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	function __Construct()
	{
        parent::__construct();
        error_reporting(E_ALL);
        $_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->load->helper(array('form', 'url','common'));
        $this->load->library(array('form_validation','session'));
        $this->load->model(array('Homemodel','Profilemodel','Common')); // database model
        $this->load->database();
        checkUserLogin();
	}

	function commision_event(){
	    if(isset($this->session->userdata['eci_super_login']))
		 {
		    $data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
	        if($this->session->userdata('user_type')==1)
	        {
	            $data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_membership_type'=>1));
	        }else{
		            $data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_user_id'=>$this->session->userdata('eci_super_id'),'eci_event_membership_type'=>1));
	             }
		$this->load->view('include/header');
		$this->load->view('admin/commisionevent',$data);
		$this->load->view('include/footer');
	     }
	    else
	    {
		    redirect(base_url().'eventadmin');
	    }
		}

    public function updatecommmisonevent($eventid=""){
	    checkAdminOnly();
        if($_POST)
        {
    		$data_arr   = array('status'=>$_POST['eci_commision_status']);
            $this->Common->update("eci_event_commision_status",$data_arr,array('eci_event_id' =>$eventid));
			redirect(base_url().'event/commision_event');
    	}else{
			$solo_commision_event = $this->Common->select('eci_event_commision_status',"where eci_event_id=$eventid");
			$data                 = $solo_commision_event[0];
            $this->load->view('include/header');
		    $this->load->view('admin/updatecommisionevent',$data);
		    $this->load->view('include/footer');
		}
	}

	function total_collection(){
    	if(isset($this->session->userdata['eci_super_login']))
		{
    		$userid=$this->session->userdata('eci_super_id');
	        if($this->session->userdata('user_type')==1)
	        {
	          $data['event_list'] = $this->Common->select('eci_event_list',$where ='','eci_event_list_sno,eci_event_list_name,eci_event_list_amount');
	        }else{
	          $data['event_list'] = $this->Common->select('eci_event_list',"where eci_event_user_id=$userid",'eci_event_list_sno,eci_event_list_name,eci_event_list_amount');
	             }
		$this->load->view('include/header');
		$this->load->view('admin/totalcollection',$data);
		$this->load->view('include/footer');
	    }
	    else
	    {
		  redirect(base_url().'eventadmin');
	    }
    }

    function add_plan(){
    	checkAdminOnly();
    	$data['msg']="";
    	if($_POST){
    		$planname       =  $this->input->post('eci_plan_name',true);
    		$plantype       =  $this->input->post('eci_plan_type',true);
    		$planprice      =  $this->input->post('eci_plan_price',true);
    		$planduration   =  18250;
    		$planstatus     =  $this->input->post('eci_plan_status',true);
    		if($plantype == 2){
    		    $planduration = $this->input->post('eci_plan_duration',true);
    		}
    		$data_arr = array("eci_plan_name"=>$planname,
                                "eci_plan_price"=>$planprice,
                                "eci_plan_duration"=>$planduration,
                                "eci_plan_type"=>$plantype,
                                "eci_plan_status"=>$planstatus);
    		$this->Common->data_insert('eci_membership_plan',$data_arr);
    		$data['msg'] = "New plan has been added";
    	}
    	$this->load->view('include/header');
		$this->load->view('admin/membershipplan/add_plan',$data);
		$this->load->view('include/footer');
    }

    function managemembershipplan(){
    	checkAdminOnly();
	    if(isset($this->session->userdata['eci_super_login']))
		{
			$data['plan_list'] = $this->Common->select('eci_membership_plan');
    		$this->load->view('include/header');
	    	$this->load->view('admin/membershipplan/managemembershipplan',$data);
		    $this->load->view('include/footer');
    	}
	    else
	    {
		  redirect(base_url().'eventadmin');
	    }
	}

    function updatemembershipplan($planid=""){
    	checkAdminOnly();
        if(isset($this->session->userdata['eci_super_login']))
        {
    	    if($_POST)
    	    {
    		            $planname     = $this->input->post('eci_plan_name',true);
                        $plantype     = $this->input->post('eci_plan_type',true);
    		            $planprice    = $this->input->post('eci_plan_price',true);
    		            $planduration = 18250;
    		            $planstatus   = $this->input->post('eci_plan_status',true);
    		if($plantype == 2){
    		    $planduration = $this->input->post('eci_plan_duration');
    		}
    		$data_arr = array("eci_plan_name"=>$planname,
                              "eci_plan_price"=>$planprice,
                              "eci_plan_duration"=>$planduration,
                              "eci_plan_type"=>$plantype,
                              "eci_plan_status"=>$planstatus);
            $this->Common->update("eci_membership_plan",$data_arr,array('eci_plan_id' =>$planid));
			redirect(base_url().'event/managemembershipplan');
    	    }else{
                    $data['solo_plan'] = $this->Common->select('eci_membership_plan',"where eci_plan_id=$planid");
                    $this->load->view('include/header');
                    $this->load->view('admin/membershipplan/updatemembershipplan',$data);
                    $this->load->view('include/footer');
    	         }
      }else{
		     redirect(base_url().'eventadmin');
	        }
    }

    public function deletemembershipplan($plan_id=""){
          if($plan_id!=""){
          	$this->Common->delete('eci_membership_plan',array('eci_plan_id'=>$plan_id));
          	redirect(base_url().'event/managemembershipplan');
          }else{
          	redirect(base_url().'event/managemembershipplan');
          }
	}

    function manageuserplan(){
		checkAdminOnly();
        $strSql                 = "SELECT DISTINCT * FROM `eci_user_membership_plan` WHERE eci_user_plan_id!=1";
        $result                 =  $this->db->query($strSql);
        $data['user_plan_list'] =  $result->result_array();
		$this->load->view('include/header');
		$this->load->view('admin/usermembershipplan/manageuserplan',$data);
		$this->load->view('include/footer');
	}

	function updateuserplan($userplanid=""){
		checkAdminOnly();
        if(isset($this->session->userdata['eci_super_login'])){
    	if($_POST){
             $planid    = $_POST['eci_user_plan_name'];
             $data_arr  = array(
                                'eci_membership_plan_id'=> $_POST['eci_user_plan_name'],
                                'eci_user_plan_status'=> $_POST['eci_user_plan_status'],
                                'startdate'=>time());
            $this->Common->update("eci_user_membership_plan",$data_arr,array('eci_user_plan_id' =>$userplanid));
            $userid     =   select_single_data("eci_user_membership_plan","where eci_user_plan_id=$userplanid","eci_user_id");
            $username   =   select_single_data("eci_admin","where eci_admin_sno=$userid","eci_admin_name");
            $email      =   select_single_data("eci_admin","where eci_admin_sno=$userid","eci_admin_email");
            $plandetail =   $this->Common->select("eci_membership_plan","where eci_plan_id=$planid");
            $planname   =   $plandetail[0]['eci_plan_name'];
            $plantype   =   'Time Based';
            if($plandetail[0]['eci_plan_type']==1){
            	$plantype='Commision';
            }
            $planduration = $plandetail[0]['eci_plan_duration'].' Days';
            $contact_sett = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			$cnt_data     = json_decode($contact_sett[0]['eci_contactus_data']);
			if($cnt_data->send_contact_query == 'no')
				$admin_email = $cnt_data->eci_contact_new_email;
				else
				$admin_email = $cnt_data->eci_contact_email;
				$planstatus  = $_POST['eci_user_plan_status'];
				$email_data  = array('username'=>$username,'planname'=>$planname,'plantype'=>$plantype,'planduration'=>$planduration,'planstatus'=>$planstatus);
				$message     = getemailmsg('addplan',$email_data);
				$headers     = "MIME-Version: 1.0" . "\r\n";
				$headers     .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers     .= 'From:' .$admin_email. "\r\n";
				mail($email,'Update Plan ',$message,$headers );
    			redirect(base_url().'event/manageuserplan');
    	}else{
                $userplan    = $this->Common->select('eci_user_membership_plan',"where eci_user_plan_id=$userplanid");
                $data        = $userplan[0];
                $data['plan']= $this->Common->select('eci_membership_plan');
                $this->load->view('include/header');
                $this->load->view('admin/usermembershipplan/updateuserplan',$data);
                $this->load->view('include/footer');
          }
      }else{
		redirect(base_url().'eventadmin');
	        }
    }

    function adduserplan(){
    	checkAdminOnly();
    	if($_POST){
    		$userid         =   $this->input->post('eci_user_email');
    		$planid         =   $this->input->post('eci_user_plan_name');
    		$checkuserplan  =   $this->Common->select('eci_user_membership_plan',"where eci_user_id=$userid","eci_user_plan_id");
    		$data_insert    = array("eci_user_id"=>$userid,
                                    "eci_membership_plan_id"=>$planid,
                                    "startdate"=>time());
    		if($checkuserplan){
    			$this->Common->update('eci_user_membership_plan',$data_insert,array("eci_user_id"=>$userid));
    			redirect(base_url().'event/manageuserplan');
    		}else{
    			$this->Common->data_insert('eci_user_membership_plan',$data_insert);
    			redirect(base_url().'event/manageuserplan');
    		}
    	}else{
    	    $data['userlist']   = $this->Common->select('eci_admin','','eci_admin_sno,eci_admin_email');
    	    $data['plan']       = $this->Common->select('eci_membership_plan');
    	    $this->load->view('include/header');
    	    $this->load->view('admin/usermembershipplan/adduserplan',$data);
    	    $this->load->view('include/footer');
            }
    }

    function userplanrequest(){
    	checkAdminOnly();
    	$data['user_plan_list'] = $this->Common->select('eci_user_membership_plan',"where eci_user_plan_status='Inactive'");
    	$this->load->view('include/header');
    	$this->load->view('admin/usermembershipplan/userplanrequest',$data);
    	$this->load->view('include/footer');
    }
	
	public function add_coupon(){
		checkAdminOnly();
		$data = array();
		if($_POST){
			        $coupon_code   = $_POST['eci_coupon_code'];
			        $where         = " where eci_coupon_code = '$coupon_code'";
			        $checkexitsing = $this->Common->select('eci_coupon',$where);
			if(empty($checkexitsing)){
				strtotime($_POST['eci_coupon_expir_date']);
				$data_insert = array(
                                        "eci_coupon_code"=>$_POST['eci_coupon_code'],
                                        "eci_coupon_name"=>$_POST['eci_coupon_name'],
                                        "eci_coupon_description"=>$_POST['eci_coupon_desc'],
                                        "eci_coupon_type"=>$_POST['eci_coupon_type'],
                                        "eci_coupon_amount"=>$_POST['eci_coupon_amount'],
                                        "eci_coupon_maximum_usages"=>$_POST['eci_coupon_usages_limit'],
                                        "eci_coupon_expire_date"=>$_POST['eci_coupon_expir_date'],
                                        "eci_coupon_status"=>$_POST['eci_coupon_status'],
                                        "eci_coupon_event" => implode(',',$_POST['eci_coupon_event'])
				                    );
				$this->Common->data_insert('eci_coupon',$data_insert);
				redirect(base_url().'event/manage_coupon');
		}else{
			$data['msg'] = "Coupon with this code is already exist please try again with other code";
		}
		}
		$data['eventList'] = $this->my_model->select_data('eci_event_list.eci_event_list_sno,eci_event_list.eci_event_list_name','eci_event_list',array('eci_event_list_status' => 1));
		$this->load->view('include/header');
		$this->load->view('discount_coupon/add_coupon',$data);
		$this->load->view('include/footer');
	}

	public function manage_coupon(){
		checkAdminOnly();
		$data['coupon_list'] = $this->Common->select('eci_coupon');
		$this->load->view('include/header');
		$this->load->view('discount_coupon/manage_coupon',$data);
		$this->load->view('include/footer');
	}
	
	public function edit_coupon($coupan_id=""){
		checkAdminOnly();
		if($coupan_id != ""){
			if($_POST){
              $data_update=array(	
								"eci_coupon_code"=>$_POST['eci_coupon_code'],
								"eci_coupon_name"=>$_POST['eci_coupon_name'],
								"eci_coupon_description"=>$_POST['eci_coupon_desc'],
								"eci_coupon_type"=>$_POST['eci_coupon_type'],
								"eci_coupon_amount"=>$_POST['eci_coupon_amount'],
								"eci_coupon_maximum_usages"=>$_POST['eci_coupon_usages_limit'],
								"eci_coupon_expire_date"=>$_POST['eci_coupon_expir_date'],
								"eci_coupon_status"=>$_POST['eci_coupon_status'],
								"eci_coupon_event" => implode(',',$_POST['eci_coupon_event'])
								);
		$this->Common->update("eci_coupon",$data_update,array('eci_coupon_id' =>$coupan_id));
		redirect(base_url().'event/manage_coupon');
			}else{
				$coupondata        = $this->Common->select('eci_coupon',"where eci_coupon_id=$coupan_id");
				$data['eventList'] = $this->my_model->select_data('eci_event_list.eci_event_list_sno,eci_event_list.eci_event_list_name','eci_event_list',array('eci_event_list_status'=>1));
				if($coupondata){
                    $data['coupan_detail']=$coupondata[0];
                    $this->load->view('include/header');
                    $this->load->view('discount_coupon/edit_coupon',$data);
                    $this->load->view('include/footer');
				}else{
					    redirect(base_url().'event/manage_coupon');
				     }
			}

		}else{
			   redirect(base_url().'event/manage_coupon');
		     }
	}

	public function coupon_detail($coupon_code=""){
		checkAdminOnly();
		if($coupon_code != ""){
				$coupondata    =   $this->Common->select('eci_coupon',"where eci_coupon_code='$coupon_code'");
				if($coupondata){
                                $data['coupan_detail'] =  $coupondata[0];
                                $this->load->view('include/header');
                                $this->load->view('discount_coupon/edit_coupon',$data);
                                $this->load->view('include/footer');
				               }else{
					                    redirect(base_url().'event/manage_coupon');
				                    }
		        }else{
			            redirect(base_url().'event/manage_coupon');
		             }
	}

	public function delete_coupon($coupon_id = ""){
          if($coupon_id != ""){
          	$this->Common->delete('eci_coupon',array('eci_coupon_id'=>$coupon_id));
          	redirect(base_url().'event/manage_coupon');
          }else{
          	redirect(base_url().'event/manage_coupon');
          }
	}
	public function delete_image($image_id=""){
		if($image_id != ""){
			$checkimage = $this->Common->select('eci_event_image_list',"where eci_event_image_list_sno=$image_id","eci_event_image_list_name");
			if($checkimage){
                            $imagename          = $checkimage[0]['eci_event_image_list_name'];
                            $checkfeatureimage  = $this->Common->select('eci_event_list',"where eci_event_list_img=$image_id","eci_event_list_sno");
			if($checkfeatureimage){
			    $data['msg'] = "You cannot delete image that is set as feature image for the event";
			}else{
					$this->Common->delete('eci_event_image_list',array('eci_event_image_list_sno'=>$image_id));
					 $file = './assets/back/eventimage/'.$imagename;
					 unlink($file);
					$data['msg'] = "Your Image has been deleted";
			}
		 }
		}
		$userid=$this->session->userdata('eci_super_id');
		$where="";
		
		if($this->session->userdata('user_type')==2){
			$where = "where eci_admin_sno='$userid'";
		}
		$data['images'] = $this->Common->select('eci_event_image_list',$where);
		  $this->load->view('include/header');
		  $this->load->view('admin/deleteimage',$data);
		  $this->load->view('include/footer');
	}

	public function paymentmode(){
	      $data = "";
		  $this->load->view('include/header');
		  $this->load->view('admin/paymentmode',$data);
		  $this->load->view('include/footer');
	}
}
