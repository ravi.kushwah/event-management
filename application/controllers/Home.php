<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __Construct()
	{
	parent::__construct();
	$_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	$_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$this->load->helper(array('form', 'url','text','common'));
	$this->load->library(array('form_validation','session'));
	$this->load->model(array('Homemodel','Common')); // database model 
	$this->load->database();
	}

	function index(){
		$data['mainevent']			= $this->Homemodel->event_tbl('select','',array('eci_event_list_featured'=>1));
		$data['service_list']		= $this->Homemodel->event_service_tbl('select','',array('eci_event_service_list_status'=>1));
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['background_imgs'] 	= $this->Homemodel->website_setting_tbl('select','bg','');
		$data['upcoming_event'] 	= $this->Homemodel->event_tbl('select','upcoming','',4,'no');
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$data['contact_bg_image']	= $this->Homemodel->get_image_name('sec_contact');
		$data['upcoming_bg_image']	= $this->Homemodel->get_image_name('sec_upcoming');
		$data['home_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/index',$data);
		$this->load->view('home/common/footer',$data);
	}
	
	function test(){
		echo "jgfejg";
		$data	= '{"mc_gross":"200.00","protection_eligibility":"Ineligible","address_status":"confirmed","payer_id":"HBCQKADFVN6BC","address_street":"1 Main St","payment_date":"03:30:54 Aug 16, 2018 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","first_name":"shabaj","address_country_code":"US","address_name":"shabaj shah","notify_version":"3.9","custom":"{\"for\":\"event\",\"personname\":\"[\\\"abc rajputygrt\\\",\\\"abc rajputdthtf\\\",\\\"abc rajputjfdsyj\\\",\\\"abc rajputyj\\\"]\",\"coupon_code\":\"\",\"book_seat\":\"{\\\"Golden - 2019\\\":2,\\\"Silver - 2019\\\":2}\"}","payer_status":"verified","address_country":"United States","address_city":"San Jose","quantity":"1","verify_sign":"AS0U8jiKEDx2lJOGj2e8G6FIXni1AtFLYj.XHIQEAs.ja4PPqV.ZTSM3","payer_email":"shahbaj.shah@himanshusofttech.com","txn_id":"00933354JA2785729","payment_type":"instant","last_name":"shah","address_state":"CA","receiver_email":"syinfotech333@gmail.com","pending_reason":"unilateral","txn_type":"web_accept","item_name":"ev5ent4","mc_currency":"USD","item_number":"","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"200.00","ipn_track_id":"9c094af24a7c3"}';
		$data1  = json_decode($data , true);
		$custom = json_decode($_POST['custom'], true);
		echo "<pre>";
		print_r($data1);
		print_r($custom);
	}
	
	function event_detail($event_uniqid=''){
		if($event_uniqid==''){
			redirect(base_url());
		}
		$data['contactus_setting'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['background_imgs']	= $this->Homemodel->website_setting_tbl('select','bg','');
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$data['solo_event']			= $this->Homemodel->event_tbl('select','',array('eci_event_list_uniqid'=>$event_uniqid));
		
		$data['eci_admin']			= $this->db_model->select_data('eci_admin_name,','eci_admin',array('eci_admin_sno'=>$data['solo_event'][0]['eci_event_user_id']),'','','','','','');
		
		$data['event_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/event_detail',$data);
		$this->load->view('home/common/footer',$data);
	}
	
	 function events(){
		$this->load->library("pagination");
		$config = array();
	    $config["base_url"] = base_url() . "Home/events";
        $config['total_rows']      = $this->Homemodel->countAll('eci_event_list','','','','','','');
        $config['per_page']        = 5;
        $config["full_tag_open"]   = '<ul class="pagination">';
        $config["full_tag_close"]  = '</ul>';
        $config["first_tag_open"]  = '<li class="page-item page-link ">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"]   = '<li class="page-item page-link">';
        $config["last_tag_close"]  = '</li>';
        $config["next_tag_open"]   = '<li  class="page-item "><span aria-hidden="true"">';
        $config["next_tag_close"]  = '</span></li>';
        $config["prev_tag_open"]   = '<li  class="page-item"> <span aria-hidden="true" ">';
        $config["prev_tag_close"]  = '</span></li>';
        $config["num_tag_open"]    = '<li  class="page-item ">';
        $config["num_tag_close"]   = '</li>';
        $config["cur_tag_open"]    = '<li  class="page-item active" > <a>';
        $config["cur_tag_close"]   = '</a></li>';
        $config['first_link']      = "Previous";
        $config['last_link']       = "Next";
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();

		$data['upcoming_event']     = $this->Homemodel->get_users('select','','',$config["per_page"],$page,'');
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['background_imgs']   	= $this->Homemodel->website_setting_tbl('select','bg','');
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$data['event_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/events',$data);
		$this->load->view('home/common/footer',$data);	  


		// $data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		// $data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		// $data['background_imgs']   	= $this->Homemodel->website_setting_tbl('select','bg','');
		// $data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		// $data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
       	// $data['upcoming_event'] 	= $this->Homemodel->event_tbl('select','','');
	   	// $data['event_active']		= 1;
		// $this->load->view('home/common/header',$data);
		// $this->load->view('home/events',$data);
		// $this->load->view('home/common/footer',$data);	  
	}
	 function Add_events(){
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['background_imgs']   	= $this->Homemodel->website_setting_tbl('select','bg','');
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
       	$data['upcoming_event'] 	= $this->Homemodel->event_tbl('select','','');
	   	$data['add_event_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/Add_event',$data);
		$this->load->view('home/common/footer',$data);	  
	}
	
	function past_event(){
		
        $newDate = date('m/d/Y'); 
		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = base_url() . "Home/past_event";
	    $config["total_rows"] = $this->Homemodel->countAll('eci_event_list',array('eci_event_list_evnt_date < '=>$newDate),'','','','','');
		// echo $this->db->last_query();die;
        $config['per_page']        = 5;
        $config["full_tag_open"]   = '<ul class="pagination">';
        $config["full_tag_close"]  = '</ul>';
        $config["first_tag_open"]  = '<li class="page-item page-link">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"]   = '<li class="page-item page-link">';
        $config["last_tag_close"]  = '</li>';
        $config["next_tag_open"]   = '<li class="page-item"><span aria-hidden="true" style="display: none;">';
        $config["next_tag_close"]  = '</span></li>';
        $config["prev_tag_open"]   = '<li class="page-item"> <span aria-hidden="true" style="display: none;">';
        $config["prev_tag_close"]  = '</span></li>';
        $config["num_tag_open"]    = '<li class="page-item ">';
        $config["num_tag_close"]   = '</li>';
        $config["cur_tag_open"]    = '<li class="page-item active"> <a>';
        $config["cur_tag_close"]   = '</a></li>';
        $config['first_link']      = "Previous";
        $config['last_link']       = "Next";
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$data['contactus_setting'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
       	$data['upcoming_event'] = $this->Homemodel->get_users('select','recent','',$config["per_page"], $page);

		// $data['contactus_setting'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		// $data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		// $data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		// $data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
       	// $data['upcoming_event'] 	= $this->Homemodel->event_tbl('select','recent','');
	   	$data['event_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/events',$data);
		$this->load->view('home/common/footer',$data);	  
	}

	function upcoming(){
		$newDate = date('m/d/Y'); 
	   $this->load->library("pagination");
	   $config = array();
	   $config["base_url"] = base_url() . "Home/upcoming";
	  
	    $date = new DateTime();
        $timestamp = $date->getTimestamp();
	    $config["total_rows"] = $this->Homemodel->countAll('eci_event_list',array('eci_event_list_evnt_date >='=>$newDate,'eci_event_list_upcoming !='=>0),'','','','','');
		
	    $config['per_page']        = 5;
        $config["full_tag_open"]   = '<ul class="pagination">';
        $config["full_tag_close"]  = '</ul>';
        $config["first_tag_open"]  = '<li class="page-item page-link">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"]   = '<li class="page-item page-link">';
        $config["last_tag_close"]  = '</li>';
        $config["next_tag_open"]   = '<li class="page-item"><span aria-hidden="true" style="display: none;">';
        $config["next_tag_close"]  = '</span></li>';
        $config["prev_tag_open"]   = '<li class="page-item"> <span aria-hidden="true" style="display: none;">';
        $config["prev_tag_close"]  = '</span></li>';
        $config["num_tag_open"]    = '<li class="page-item ">';
        $config["num_tag_close"]   = '</li>';
        $config["cur_tag_open"]    = '<li class="page-item active"> <a>';
        $config["cur_tag_close"]   = '</a></li>';
        $config['first_link']      = "Previous";
        $config['last_link']       = "Next";
	   $this->pagination->initialize($config);
	   $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
	   $data["links"] = $this->pagination->create_links();
	//    print_r($config["total_rows"]);
	//    die;
	   $data['contactus_setting'] 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
	   $data['social_setting']			= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
	   $data['background_imgs'] 		= $this->Homemodel->website_setting_tbl('select','bg','');
	   $data['payment_detail'] 		= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
	   $data['seo_setting'] 			= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
	   $data['upcoming_event'] = $this->Homemodel->get_users('select','upcoming','',$config["per_page"], $page);

		// $data['contactus_setting'] 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		// $data['social_setting']			= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		// $data['background_imgs'] 		= $this->Homemodel->website_setting_tbl('select','bg','');
		// $data['payment_detail'] 		= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
		// $data['seo_setting'] 			= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
       	// $data['upcoming_event'] 		= $this->Homemodel->event_tbl('select','upcoming','');

	   	$data['event_active']			= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/events',$data);
		$this->load->view('home/common/footer',$data);	  
	}

	function contactus(){
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['contact_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('home/contactus',$data);
		$this->load->view('home/common/footer',$data);	  
	}
	//["Golden - 2019,3000,60","Silver - 2019,5000,40"]
	function get_event_details(){
		$result 		  = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['eventid']));
		$result_booking   = $this->Homemodel->event_payment_detail_tbl('select','sum_booking',array('eci_payment_detail_email'=>$_POST['eventid']));
		$userpaypalemail  = $this->Homemodel->getuserpaypalemail($_POST['eventid']);
		$str['tableData'] = '';
		if($result[0]['eci_event_list_categories'] !='null' && $result[0]['eci_event_list_max_user'] == 0){
			$arr 		= json_decode($result[0]['eci_event_list_catwise_seat_left']);
			$total_Tic  = json_decode($result[0]['eci_event_list_categories']);
			$record=array();
			$row='';
			$cnt=0;
			$row .= '<table class="detail_table"><tr>
							<th>Category</th>
							<th>Left Seats</th>
							<th>Cost/Person</th>
							<th>How Many Seats?</th>
						</tr>';
			for($i=0;$i<count($arr);$i++){
				$cnt++;
				$record = explode(',',$arr[$i]);
				$total = explode(',',$total_Tic[$i]);
					if(isset($record[1]) && $record[1] != 0){
						$seat = $record[1];
						$seat_input = '<td data-target="'.$total[1].'"><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="" data-counter="'.$cnt.'" ></td>';
					}else{
						$seat='All Tickets are Sold';
						$seat_input = '<td data-target="'.$total[1].'"><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="" data-counter="'.$cnt.'"  Disabled></td>';
					}
					$row .= '<tr>
							<td class="cat_name'.$cnt.'">'.$record[0].'</td>
							<td class="cat_left_seat'.$cnt.'">'.$seat.'</td>
							<td class="cat_tic_price'.$cnt.'">'.$record[2].'</td>
							'.$seat_input.'
						</tr>';
			} 
			$row .='</table>'; 
			$str['tableData'] = $row;
		}
		 echo json_encode(array_merge($result,$result_booking,$userpaypalemail,$str));
	}
	public function EventSearch(){
	   
        if(isset($_POST['value'])){
            $newDate = date('m/d/Y'); 
		   
            $like = array('eci_event_list_name',$_POST['value']);
			$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
			// array('eci_payment_detail','eci_payment_detail.eci_payment_detail_sno=eci_event_list.eci_event_list_sno','left')
            $upcoming_event= $this->db_model->select_data('*','eci_event_list','','','',$like,'','','');
			
            if(!empty($upcoming_event)){
                $resp = array('status'=>1,'data'=>$upcoming_event);
            }else{
                $resp = array('status'=>0,'data'=>$data); 
            }
            
        }else{
            $resp = array('status'=>0,'data'=>''); 
        }
        echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	}
	function SendMail($from,$to,$subject,$body){
	    $smtp_encryption = 'tsl';
        $smtp_port = '587';
        $smtp_user = 'omprakash.jha@himanshusofttech.com';
        $smtp_password = 'GNmf3zKrOBHtdJ17';
        $smtp_server  = 'Smtp-relay.sendinblue.com';
        
        $this->load->library('email'); // load library 
        
         $config = array(
            'protocol' => 'SMTP',
    		'smtp_host' => $smtp_server,
    		'smtp_port' => $smtp_port,
    		'smtp_user' => $smtp_user,
    		'smtp_pass' => $smtp_password,
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'newline'   =>  "\r\n",
            'smtp_crypto'   =>  $smtp_encryption,
            // 'SMTPAuth'   => true,
        );
        $this->email->initialize($config);
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($body);
        if(! $this->email->send()){
    		$c =  $this->email->print_debugger();
    	    return $ress = array('status'=>1,'message'=>$c);
    	}else{
	    	return $ress = array('status'=>1,'message'=>'Mail has been sent successfully.');
    	}
    
	}
// 	function send_contact_query(){
// 		$em_name	= $_POST['em_name'];
// 		$em_email	= $_POST['em_email'];
// 		$em_subject = $_POST['em_subject'];
// 		$em_msg		= $_POST['em_msg'];
// 		if(trim($em_name)!="" && trim($em_email)!="" && trim($em_subject)!="" && trim($em_msg)!="")
// 		{
// 			if(filter_var($em_email, FILTER_VALIDATE_EMAIL))
// 			{
// 				$contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
// 				$cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data']);
// 				if($cnt_data->send_contact_query == 'no')
// 					$admin_email	= $cnt_data->eci_contact_new_email;
// 				else
// 					$admin_email	= $cnt_data->eci_contact_email;
// 					$message		= "Hi Admin<p>".$em_name." has sent a query having email id as ".$em_email." </p><p>Subject is : ".$em_subject."</p><p>Message is : ".$em_msg."</p>";
// 					$headers 		= "MIME-Version: 1.0" . "\r\n";
// 					$headers 		.= "Content-type:text/html;charset=UTF-8" . "\r\n";
// 					$headers 		.= 'From: <support@eventmanagement.com>' . "\r\n";
// 				if(mail($admin_email,'Query for ',$message,$headers ))
// 				{			
// 				echo '1#<p class="font-color-green">Mail has been sent successfully.</p>';
// 				}
// 				else
// 				{
// 				echo '2#<p class="font-color-red">Please Try Again.</p>';
// 				}
// 				if($cnt_data->save_data_db == '1')
// 				{
// 					$data_arr=array(
// 					'users_name'	=> $_POST['em_name'],
// 					'users_email'	=> $_POST['em_email'],
// 					'users_subject'	=> $_POST['em_subject'],
// 					'users_message'	=> $_POST['em_msg'],
// 					'date'	=> date('d-m-Y'),
// 					);
// 					$this->Homemodel->event_contactus_tbl('insert',array('eci_contactus_sno'=>'','eci_contactus_data'=>json_encode($data_arr)),'');
// 				}
// 			}
// 			else
// 				echo '2#<p class="font-color-red">Please provide valid email.</p>';
// 		}
// 		else
// 		{
// 			echo '2#<p class="font-color-red">Please fill all the details.</p>';
// 		}
// 	}
	function send_contact_query(){
	
		$em_name	= $_POST['em_name'];
		$em_email	= $_POST['em_email'];
		$em_subject = $_POST['em_subject'];
		$em_msg		= $_POST['em_msg'];
		if(trim($em_name)!="" && trim($em_email)!="" && trim($em_subject)!="" && trim($em_msg)!="")
		{
			if(filter_var($em_email, FILTER_VALIDATE_EMAIL))
			{
				$contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
				$cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data']);
				if($cnt_data->send_contact_query == 'no')
					$admin_email	= $cnt_data->eci_contact_new_email;
				else
					$admin_email	= $cnt_data->eci_contact_email;
					$message		= "Hi Admin<p>".$em_name." has sent a query having email id as ".$em_email." </p><p>Subject is : ".$em_subject."</p><p>Message is : ".$em_msg."</p>";
					$headers 		= "MIME-Version: 1.0" . "\r\n";
					$headers 		.= "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers 		.= 'From: <support@eventmanagement.com>' . "\r\n";

				$res = $this->SendMail($admin_email,$em_email,'Query for ',$message);	
				if($res)
				{			
				echo json_encode(array('success'=>1,'sms'=>'Mail has been sent successfully.'));
				}
				else
				{
                    echo json_encode(array('success'=>2,'sms'=>'<p class="font-color-red">Please Try Again.</p>'));
				// echo '2#<p class="font-color-red">Please Try Again.</p>';
				}
				if($cnt_data->save_data_db == '1')
				{
					$data_arr=array(
					'users_name'	=> $_POST['em_name'],
					'users_email'	=> $_POST['em_email'],
					'users_subject'	=> $_POST['em_subject'],
					'users_message'	=> $_POST['em_msg'],
					'date'	=> date('d-m-Y'),
					);
					$this->Homemodel->event_contactus_tbl('insert',array('eci_contactus_sno'=>'','eci_contactus_data'=>json_encode($data_arr)),'');
				}
			}
			else
             echo json_encode(array('success'=>2,'sms'=>'Please provide valid email.'));
				// echo '2#<p class="font-color-red">Please provide valid email.</p>';
		}
		else
		{
            echo json_encode(array('success'=>2,'sms'=>'Please fill all the details.'));
			// echo '2#<p class="font-color-red">Please fill all the details.</p>';
		}
	}
	function thanks($for=""){
		$data['seo_setting'] 		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['payment_detail'] 	= $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>1));
		if($for == "plan"){
			$data['payment_detail'] = array(array('eci_payment_detail_msg'=>'Thank you for purchasing plan with us.All the detail wiil be sent to your email shortly'));
		}
		if($for == "cancel"){
			$data['payment_detail'] = array(array('eci_payment_detail_msg'=>"You have canceled the payment, but don't worry the event's tickets are still in stock. You can purchase them any time you want."));
		}
		$this->load->view('home/common/header',$data);
		$this->load->view('home/thanks',$data);
		$this->load->view('home/common/footer',$data);	  
	}
	
	function save_info_invoice()
	{
		
		if(isset($_POST['eci_pay_paypal']))
		{
			if(isset($_POST['em_invoice_name']))
				$cust_name  = $_POST['em_invoice_name'];
			else
				$cust_name = '';
			
			if(isset($_POST['em_invoice_email']))
				$cust_email = $_POST['em_invoice_email'];
			else
				$cust_email = $_POST['em_invoice_email_free'];
			
			if(isset($_POST['em_invoice_cntct']))
				$cust_cntct = $_POST['em_invoice_cntct'];
			else
				$cust_cntct = '';
			
			if(isset($_POST['em_invoice_add']))
				$cust_add = $_POST['em_invoice_add'];
			else
				$cust_add = '';
			$ipn_arr=array(
							'payment_date'		=> date('H:i:s M d, Y'),
							'first_name'		=> $cust_name,
							'mc_fee'			=> $_POST['event_amnt'],
							'payer_email'		=> $cust_email,
							'txn_id'			=> 'Offline',
							'manual_cntc_no'	=> $cust_cntct,
							'manual_address'	=> $cust_add,
						);
			$eventid 		= $_POST['em_invoice_eventid'];
			$cat_seat_left  = $_POST['em_invoice_cat_seat_left'];
			$uniq_tckt_no 	= 'EV'.date('idsm').rand();
			$data_arr		= array(
										'eci_payment_detail_email'			=> $_POST['em_invoice_eventid'],
										'eci_payment_detail_ccode'			=> $_POST['em_invoice_ticketcount'],
										'eci_payment_detail_msg'			=> json_encode($ipn_arr),
										'eci_payment_detail_em_temp'		=> 'user',
										'eci_payment_detail_type'			=> 2,
										'eci_payment_detail_tckt_no'		=> $uniq_tckt_no,
										'eci_payment_booking_source'		=> 'Offline',
										'eci_payment_coupon_code'			=> $_POST['em_invoice_coupon_code'],
										'eci_payment_detail_persons_name'	=> json_encode($_POST['person']),
										'eci_booked_tickets_cate'			=> $_POST['em_invoice_cat_seat_left']
			);
			// $event_uniq_arr2[0] -- event code
			// $event_uniq_arr2[1] -- ticket count
			$this->Homemodel->event_payment_detail_tbl('insert',$data_arr,'');
			if($cat_seat_left !='null'){
				$event_detail		= $this->Common->select('eci_event_list',"where eci_event_list_sno='$eventid'",' eci_event_list_catwise_seat_left');
				$total_leftseat 	= json_decode($event_detail[0]['eci_event_list_catwise_seat_left']);
				$arry_str 			= array();
				$json_de  			= json_decode($cat_seat_left);
				for($i=0; $i<sizeof($total_leftseat); $i++){
					$totalseat_left_arr[$i] = explode(',',$total_leftseat[$i]);
					$booked_seat_arr[$i]	= explode(',',$json_de[$i]);
					if($totalseat_left_arr[$i][0] 	= $booked_seat_arr[$i][0]){
						$totalseat_left_arr[$i][1] 	= $totalseat_left_arr[$i][1] - $booked_seat_arr[$i][1];
					}
					$arry_str[$i] =	$totalseat_left_arr[$i][0].','.$totalseat_left_arr[$i][1].','.$totalseat_left_arr[$i][2];
				}
				$json_data 		= json_encode($arry_str);
				$updatedata  	= array('eci_event_list_catwise_seat_left'=>$json_data);
				$this->my_model->update_data('eci_event_list',$updatedata,array('eci_event_list_sno'=>$eventid));
			}
			//SEND MAIL 
			if($_POST['em_invoice_sendemail'] == 1 || isset($_POST['em_invoice_email_free'] ) && $cust_email != '')
			{
				$event_data 		= $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['em_invoice_eventid']));
				$contactus_setting  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
				$cnt_data 			= json_decode($contactus_setting[0]['eci_contactus_data']);
				$paysetting 		= $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>1));
				$email_template 	= $paysetting[0]['eci_payment_detail_em_temp'];
				$email_template		= str_replace("[cus_name]", $cust_name, $email_template);
				$email_template 	= str_replace("[event_name]", $event_data[0]['eci_event_list_name'], $email_template);
				$email_template 	= str_replace("[no_tckt]", $_POST['em_invoice_ticketcount'], $email_template);
				$email_template 	= str_replace("[break]", '<br>', $email_template);
				$email_template 	= str_replace("[uniq_tckt]",$uniq_tckt_no, $email_template);
				$em_msg				= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> ".$event_data[0]['eci_event_list_name']."</title></head><body><p>".$email_template."</p></body></html>";
				$from = 'support@eventmanagement.com';
				$res = $this->SendMail($from,$cust_email,'Event booking by customer email copy',$em_msg);	
				// if($cnt_data->eci_contact_email != '')
				// 	$from = $cnt_data->eci_contact_email;
				// else
				// 	$from = 'support@eventmanagement.com';
				// $headers = 'From: '.$from.'  '."\r\n";
				// $headers .= 'MIME-Version: 1.0' . "\r\n";
				// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// $to = $cust_email;
				// //$to = 'vivek.tiwari@himanshusofttech.com';
				// $subject = 'Thanks for booking ticket for '.$event_data[0]['eci_event_list_name'];
				// mail($to,$subject,$em_msg,$headers);
				// mail($from,'Event booking by customer email copy',$em_msg,$headers);
				redirect(base_url().'home/thanks/plan');
			}
		}
		else
			redirect(base_url());
		}
	function test123(){
		$skey = 'sk_test_51F39j4Dn0Yc5TNXRktDp0b1CdEwNxiLIHg2e3jihOgrIKXUyNn8VzJKOdHS76RIbU8Hb4xnDbFIIfO0EJXoJook20054O5hd9t';
		// $allPro = stripeGetAllProduct($skey);
		// $priceId = "";
		// foreach ($allPro as $key => $value) {
		// 		if($value['name']=='tree'){
		// 			$priceId .=$value['id'];
		// 			break;
		// 		}
		// }
		// $res =  stripeProductUpdate($skey,$priceId,'tree',151,'INR');
		// $res = stripePriceCreate($skey,$priceId,50,'USD');
		// echo "<pre>";print_r($res);
		// die;
		// $priceId = stripeProductCreate('testing',1500,'INR');	
		stripePayNow($skey ,'price_1MwiMMDn0Yc5TNXRhXVao8cm');
	}
}

?>
