<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __Construct()
	{
        parent::__construct();
        error_reporting(E_ALL);
        $_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->load->helper(array('form','url','common'));
        $this->load->library(array('form_validation','session'));
        $this->load->model(array('Homemodel','Profilemodel','Common')); // database model
        $this->load->database();
        checkUserLogin();
	}
   
   function index(){
   	 $data['user_detail']   = $this->Profilemodel->getuserdetail();
   	 $data['msg']           = "";
   	 $this->load->view('include/header');
     $this->load->view('user/profile',$data);
     $this->load->view('include/footer');
   }

   public function editprofile(){
    if($_POST){
      $main_data=array("eci_admin_name"=>$this->input->post('eci_admin_name'));
      $address = str_replace("\r\n",'<br>',$_POST['eci_admin_address']);
      $add_data=array(
                        "eci_admin_address"         => $address,
                         "eci_admin_contact"        => $this->input->post('eci_admin_contact'),
                         "eci_admin_gender"         => $this->input->post('eci_admin_gender')
                          );
        $this->Common->update("eci_admin_additional_info",$add_data,array('eci_admin_sno'=>$this->session->userdata('eci_super_id')));
        $this->Common->update("eci_admin",$main_data,array('eci_admin_sno'=>$this->session->userdata('eci_super_id')));
        }
        $data['user_detail']=$this->Profilemodel->getuserdetail();
   	    $this->load->view('include/header');
   	    $this->load->view('user/editprofile',$data);
   	    $this->load->view('include/footer');
   }

   public function changepassword(){
	  $userid                   = $this->session->userdata('eci_super_id');
	  $passChangeFormValidation = $this->__setFormRules('changePassword');
	  if ($passChangeFormValidation) {
       $eci_old_password=md5($this->input->post('eci_old_password'));
       $userpassword=select_single_data('eci_admin',"where eci_admin_sno='$userid'",'eci_admin_pwd');
	  if($eci_old_password==$userpassword){
       $data    =   array("eci_admin_pwd"=>md5($this->input->post('eci_new_password')));
       $this->Common->update("eci_admin",$data,array('eci_admin_sno'=>$this->session->userdata('eci_super_id')));
              $data['msg']  = "Password change  successfully";
              $data['class']= "alert alert-success";
      }else{
        $data['msg']    = "Please enter correct old password";
		$data['class']  = "alert alert-danger";
      }
    }
    $data['user_detail'] =  $this->Profilemodel->getuserdetail();
    $this->load->view('include/header');
   	$this->load->view('user/changepassword',$data);
   	$this->load->view('include/footer');
   }
 
    public function editprofilepicture(){
	    $check = getimagesize($_FILES["userfile"]["tmp_name"]);
        if($check==FALSE){
            return false;
        }
        $userid=$this->session->userdata('eci_super_id');
        $config['upload_path']   = './assets/front/images/profile/';
	    $config['allowed_types'] = '*';
        $config['max_size']      = '0';
        $userimage               = $this->Common->select('eci_admin_additional_info',"where eci_admin_sno=$userid",'eci_admin_image');
        $currentprofile          = $userimage[0]['eci_admin_image'];
        $this->load->library('upload', $config);
        if ($this->upload->do_upload())
        {
        $uploaddata =   $this->upload->data();
        $img_name   =   $uploaddata['raw_name'];
        if($currentprofile){
         $file  =   './assets/front/images/profile/'.$currentprofile;
           unlink($file);
         }
         }
        else
         $img_name='';
         $this->upload->display_errors();
        if($img_name != '')
        {
            $imgdbname  =   $img_name.date('ymdHis').$uploaddata['file_ext'];
            $data       =   array("eci_admin_image"=>$imgdbname);
            $this->Common->update("eci_admin_additional_info",$data,array('eci_admin_sno'=>$this->session->userdata('eci_super_id')));
            $oldfile    =   FCPATH.'assets/front/images/profile/'.$img_name.$uploaddata['file_ext'];
            $newfile    =   FCPATH.'assets/front/images/profile/'.$img_name.date('ymdHis').$uploaddata['file_ext'];
            rename($oldfile,$newfile);
            $src        =   base_url().'assets/front/images/profile/'.$imgdbname;
            echo $src;
        }else{
          return false;
            }
    }

    function plan(){
          $currency                 =   $this->Common->select('eci_payment_detail',"","eci_payment_detail_ccode");
          $data['currency']         =   $currency[0];
          $data['commisionplans']   =   $this->Common->select('eci_membership_plan',"where eci_plan_type=1 and eci_plan_status=1");
          $data['timeplans']        =   $this->Common->select('eci_membership_plan',"where eci_plan_type=2 and eci_plan_status=1");
          $this->load->view('include/header');
          $this->load->view('admin/usermembershipplan/plan',$data);
          $this->load->view('include/footer');
   }

   function orderplan($planid=""){
     if($planid!=""){
         if($_POST){
                     $userid       =   $this->session->userdata("eci_super_id");
                     $usercontact  =   select_single_data("eci_admin_additional_info","where eci_admin_sno=$userid","eci_admin_contact");
         if($usercontact!="")
         {
         $data_insert=array(
                            "eci_user_id"               => $userid,
                            "eci_membership_plan_id"    => $planid,
                            "eci_user_plan_status"      => "Inactive",
                            "eci_user_plan_contact"     => $usercontact);
         $checkuserplan=$this->Common->select('eci_user_membership_plan',"where eci_user_id=$userid","eci_user_plan_id");
         if($checkuserplan){
           $this->Common->update("eci_user_membership_plan",$data_insert,array('eci_user_id'=>$userid));
           $data['msg']="Thank you for upgrading plan.Our represantative will contact you within 24 hour";
            }else{
            $this->Common->data_insert("eci_user_membership_plan",$data_insert);
            $data['msg']="Thank you for purchasing plan.Our represantative will contact you within 24 hour";
                }
         }
         else{
           redirect(base_url().'profile/orderplan/'.$planid);
            }
            $this->load->view('include/header');
            $this->load->view('admin/usermembershipplan/plansuccess',$data);
         $this->load->view('include/footer');
         }else{
        $data['plandetail']     = $this->Common->select('eci_membership_plan',"where eci_plan_id=$planid");
        $data['payment_detail'] = $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1'));
        $this->load->view('include/header');
        $this->load->view('admin/usermembershipplan/orderplan',$data);
        $this->load->view('include/footer');
            }
        }else{
            redirect(base_url().'profile/plan');
            }
    }

    function ordercommisionplan($planid=""){
        if($planid!=""){
        $plantype       =   $this->Common->select('eci_membership_plan',"where eci_plan_id=$planid","eci_plan_type");
        $plantype       =   $plantype[0]['eci_plan_type'];
        if($plantype==1){
            $userid         =   $this->session->userdata("eci_super_id");
            $data_insert    =   array(
                                    "eci_user_id"            =>$userid,
                                    "eci_membership_plan_id" =>$planid,
                                    "eci_user_plan_status"   =>"Active",
                                    "startdate"              =>time()
                                    );
            $checkuserplan = $this->Common->select('eci_user_membership_plan',"where eci_user_id=$userid","eci_user_plan_id");
            if($checkuserplan){
                $this->Common->update("eci_user_membership_plan",$data_insert,array('eci_user_id'=>$userid));
                $data['msg']="Thank you for upgrading commision plan.";
            }else{
             $this->Common->data_insert("eci_user_membership_plan",$data_insert);
             $data['msg']="Thank you for purchasing commision plan.";
                }
            $this->load->view('include/header');
            $this->load->view('admin/usermembershipplan/plansuccess',$data);
            $this->load->view('include/footer');
        }else{
            redirect(base_url().'profile/plan');
            }
        }else{
            redirect(base_url().'profile/plan');
         }
    }

    function paypalplanpurchase(){
    $txn_id         = $_GET['tx'];
    $payment_status = $_GET['st'];
    if($txn_id && $payment_status =='Completed'){
    $data_insert=array(
                            "eci_user_id"               => $userid,
                            "eci_membership_plan_id"    => $planid,
                            "eci_user_plan_status"      => "Inactive",
                            "eci_user_plan_contact"     => $this->input->post('eci_plan_contact'));
    $checkuserplan=$this->Common->select('eci_user_membership_plan',"where eci_user_id=$userid","eci_user_plan_id");
    if($checkuserplan){
      $this->Common->update("eci_user_membership_plan",$data_insert,array('eci_user_id'=>$userid));
      $data['msg'] = "Thank you for upgrading plan.Our representative will call you within 24 hour";
    }else{
         $this->Common->data_insert("eci_user_membership_plan",$data_insert);
         $data['msg'] = "Thank you for purchasing plan.Our representative will call you within 24 hour";
    }
        $this->load->view('include/header');
        $this->load->view('admin/usermembershipplan/plansuccess',$data);
        $this->load->view('include/footer');
    }else{
        echo "unsuccess";
         }
    }

    function __setFormRules($formName=''){
      switch($formName){      
      case 'editProfile':
      $this->form_validation->set_rules('firstName', 'First name', 'trim|required');
      $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
      $this->form_validation->set_rules('phone_number', 'phone number', 'trim|numeric');
      break;
      case'changePassword':
      $this->form_validation->set_rules('eci_old_password', 'old password', 'trim|required');
      $this->form_validation->set_rules('eci_new_password', 'new password', 'trim|required|min_length[6]|matches[eci_cnew_password]');
      $this->form_validation->set_rules('eci_cnew_password', 'confirm password', 'trim|required');
      break;      
    }
       $this->form_validation->set_rules('userImage', 'User Image', 'trim');
       $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
       return $this->form_validation->run();
  }
}
