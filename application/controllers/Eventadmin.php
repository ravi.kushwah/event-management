<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventadmin extends CI_Controller {

	function __Construct()
	{
	parent::__construct();
	error_reporting(E_ALL);
	$_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
	$_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
	$this->load->helper(array('form', 'url','common'));
	$this->load->library(array('form_validation','session'));
	$this->load->model(array('Homemodel','Profilemodel','Common','my_model')); // database model 
	$this->load->database();
	}
	
	function index(){
		if(!isset($this->session->userdata['eci_super_login']))
		{
	         redirect(base_url().'user');
		}
		else
		{
			redirect(base_url().'eventadmin/addevent');
		}
	}
	
	function login($msg = ''){
		if(!isset($this->session->userdata['eci_super_login']))
		{
		if($msg == '')
		{
			redirect(base_url().'eventadmin');	
		}
		else
		{
			if($msg == 1)
			$this->session->set_flashdata('error', "Invaid credentials");
			else if($msg == 2)
				$this->session->set_flashdata('error', "Your account is not verified, please verify your account.Please check your email address to verify account");
			else if($msg == 'success')
				$this->session->set_flashdata('success', "Your account is verified now you can login");
			else if($msg == 'passwordreset')
				$this->session->set_flashdata('success', "Your Password has been reset");
			else
				$data['err_msg'] = "";
			$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
			$this->load->view('home/common/header',$data);
			$this->load->view('admin/logins',$data);
			$this->load->view('home/common/footer',$data);
			
		}
		}
		else
		{
		redirect(base_url().'eventadmin/addevent');
		}
	}
	
	function super_login(){
		if(!isset($this->session->userdata['eci_super_login']))
		{
			if(isset($_POST['getin_btn']))
			{
			$res = $this->Homemodel->admin_login(array('eci_admin_email'=>$_POST['uemail'],'eci_admin_pwd'=>md5($_POST['password'])));
			if(!empty($res))
			{
				$status = $res[0]['status'];
				if($status == 1){
						$password = $this->input->post('password',true);
						$uname    = $this->input->post('uemail',true);
						$password = base64_encode($password);
						if($_POST['rem_inp'] == 1)
						{
							$sec   =  time()+31536000;
							setcookie('eci_remember_uemail',$uname,$sec,'/');
							setcookie('eci_remember_password',$password,$sec,'/');
						}
						else
						{
							if(isset($_COOKIE['eci_remember_uemail']) && isset($_COOKIE['eci_remember_password']))
							{
								unset($_COOKIE['eci_remember_uemail']);
								setcookie('eci_remember_uemail','',time()-3600,'/');
								unset($_COOKIE['eci_remember_password']);
								setcookie('eci_remember_password','',time()-3600,'/');
							}
						}
						redirect(base_url().'eventadmin/create_session/'.$res[0]['eci_admin_sno'].'/'.$res[0]['eci_admin_pwd']);
					}else{
						redirect(base_url().'eventadmin/login/2');
						}
			}
			else
			{
				redirect(base_url().'eventadmin/login/1');
			}
		}
		else
		{
			redirect(base_url().'eventadmin');	
		}
		}
		else
		 redirect(base_url().'eventadmin/addevent');
	}
	
	function create_session($id='',$pass='')
	{
		$result=$this->Homemodel->admin_login(array('eci_admin_sno'=>$id,'eci_admin_pwd'=>$pass));
		if(!empty($result)){
		$user_details	= array(
								'eci_super_id'		=> $result[0]['eci_admin_sno'],
								'eci_super_name'		=> $result[0]['eci_admin_name'],
								'user_type'		=> $result[0]['user_type'],
								'eci_super_login'		=> true,
		                       );
		$this->session->set_userdata($user_details);
		if($result[0]['user_type'] == 1){
		redirect(base_url().'eventadmin/dashboard');
    	}else{
    	redirect(base_url().'profile');
		     }
		}else{
		redirect(base_url());
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
		
	function addevent(){		
		
		$noofcat = $this->input->post('noofcat',true);
		$json = 'null';
		if(isset($noofcat) && $noofcat !='' ){
			$category = array();
			$j		  = 0;
			for($i=0;$i<$noofcat;$i++){
				$category[$i] = '';
				$j++;
				$category[$i] .= $_POST['eci_cat_name'.$j].','.$_POST['eci_max_users'.$j].','.$_POST['eci_book_amount'.$j];
			}
			$json = json_encode($category);
		}
		$class = "";
		checkUserLogin();
		if(isset($_POST['sub_evnt_save'])) {
			
			$checkevent_name = get_eventname($_POST['eci_event_name']);
			if($checkevent_name == 0){
			   $checksubscription = checksubcription();
			    if($checksubscription){
					if(isset($_POST['tcat']) && $_POST['tcat'] == 1){
						$max_user = 0;
						$amt 	  = 0;
					}else{
						$max_user = $this->input->post('eci_max_users',true);
						$amt 	  = $this->input->post('eci_booking_amount',true);
					}
					$imagename = '';
					if($_FILES['eci_img']['name']!=''){
						$path			         = dirname(__FILE__);
						$abs_path		         = explode('application',$path);
						$pathToImages 		     = $abs_path[0].'/assets/back/events/';
						$config['upload_path']   = $pathToImages;
						$config['allowed_types'] = '*';
						$config['max_size']      = '0';
						$this->load->library('upload', $config);
						if ($this->upload->do_upload('eci_img'))
						{
							$uploaddata		         =  $this->upload->data();
							$img_name		         =  $uploaddata['raw_name'];
							$imagename			     =  $img_name.$uploaddata['file_ext'];
							$target_img				 =  $pathToImages.$imagename;
							$config['image_library'] = 'gd2';
							$config['source_image']  = $target_img;
							$config['maintain_ratio']= FALSE;
							$config['width']         = 600;
							$config['height']        = 320;
							$this->load->library('image_lib', $config);
							$this->image_lib->resize();
						}
					}			 	
					$time		= $_POST['eci_event_time_h'];
					$timearray	= explode(':',$time);
					$h			= trim($timearray[0]);
					$m			= trim($timearray[1]);
					$p			= trim($timearray[2]);
					$timestamp  = strtotime($_POST['eci_event_date'].' '.$h.':'.$m.$p);
					if($_POST['eci_event_list_amount']!="FREE"){
						$curr = $this->db_model->select_data('eci_payment_secret_key,eci_payment_detail_ccode,eci_payment_publish_key','eci_payment_detail','',1,'','')[0];
						$stripePro = stripeProductCreate($curr['eci_payment_secret_key'],$_POST['eci_event_name'], $amt,$curr['eci_payment_detail_ccode'],$_POST['eci_event_desc']);
						$priceID = $stripePro['priceData']['id'];
					}else{
						$priceID = "";
					}
					$data_Arr=array(
									'eci_event_list_name'		       => $_POST['eci_event_name'],
									'eci_event_pro_price_id'		   => $priceID,
									'eci_event_list_evnt_date'	       => $_POST['eci_event_date'],
									'eci_event_list_evnt_time'	       => $_POST['eci_event_time_h'],
									'eci_event_list_fetu'		       => $_POST['eci_ser'],
									'eci_event_list_max_user'	       => $max_user,
									'eci_event_list_amount'		       => $amt,
									'eci_event_list_desc'		       => $_POST['eci_event_desc'],
									'eci_event_list_address'	       => $_POST['eci_event_address'],
									'eci_event_list_video'			   => $_POST['eci_yt_url'],
									'eci_event_list_uniqid'		       => substr(str_shuffle("01234123456789123489"), 0, 6),
									'eci_event_list_img'		       => 0,
									'eci_event_list_date'		       => date('d-m-Y'),
						            'eci_event_list_status'			   => 1,
									'eci_event_list_upcoming'	       => 1,
									'eci_event_list_booking	'	       => 1,
									'eci_event_list_timestamp'	       => $timestamp,
									'eci_event_user_id'			       => $this->session->userdata('eci_super_id'),
									'eci_event_membership_type'        => $checksubscription->eci_plan_type,
									'eci_event_list_image'	           => $imagename,
									'eci_event_list_cat'               => $_POST['tcat'],
									'eci_event_list_noofcat'           => $_POST['noofcat'],
									'eci_event_list_categories' 	   => $json,
									'eci_event_list_catwise_seat_left' => $json,
					);
					
					$eventid  = $this->Homemodel->event_tbl('insert',$data_Arr,'');
					if($checksubscription->eci_plan_type == 1){
						$event_addtional = array("eci_event_id"=>$eventid,
									             "status"=>"Pending");
						$this->Common->data_insert('eci_event_commision_status',$event_addtional);
					}
					$msg	= "Event has been saved successfully.";
					$class	= "success";
				}else{
						$planurl  = '<a class="btn btn-default" href="'.base_url().'profile/plan">Purchase Plan</a>';
						$msg      = "Your subscription plan has been expire Or You not subcribe to any plan <br>Please click here to subcribe:$planurl";
						$class    = "danger";
					}
				}else{
						$msg	= "Event with this name is already exist please try with some other name ";
						$class	= "danger";
				 }
				}else{
					$msg='0';
			 		 }
			$data['class']    =   $class;
			$data['msg']      =   $msg;
			$data['services'] =   $this->Common->select('eci_event_service_list','where eci_event_service_list_status!=0','eci_event_service_list_sno,eci_event_service_list_name');
			$this->load->view('include/header');
			$this->load->view('admin/addevent',$data);
			$this->load->view('include/footer');
	}
	
	function updateexistingevent($id=''){
		checkUserLogin();
		$class="";
		if($id != '')
		{
			$j			= 0;
			$json   	= 'null';
			$tcat   	= 0;
			$ncat   	= 0;
			$category[] =array();
			if(isset($_POST['noofcat']) && $_POST['noofcat'] !=''){
				for($i=0;$i<$_POST['noofcat'];$i++){
					$category[$i]='';
					$j++;
					$category[$i] .= $_POST['eci_cat_name'.$j].','.$_POST['eci_max_users'.$j].','.$_POST['eci_book_amount'.$j];
				}
				$json =json_encode($category);
			}
			if(isset($_POST['sub_evnt_save'])) {
					if(isset($_POST['tcat']) && $_POST['tcat'] == 1){
						$max_user = 0;
						$amt 	  = 0;
						$tcat     = $_POST['tcat'];
						$ncat 	  = $_POST['noofcat'];
					}else{
						$max_user = $_POST['eci_max_users'];
						$amt 	  = $_POST['eci_booking_amount'];
					}
                $imagename = '';
				if($_FILES['eci_img']['name']!=''){
					$path		             = dirname(__FILE__);
					$abs_path	             = explode('application',$path);
					$pathToImages            = $abs_path[0].'/assets/back/events/';
					$config['upload_path']   = $pathToImages;
					$config['allowed_types'] = '*';
					$config['max_size']    	 = '0';
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('eci_img'))
					{
						$uploaddata 	 = $this->upload->data();
						$img_name		 = $uploaddata['raw_name'];
						$imagename		 = $img_name.$uploaddata['file_ext'];
						$prod_upload_det = $this->Common->select('eci_event_list',"where eci_event_list_sno =$id ",'eci_event_list_image');
						if($prod_upload_det){
							if( $prod_upload_det[0]['eci_event_list_image'] != '' ) {
								unlink( $pathToImages.$prod_upload_det[0]['eci_event_list_image'] );
							}
						}
						$target_img				 = $pathToImages.$imagename;
						$config['image_library'] = 'gd2';
						$config['source_image']  = $target_img;
						$config['maintain_ratio']= FALSE;
						$config['width']         = 600;
						$config['height']        = 320;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
					}
				}			 
				$time		= $_POST['eci_event_time_h'];
				$timearray	= explode(':',$time);
				$h			= trim($timearray[0]);
				$m			= trim($timearray[1]);
				$p			= trim($timearray[2]);
				$timestamp 	= strtotime($_POST['eci_event_date'].' '.$h.':'.$m.$p);
				$curr = $this->db_model->select_data('eci_payment_secret_key,eci_payment_detail_ccode,eci_payment_publish_key','eci_payment_detail','',1,'','')[0];
				if($_POST['eci_event_list_amount']!="FREE"){
					$allPro = stripeGetAllProduct($curr['eci_payment_secret_key']);
					$priceId = "";
					foreach ($allPro as $key => $value) {
							if($value['name']=='tree'){
								$priceId .=$value['id'];
								break;
							}
					}
					if(empty($priceId)){
						$stripePro = stripeProductCreate($curr['eci_payment_secret_key'],$_POST['eci_event_name'], $amt,$curr['eci_payment_detail_ccode'],$_POST['eci_event_desc']);
					}else{
						$stripePro =  stripeProductUpdate($curr['eci_payment_secret_key'],$priceId,$_POST['eci_event_name'],$amt,$curr['eci_payment_detail_ccode']);
					}
					$priceID = $stripePro['priceData']['id'];
				}else{
					$priceID = "";
				}
				$data_Arr=array(
								'eci_event_list_name'				=> $_POST['eci_event_name'],
								'eci_event_pro_price_id'		   => $priceID,
								'eci_event_list_evnt_date'			=> $_POST['eci_event_date'],
								'eci_event_list_evnt_time'			=> $_POST['eci_event_time_h'],
								'eci_event_list_fetu'				=>$_POST['eci_ser'],
								'eci_event_list_max_user'			=> $max_user,
								'eci_event_list_desc'				=> $_POST['eci_event_desc'],
								'eci_event_list_address'			=> $_POST['eci_event_address'],
								'eci_event_list_video'				=> $_POST['eci_yt_url'],
								'eci_event_list_timestamp'			=> $timestamp,
								'eci_event_list_amount'				=> $amt,
								'eci_event_list_cat' 				=> $tcat,
								'eci_event_list_noofcat' 			=> $ncat,
								'eci_event_list_categories' 		=> $json,
								'eci_event_list_catwise_seat_left' 	=> $json,
					);
					if($imagename != ''){
					  $data_Arr['eci_event_list_image'] = $imagename;
					}
				
				$this->Homemodel->event_tbl('update',$data_Arr,array('eci_event_list_sno'=>$id));
				$msg	= "Event update succesfully";
				$class	= "success";
			}else{
					$msg='0';
				}
			$data['solo_event'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$id));
			$data['services']   = $this->Common->select('eci_event_service_list','where eci_event_service_list_status!=0','eci_event_service_list_sno,eci_event_service_list_name');
			$currencycode 		= select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");
			$data['indication'] = $data['solo_event'][0]['eci_event_list_max_user'];
			$data['class']		= $class;
			$data['msg']		= $msg;
			$decode_json 		= json_decode($data['solo_event'][0]['eci_event_list_categories'],true);
			$categories			= '';
			$c					= 0;
			if($data['solo_event'][0]['eci_event_list_cat'] == 1){
				for($i=0;$i<count($decode_json);$i++){
					$c++;
					$str 		 = $decode_json[$i];
					$record 	 = explode(',',$str,3);
					$categories .='<div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_namecat_err" ></p><label for="eci_no_service">Ticket Category Name<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 catName" id="eci_cat_name" name="eci_cat_name'.$c.'" placeholder="" value="'.$record[0].'"><p class="help-block">For ex Golden ,Silver, platinum. </p></div><div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_catmem_err" ></p><label for="eci_no_service">Maximum Members<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 maxMember" id="eci_max_users" name="eci_max_users'.$c.'" placeholder="" value="'.$record[1].'" ><p class="help-block">Booking will be ON by default. </p></div><div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_catamt_err" ></p><label for="eci_book_amount">Amount to be paid by 1 member ('.$currencycode.')<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 bookAmt" id="eci_book_amount" name="eci_book_amount'.$c.'" placeholder="" value="'.$record[2].'" ><p class="help-block"> Do not type currency symbol. </p></div>';
				}
				$data['categoryRecords'] = $categories;
				$data['noofcat']		 = count($decode_json);
			}
			$this->load->view('include/header');
			$this->load->view('admin/addevent',$data);
			$this->load->view('include/footer');
		}else{
			redirect(base_url().'eventadmin/manageevent');
		}
	}

	function manageevent(){
		if(isset($this->session->userdata['eci_super_login']))
		{
			$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
		if($this->session->userdata('user_type')==1){
		$data['event_list'] = $this->Homemodel->event_tbl('select','','');
		}else{
		$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_user_id'=>$this->session->userdata('eci_super_id')));
		}
		$this->load->view('include/header');
		$this->load->view('admin/manageevent',$data);
		$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
		
	function uploadimage(){
		if(isset($this->session->userdata['eci_super_login']))
		{
			$this->load->view('include/header');
			$this->load->view('admin/uploadimage');
			$this->load->view('include/footer');
		}else{
			redirect(base_url().'eventadmin');
		}
	}

	function saveeventimage(){
		if(isset($this->session->userdata['eci_super_login']))
		{
		  $checksubscription		= checksubcription();
		  $path						= dirname(__FILE__);
		  $abs_path					= explode('/application/',$path);
		  $config['upload_path'] 	= './assets/back/eventimage/';
		  $config['allowed_types'] 	= '*';
		  $config['max_size']   	= '0';
		  $this->load->library('upload', $config);
		if ($this->upload->do_upload())
		{
			$uploaddata	=	$this->upload->data();
			$img_name	=  	$uploaddata['raw_name'];
			
		}
		else 
			$img_name = '';
			$error	  = $this->upload->display_errors();
		if($img_name != '')
		{
			$userid		= $this->session->userdata('eci_super_id');
			$imgdbname	= $img_name.date('ymdHis').$uploaddata['file_ext'];
			echo $imgdbname;
			$this->Homemodel->event_image_tbl('insert',array('eci_event_image_list_name'=> $imgdbname,'eci_admin_sno'=>$userid,),'');
			$oldfile	= FCPATH.'assets/back/eventimage/'.$img_name.$uploaddata['file_ext'];
			$newfile	= FCPATH.'assets/back/eventimage/'.$img_name.date('ymdHis').$uploaddata['file_ext'];
			rename($oldfile,$newfile);
		}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function setimage($msg = ''){
		if(isset($this->session->userdata['eci_super_login']))
		{
			if(isset($msg))
			$data['msg'] = $msg;
			else
			$data['msg'] = 0;
    		if($this->session->userdata['user_type'] == 1){
			$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
    		}else{
    		$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1,'eci_event_user_id'=>$this->session->userdata('eci_super_id')));
    		}
			$this->load->view('include/header');
			$this->load->view('admin/setimage',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function backgroundimage($msg=''){
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if(isset($msg))
			$data['msg'] = $msg;
		else
			$data['msg'] = 0;
			$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
			$this->load->view('include/header');
			$this->load->view('admin/backgroundimage',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function saveeventfeaturedimg()
	{
		if(isset($this->session->userdata['eci_super_login']))
		{
			if(isset($_POST['sub_save_feature']))
			{
			if(isset($_POST['section_selection']))
			{
				if($_POST['section_selection'] == 'sec_top'){
					if(isset($_POST['featured'])){
						$checked_img=implode(',',$_POST['featured']);
					}else{
							$checked_img='';
					     }
				}else{
						$checked_img=$_POST['featured'];
					 }
				$this->Homemodel->website_setting_tbl('update',array('eci_website_setting_value	'=>$checked_img),array('eci_website_setting_name	'=>$_POST['section_selection']));
				redirect(base_url().'eventadmin/backgroundimage/1');
			}
			else
			{
				$this->Homemodel->event_tbl('update',array('eci_event_list_img'=>$_POST['featured']),array('eci_event_list_sno'=>$_POST['event_selection']));
				redirect(base_url().'eventadmin/setimage/1');
			}
			}
			else
			redirect(base_url().'eventadmin/backgroundimage');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function get_images(){
		if(isset($this->session->userdata['eci_super_login']) && isset($_POST['type']))
		{
			if($_POST['type'] == 'bg') {
			$sec_detail 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>$_POST['selectedsection']));
			$images_list 	= $this->Homemodel->event_image_tbl('select','','');
			$displayHTML	= '';
			if(!empty($images_list))
			{
			if($_POST['selectedsection'] == 'sec_top'){
				$inputelement	= 'checkbox';
				$featured		= 'featured[]';
				$msg			= 'Set as slider Image';
				}
			else{
				$inputelement	='radio';
				$featured		='featured';
				$msg			='Set this image';
				}
			foreach($images_list as $solo_images_list)
				{
					$img_arr	=  explode(',',$sec_detail[0]['eci_website_setting_value']);
					
					if(in_array($solo_images_list['eci_event_image_list_sno'],$img_arr))
						$radio = 'checked';
					else
						$radio = '';
					$displayHTML .='<div class="col-lg-4 col-md-4 col-sm-4"><div class="eci_setimage"> <img src="'.base_url().'assets/back/eventimage/'.$solo_images_list['eci_event_image_list_name'].'" />
					<div class="'.$inputelement.'">  <input id="radio_'.$solo_images_list['eci_event_image_list_sno'].'" type="'.$inputelement.'" name="'.$featured.'" value="'.$solo_images_list['eci_event_image_list_sno'].'" '.$radio.'> <label for="radio_'.$solo_images_list['eci_event_image_list_sno'].'"><b>'.$msg.'</b></label> </div></div> </div>';
				}
			}
			echo $displayHTML;
		}
		else
		{
			$images_list 	= $this->Homemodel->event_image_tbl('select','','');
			$displayHTML	='';
			if(!empty($images_list))
			{
			foreach($images_list as $solo_images_list)
				{
					$featuredimg	=$this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['selectedevent'],'eci_event_list_img'=>$solo_images_list['eci_event_image_list_sno']));
					if(!empty($featuredimg))
						$radio = 'checked';
					else
						$radio = '';
					$displayHTML .='<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="eci_setimage"> 
					<img src="'.base_url().'assets/back/eventimage/'.$solo_images_list['eci_event_image_list_name'].'" />
					<div class="radio"> 
					<input id="radio_'.$solo_images_list['eci_event_image_list_sno'].'" type="radio" name="featured" value="'.$solo_images_list['eci_event_image_list_sno'].'" '.$radio.'>
					<label for="radio_'.$solo_images_list['eci_event_image_list_sno'].'"><b>Set as Feature</b></label> 
					</div>
					</div>
					</div>';
				}
			}
			echo $displayHTML;
		}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function updateeventssetting()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		$result=$this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['eventid']));
		
		if($result[0]['eci_event_list_featured'] == '0')
			$feature = '1';
		else
			$feature = '0';
		
		if($result[0]['eci_event_list_upcoming'] == '0')
			$upcoming = '1';
		else
			$upcoming = '0';
			
		if($result[0]['eci_event_list_booking'] == '0')
			$booking = '1';
		else
			$bookin = '0';
		if($_POST['type'] == 'upcom')
			$this->Homemodel->event_tbl('update',array('eci_event_list_upcoming'=>$upcoming),array('eci_event_list_sno'=>$_POST['eventid']));
		elseif($_POST['type'] == 'book')
		{
			$this->Homemodel->event_tbl('update',array('eci_event_list_booking'=>$booking),array('eci_event_list_sno'=>$_POST['eventid']));
			echo $booking;
		}
		else
		{
			$this->Homemodel->event_tbl('update',array('eci_event_list_featured'=>0),'');
			$this->Homemodel->event_tbl('update',array('eci_event_list_featured'=>$feature),array('eci_event_list_sno'=>$_POST['eventid']));
		}
		}
		else
		{
		redirect(base_url().'eventadmin');
		}
	}

	function addservices($msg='')
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if(isset($msg))
			$data['msg']=$msg;
		else
			$data['msg']=0;
			$data['images_list']=$this->Homemodel->event_image_tbl('select','','');
			$this->load->view('include/header');
			$this->load->view('admin/addservices',$data);
			$this->load->view('include/footer');
		}
		else
		{
	    	redirect(base_url().'eventadmin');
		}
	}
	
	function saveservices()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if(isset($_POST['sub_service_save']))
		{
			$images  =  "";
			if(isset($_POST['serviceimg'])){
			$images  =  implode(',',$_POST['serviceimg']);
			}
			$data_arr  =  array(
								'eci_event_service_list_name'	=> $_POST['eci_service_name'],
								'eci_event_service_list_des'	=> $_POST['eci_service_desc'],
								'eci_event_service_list_img'	=> $images,
								);
			$this->Homemodel->event_service_tbl('insert',$data_arr,'');
			redirect(base_url().'eventadmin/addservices/1');
		}
		else
			redirect(base_url().'eventadmin/addservices');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function updateexistingservices($id='')
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if($id != ''){
		if(isset($_POST['sub_service_save']))
		{
			$images		= implode(',',$_POST['serviceimg']);
			$data_arr	= array(
									'eci_event_service_list_name'	=> $_POST['eci_service_name'],
									'eci_event_service_list_des'	=> $_POST['eci_service_desc'],
									'eci_event_service_list_img'	=> $images,
									);
			$this->Homemodel->event_service_tbl('update',$data_arr,array('eci_event_service_list_sno'=>$id));
			redirect(base_url().'eventadmin/manageservices');
		}
		else
		{
			$data['solo_service'] = $this->Homemodel->event_service_tbl('select','',array('eci_event_service_list_sno'=>$id));
			$data['msg']		  = 0;
			$data['images_list']  = $this->Homemodel->event_image_tbl('select','','');
			$this->load->view('include/header');
			$this->load->view('admin/addservices',$data);
			$this->load->view('include/footer');
		}
		}
		else
		{
			redirect(base_url().'eventadmin/manageservices');
		}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function manageservices()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
			$data['service_list']	=	$this->Homemodel->event_service_tbl('select','','');
			$this->load->view('include/header');
			$this->load->view('admin/manageservices',$data);
			$this->load->view('include/footer');
		}
		else
		{
		redirect(base_url().'eventadmin');
		}
	}
	
	function delete_n_change($page='',$id='',$status='')
	{
		if(isset($this->session->userdata['eci_super_login']))
		{
		if($page != '' && $id != '' && $status != '')
		{
			if($page=='serv')
			{
				if($status == 'del')
				{
					$this->Homemodel->event_service_tbl('delete','',array('eci_event_service_list_sno'=>$id));
				}
				elseif($status == '1' || $status == '0')
				{
					$this->Homemodel->event_service_tbl('update',array('eci_event_service_list_status'=>$status),array('eci_event_service_list_sno'=>$id));
				}
				redirect(base_url().'eventadmin/manageservices');
			}
			elseif($page=='event')
			{
				if($status == 'del')
				{
					$this->Homemodel->event_tbl('delete','',array('eci_event_list_sno'=>$id));
					$this->Homemodel->event_tbl('delete','',array('eci_event_list_sno'=>$id));
				}
				redirect(base_url().'eventadmin/manageevent');
			}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function recentevents()
	{
		if(isset($this->session->userdata['eci_super_login']))
		{
			if($this->session->userdata('user_type')==1){
			$data['event_list'] = $this->Homemodel->event_tbl('select','recent','');
		}else{
			$data['event_list'] = $this->Homemodel->event_tbl('select','recent',array('eci_event_user_id'=>$this->session->userdata('eci_super_id')));
		}
			$this->load->view('include/header');
			$this->load->view('admin/recentevents',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function socialsetting()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
			if(isset($_POST['save_web_settings']))
			{
			if($_FILES['userfile']['name']!='')
			{
			$path				     = dirname(__FILE__);
			$abs_path			     = explode('application',$path);
			$config['upload_path']   = $abs_path[0].'/assets/front/images/';
			$config['allowed_types'] = '*';
			$config['max_size']      = '0';
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('userfile'))
			{
				$uploaddata	=  $this->upload->data();
				$img_name	=  $uploaddata['raw_name'];
			}
			else{ 
				$img_name	= '';
				$error 		= $this->upload->display_errors();
			}
			if($img_name != '')
			{
				rename($abs_path[0].'/assets/front/images/'.$img_name.$uploaddata['file_ext'],$abs_path[0].'/assets/front/images/logo.png');
			}
			}
			if($_POST['eci_fb']!='')
			{
				$eci_fb	=	$_POST['eci_fb'];
				if(isset($_POST['show_to_user_fb']))
					$show_to_user_fb	= 1;
				else
					$show_to_user_fb	= 0;
			}
			else
			{
				$eci_fb			 = '';
				$show_to_user_fb = 0;
			}
			if($_POST['eci_twitter']!='')
			{
				$eci_twitter	= $_POST['eci_twitter'];
				if(isset($_POST['show_to_user_twitter']))
					$show_to_user_twitter = 1;
				else
					$show_to_user_twitter = 0;
			}
			else
			{
				$eci_twitter	      = '';
				$show_to_user_twitter = 0;
			}
			if($_POST['eci_gplus']!='')
			{
				$eci_gplus	=$_POST['eci_gplus'];
				if(isset($_POST['show_to_user_gplus']))
					$show_to_user_gplus = 1;
				else
					$show_to_user_gplus = 0;
			}
			else
			{
				$eci_gplus='';
				$show_to_user_gplus = 0;
			}
			if($_POST['eci_pinterest']!='')
			{
				$eci_pinterest=$_POST['eci_pinterest'];
				if(isset($_POST['show_to_user_pinterest']))
					$show_to_user_pinterest = 1;
				else
					$show_to_user_pinterest = 0;
			}
			else
			{
				$eci_pinterest		    = '';
				$show_to_user_pinterest = 0;
			}
			if($_POST['eci_copy_txt']!='')
			{
				$eci_copy_txt=$_POST['eci_copy_txt'];
				if(isset($_POST['show_to_user_copy_txt']))
					$show_to_user_copy_txt = 1;
				else
					$show_to_user_copy_txt = 0;
			}
			else
			{
				$eci_copy_txt			= '';
				$show_to_user_copy_txt  = 0;
			}
			if(isset($_POST['show_to_user_services']))
				$show_to_user_services = 1;
			else
				$show_to_user_services = 0;
			if(isset($_POST['show_to_user_events']))
				$show_to_user_events = 1;
			else
				$show_to_user_events = 0;
			if(isset($_POST['show_to_user_ytvideo']))
				$show_to_user_ytvideo = 1;
			else
				$show_to_user_ytvideo = 0;
			if(isset($_POST['show_to_user_contact']))
				$show_to_user_contact = 1;
			else
				$show_to_user_contact = 0;
			if($_POST['eci_display_email']!='')
			{
				$eci_display_email=$_POST['eci_display_email'];
				if($_POST['eci_pwd']!='')
					$eci_pwd=md5($_POST['eci_pwd']);
				else
					$eci_pwd=$_POST['eci_original_pwd'];
			}
			$social_arr=array(
								'eci_fb'				=>	$eci_fb,
								'show_to_user_fb'		=>	$show_to_user_fb,
								'eci_twitter'			=>	$eci_twitter,
								'show_to_user_twitter'	=>	$show_to_user_twitter,
								'eci_gplus'				=>	$eci_gplus,
								'show_to_user_gplus'	=>	$show_to_user_gplus,
								'eci_pinterest'			=>	$eci_pinterest,
								'show_to_user_pinterest'=>	$show_to_user_pinterest,
								'eci_copy_txt'			=>	$eci_copy_txt,
								'show_to_user_copy_txt'	=>	$show_to_user_copy_txt,
								'show_to_user_services'	=>	$show_to_user_services,
								'show_to_user_events'	=>	$show_to_user_events,
								'show_to_user_ytvideo'	=>	$show_to_user_ytvideo,
								'show_to_user_contact'	=>	$show_to_user_contact,
				);
			$this->Homemodel->website_setting_tbl('update',array('eci_website_setting_value'=>json_encode($social_arr)),array('eci_website_setting_name'=>'social'));
			$this->Homemodel->admin_login(array('eci_admin_sno'=>1),'update',array('eci_admin_email'=>$eci_display_email,'eci_admin_pwd'=>$eci_pwd));
			}
				$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
				$data['admin_credentials']  = $this->Homemodel->admin_login(array('eci_admin_sno'=>1));
				$this->load->view('include/header');
				$this->load->view('admin/socialsetting',$data);
				$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function contactus()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if(isset($_POST['save_contact_sub']))
		{
			if(isset($_POST['show_to_user_email']))
				$show_to_user_email = 1;
			else
				$show_to_user_email = 0;
			if(isset($_POST['show_to_user_phone']))
				$show_to_user_phone = 1;
			else
				$show_to_user_phone = 0;
			if(isset($_POST['show_to_user_address']))
				$show_to_user_address = 1;
			else
				$show_to_user_address = 0;
			if(isset($_POST['save_data_db']))
				$save_data_db = 1;
			else
				$save_data_db = 0;
			$data_arr=array(
								'eci_contact_email'		=> $_POST['eci_contact_email'],
								'eci_contact_phone'		=> $_POST['eci_contact_phone'],
								'eci_contact_address'	=> $_POST['eci_contact_address'],
								'eci_contact_new_email'	=> $_POST['eci_contact_new_email'],
								'show_to_user_email'	=> $show_to_user_email,
								'show_to_user_phone'	=> $show_to_user_phone,
								'show_to_user_address'	=> $show_to_user_address,
								'send_contact_query'	=> $_POST['send_contact_query'],
								'save_data_db'			=> $save_data_db
								);
			$this->Homemodel->event_contactus_tbl('update',array('eci_contactus_data'=>json_encode($data_arr)),array('eci_contactus_sno'=>1));
		}
		$data['contactus_detail'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$this->load->view('include/header');
		$this->load->view('admin/contactus',$data);
		$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function paymentsetting()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
			if(isset($_POST['sub_payment_save']))
			{
			if($_POST['payment_method']=='paypal')
			{
			$data_arr=array(
								'eci_payment_detail_email'  	=>  $_POST['eci_payment_email'],
								'eci_payment_secret_key'  		=>  $_POST['eci_payment_secret_key'],
								'eci_payment_publish_key'  		=>  $_POST['eci_payment_publish_key'],
								'eci_payment_detail_ccode'  	=>  $_POST['eci_payment_ccode'],
								'eci_payment_detail_msg'  		=>  $_POST['eci_payment_msg'],
								'eci_payment_detail_em_temp'  	=>  $_POST['eci_payment_detail_em_temp'],
								'eci_payment_detail_type'  		=>  1,
				);
			$adata_arr=array('eci_payment_detail_ccode'  =>  $_POST['eci_payment_ccode']);
			$this->Homemodel->event_payment_detail_tbl('update',$data_arr,array('eci_payment_detail_sno'=>1));
			$this->Homemodel->event_payment_detail_tbl('update',$adata_arr,array('eci_payment_detail_sno'=>2));
			// change other option
			$this->Homemodel->event_payment_detail_tbl('update',array('eci_payment_detail_type'=>0),array('eci_payment_detail_sno'=>2));
			}
			else
			{
				if(isset($_POST['show_invoice_name']))
					$name = 1;
				else
					$name = 0;
				if(isset($_POST['show_invoice_email']))
					$email = 1;
				else
					$email = 0;
				if(isset($_POST['show_invoice_cntctno']))
					$cntctno = 1;
				else
					$cntctno = 0;
				if(isset($_POST['show_invoice_add']))
					$add = 1;
				else
					$add = 0;
				if(isset($_POST['send_invoice_link']))
					$link = 1;
				else
					$link = 0;
				$data_arr=array(
									'name'  	=>  $name,
									'email' 	=>  $email,
									'cntctno'   =>  $cntctno,
									'add'       =>  $add,
									'link'      =>  $link,
				);
				$updateArray = array(
										'eci_payment_detail_email'	=>	json_encode($data_arr),
										'eci_payment_detail_type'	=>  1,
										'eci_payment_detail_ccode'	=>  $_POST['show_invoice_curency_code']
									);
				$aupdateArray = array('eci_payment_detail_ccode'  =>  $_POST['show_invoice_curency_code']);
				$this->Homemodel->event_payment_detail_tbl('update',$updateArray,array('eci_payment_detail_sno'=>2));
				$this->Homemodel->event_payment_detail_tbl('update',$aupdateArray,array('eci_payment_detail_sno'=>1));
			// change other option
				$updateArrayOtherOption = array(
													'eci_payment_detail_type'    =>  0,
													'eci_payment_detail_msg'     =>  $_POST['eci_payment_msg'],
													'eci_payment_detail_em_temp' =>  $_POST['eci_payment_detail_em_temp'],
				);
				$this->Homemodel->event_payment_detail_tbl('update',$updateArrayOtherOption,array('eci_payment_detail_sno'=>1));
			}
		}
			$data['payment_detail'] = $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1','2'));
			$this->load->view('include/header');
			$this->load->view('admin/paymentsetting',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	function payNow(){stripePayNow($_POST['stripe_secretkey'],$_POST['stripePriceId']);}
	
	function stripe_pay($for=""){	
	  
		if($for == "plan"){
			$currency_code	= select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");
			$stripeSessionArr 	= explode('@#',$_SESSION['stripeSession']);
		 
			$amount				= $stripeSessionArr[1]/100;
			$custom 	= json_decode($stripeSessionArr[2], true);
			$ipn_arr	= array(
						'payment_date'			=> date("H:i:s M d, Y"),
						'payment_status'		=> 'Completed',
						'first_name'			=> $stripeSessionArr[3],
						'last_name'				=> '',
						'mc_fee'				=> $amount,
						'payer_email'			=>  $stripeSessionArr[6],
						'txn_id'				=> 'Stripe',
						'mc_currency'			=> $currency_code,
						'payer_business_name'	=> $stripeSessionArr[3],
					);
			$uniq_tckt_no 		= 'EV'.date('idsm').rand();
			$event_uniq_arr1	= explode('v',$stripeSessionArr[0]);
			$event_uniq_arr2	= explode('ent',$event_uniq_arr1[1]);
			$data_arr=array(
								'eci_payment_detail_email'			=> $event_uniq_arr2[0],
								'eci_payment_detail_ccode'			=> $event_uniq_arr2[1],
								'eci_payment_detail_msg'			=> json_encode($ipn_arr),
								'eci_payment_detail_em_temp'		=> 'user',
								'eci_payment_detail_type'			=> 4, // payment status
								'eci_payment_detail_tckt_no'		=> $uniq_tckt_no,
								'eci_payment_booking_source' 		=> 'Online',
								'eci_payment_coupon_code'			=> $custom['coupon_code'],
								'eci_payment_detail_persons_name'	=> $custom['personname'],
								'eci_booked_tickets_cate'			=> $stripeSessionArr[5]
			);	
			
			$this->Homemodel->event_payment_detail_tbl('insert',$data_arr,'');
			$event_data = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$event_uniq_arr2[0]));
			//booked ticket minus from total seat left
			$cat_seat_left = $stripeSessionArr[5];
			if($cat_seat_left !='null')
			{
				$total_leftseat = json_decode($event_data[0]['eci_event_list_catwise_seat_left']);
				$arry_str 		= array();
				$json_de  		= json_decode($cat_seat_left);
				for($i=0; $i<sizeof($total_leftseat); $i++){
					$totalseat_left_arr[$i] = explode(',',$total_leftseat[$i]);
					$booked_seat_arr[$i] 	= explode(',',$json_de[$i]);
					if($totalseat_left_arr[$i][0] 	= $booked_seat_arr[$i][0]){
						$totalseat_left_arr[$i][1] 	= $totalseat_left_arr[$i][1] - $booked_seat_arr[$i][1];
					}
					$arry_str[$i] =$totalseat_left_arr[$i][0].','.$totalseat_left_arr[$i][1].','.$totalseat_left_arr[$i][2]; 
				}
				$json_data 		= json_encode($arry_str);
				$updatedata  	= array('eci_event_list_catwise_seat_left'=>$json_data);
				$this->my_model->update_data('eci_event_list',$updatedata,array('eci_event_list_sno'=>$event_uniq_arr2[0])); 
			}

			$contactus_setting	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			$contact_data 	  	= json_decode($contactus_setting[0]['eci_contactus_data']);
			$cnt_data 		  	= json_decode($contactus_setting[0]['eci_contactus_data']);
			$paysetting 	  	= $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>1));
			$email_template 	= $paysetting[0]['eci_payment_detail_em_temp'];
			$email_template 	= str_replace("[cus_name]", $stripeSessionArr[3], $email_template);
			$email_template 	= str_replace("[event_name]", $event_data[0]['eci_event_list_name'], $email_template);
			$email_template 	= str_replace("[no_tckt]", $event_uniq_arr2[1], $email_template);
			$email_template 	= str_replace("[break]", '<br>', $email_template);
			$email_template 	= str_replace("[uniq_tckt]",$uniq_tckt_no, $email_template);
			$body				= "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> ".$event_data[0]['eci_event_list_name']."</title></head><body><p>".$email_template."</p></body></html>";
           
            $contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
            $cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data'],true);
            
			$from = $cnt_data['eci_contact_email'];
			$headers 	= 'From: '.$from.'  '."\r\n";
			$headers 	.= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$to			= $stripeSessionArr[6];
			$subject 	= 'Thanks For Booking Ticket for '.$event_data[0]['eci_event_list_name'];
			
			$res = $this->SendMail($from,$to,$subject,$body);
		
			redirect(base_url().'home/thanks/plan');
		}else{
			redirect(base_url().'home/thanks/cancel');
		}
		
	
	}
	
	function ipn(){	
	  
		if($_POST){
		$data			= array("data"=>json_encode($_POST));
		$custom 		= json_decode($_POST['custom'], true);
        $for			= $custom['for'];
		$testid 		= $custom['test_id'];
        $event_detail	= $this->Common->select('eci_test',"where t_id='$testid'",'response');
		$cat_seat_left  = $event_detail[0]['response'];
		
	    if($for=='event'){
			$ipn_arr=array(
								'payment_date'			=> $_POST['payment_date'],
								'payment_status'		=> $_POST['payment_status'],
								'first_name'			=> $_POST['first_name'],
								'last_name'				=> $_POST['last_name'],
								'mc_fee'				=> $_POST['mc_gross'],
								'payer_email'			=> $_POST['payer_email'],
								'txn_id'				=> $_POST['txn_id'],
								'mc_currency'			=> $_POST['mc_currency'],
								'payer_business_name'	=> $_POST['payer_business_name'],
			);
			if($_POST['payment_status'] == 'Completed')
				$pay_status = 4;
			else
				$pay_status = 2;
			$uniq_tckt_no 		= 'EV'.date('idsm').rand();
			$event_uniq_arr1	= explode('v',$_POST['item_name']);
			$event_uniq_arr2	= explode('ent',$event_uniq_arr1[1]);
			$data_arr=array(
								'eci_payment_detail_email'			=> $event_uniq_arr2[0],
								'eci_payment_detail_ccode'			=> $event_uniq_arr2[1],
								'eci_payment_detail_msg'			=> json_encode($ipn_arr),
								'eci_payment_detail_em_temp'		=> 'user',
								'eci_payment_detail_type'			=> $pay_status, // payment status
								'eci_payment_detail_tckt_no'		=> $uniq_tckt_no,
								'eci_payment_booking_source'		=> 'Online',
								'eci_payment_coupon_code'			=> $custom['coupon_code'],
								'eci_payment_detail_persons_name'	=> $custom['personname'],
								'eci_booked_tickets_cate'			=> $cat_seat_left
							);
			// $event_uniq_arr2[0] -- event code
			// $event_uniq_arr2[1] -- ticket count
			$this->Homemodel->event_payment_detail_tbl('insert',$data_arr,'');
			$event_data = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$event_uniq_arr2[0]));
			if($cat_seat_left !='null'){
				$total_leftseat = json_decode($event_data[0]['eci_event_list_catwise_seat_left']);
				$arry_str 		= array();
				$json_de  		= json_decode($cat_seat_left);
				for($i=0; $i<sizeof($total_leftseat); $i++){
					$totalseat_left_arr[$i] = explode(',',$total_leftseat[$i]);
					$booked_seat_arr[$i] 	= explode(',',$json_de[$i]);
					if($totalseat_left_arr[$i][0] = $booked_seat_arr[$i][0]){
						$totalseat_left_arr[$i][1] = $totalseat_left_arr[$i][1] - $booked_seat_arr[$i][1];
					}
					$arry_str[$i] =$totalseat_left_arr[$i][0].','.$totalseat_left_arr[$i][1].','.$totalseat_left_arr[$i][2]; 
				}
				$json_data 		= json_encode($arry_str);
				$updatedata  	= array('eci_event_list_catwise_seat_left'=>$json_data);
				$this->my_model->update_data('eci_event_list',$updatedata,array('eci_event_list_sno'=>$event_uniq_arr2[0]));
			}			
			$contactus_setting = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			$contact_data 	   = json_decode($contactus_setting[0]['eci_contactus_data']);
			$cnt_data 		   = json_decode($contactus_setting[0]['eci_contactus_data']);
			$paysetting 	   = $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>1));
			$email_template    = $paysetting[0]['eci_payment_detail_em_temp'];
			$email_template    = str_replace("[cus_name]", $_POST['first_name'].' '.$_POST['last_name'], $email_template);
			$email_template    = str_replace("[event_name]", $event_data[0]['eci_event_list_name'], $email_template);
			$email_template    = str_replace("[no_tckt]", $event_uniq_arr2[1], $email_template);
			$email_template    = str_replace("[break]", '<br>', $email_template);
			$email_template    = str_replace("[uniq_tckt]",$uniq_tckt_no, $email_template);
			$body			   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> ".$event_data[0]['eci_event_list_name']."</title></head><body><p>".$email_template."</p></body></html>";
			
	        $contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
            $cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data'],true);
            
			$from =$cnt_data['eci_contact_email'];
			$headers 	 = 'From: '.$from.'  '."\r\n";
			$headers 	.= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$to 		 = $_POST['payer_email'];
			$subject 	 = 'Thanks For Booking Ticket for '.$event_data[0]['eci_event_list_name'];
			$res = $this->SendMail($from,$to,$subject,$body);
		}
		if($for	== 'plan'){
		  $unique_id		=	$custom['unique_id'];
		  $checkuniqueid	=	$checkuserplan=$this->Common->select('eci_membership_payment_detail',"where eci_unique_id='$unique_id'");
		if($checkuniqueid){
			$ipn_arr=array(
							'payment_date'	=> $_POST['payment_date'],
							'payment_status'=> $_POST['payment_status'],
							'first_name'	=> $_POST['first_name'],
							'last_name'		=> $_POST['last_name'],
							'mc_fee'		=> $_POST['mc_gross'],
							'payer_email'	=> $_POST['payer_email'],
							'txn_id'		=> $_POST['txn_id'],
							'mc_currency'	=> $_POST['mc_currency'],
				
						);
			$data_arr	=	array("eci_payment_detail_msg"=>json_encode($ipn_arr));
			$this->Common->update("eci_membership_payment_detail",$data_arr,array('eci_unique_id'=>$unique_id));
			$planprice	=	$checkuniqueid[0]['eci_plan_price'];
			$userid		= 	$checkuniqueid[0]['eci_user_id'];
			if($_POST['payment_status']=='Completed' && $_POST['mc_gross']==$planprice)
			{
               $checkuserplan=$this->Common->select('eci_user_membership_plan',"where eci_user_id=$userid","eci_user_plan_id");
               $data_insert=array(
									"eci_user_id"			 => $userid,
									"eci_membership_plan_id" => $checkuniqueid[0]['eci_plan_id'],
									"eci_user_plan_status"   => "Active",
									"startdate"				=>  time());
            if($checkuserplan){
            $this->Common->update("eci_user_membership_plan",$data_insert,array('eci_user_id'=>$userid));
             }
            else{
            $this->Common->data_insert("eci_user_membership_plan",$data_insert);
              }
			  $contact_sett = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			  $cnt_data 	= json_decode($contact_sett[0]['eci_contactus_data']);
				if($cnt_data->send_contact_query == 'no')
					$admin_email=$cnt_data->eci_contact_new_email;
				else
					$admin_email=$cnt_data->eci_contact_email;
				$planid			= $checkuniqueid[0]['eci_plan_id'];
				$plandetail		= $this->Common->select("eci_membership_plan","where eci_plan_id=$planid");
				$planname		= $plandetail[0]['eci_plan_name'];
                $plantype		= 'Time Based';
				$planduration	= $plandetail[0]['eci_plan_duration'].' Days';
				$planstatus		= 'Active';
				$email_data		= array('username'=>$_POST['first_name'],'planname'=>$planname,'plantype'=>$plantype,'planduration'=>$planduration,'planstatus'=>$planstatus);
				$message		= getemailmsg('addplan',$email_data);
				$headers 		= "MIME-Version: 1.0" . "\r\n";
				$headers 		.= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers		.= 'From:' .$admin_email. "\r\n";
                $to				=$_POST['payer_email'];
                $contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
                $cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data'],true);
                
    			$from =$cnt_data['eci_contact_email'];
				$res = $this->SendMail($from,$to,'Update Plan ',$message);				
			}
		}
		}
		}
	}
	function testingdemo(){
		$message ="Hello Tree Event Is Booked ";
		$to				='try123@mailinator.com';
		$from	 = 'support@eventmanagement.com';
		$res = $this->SendMail($from,$to,'Book Now',$message);		
	}
	function SendMail($from,$to,$subject,$body){
	    $smtp_encryption = 'tsl';
        $smtp_port = '587';
        $smtp_user = 'omprakash.jha@himanshusofttech.com';
        $smtp_password = 'GNmf3zKrOBHtdJ17';
        $smtp_server  = 'Smtp-relay.sendinblue.com';
        
        $this->load->library('email'); // load library 
        
         $config = array(
            'protocol' => 'SMTP',
    		'smtp_host' => $smtp_server,
    		'smtp_port' => $smtp_port,
    		'smtp_user' => $smtp_user,
    		'smtp_pass' => $smtp_password,
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'newline'   =>  "\r\n",
            'smtp_crypto'   =>  $smtp_encryption,
            // 'SMTPAuth'   => true,
        );
        $this->email->initialize($config);
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($body);
        if(! $this->email->send()){
    		$c =  $this->email->print_debugger();
    	    return $ress = array('status'=>0,'message'=>$c);
    	}else{
	    	return $ress = array('status'=>1,'message'=>'Mail has been sent successfully.');
    	}
    
	}
	function bookingdetail($msg=''){
		if(isset($this->session->userdata['eci_super_login']))
		{
			$userid=$this->session->userdata('eci_super_id');
			$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
			if($this->session->userdata('user_type')==2){
				$data['event_list'] = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1,'eci_event_user_id'=>$userid));
			}
			if(isset($msg))
				$data['msg']=$msg;
				
			
			$this->load->view('include/header');
			$this->load->view('admin/bookingdetail',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function get_booking_details(){
	   
		if(isset($this->session->userdata['eci_super_login']))
		{
			$join_array 	= array('multiple',
								array(
										array('eci_event_list','eci_event_list.eci_event_list_sno = eci_payment_detail.eci_payment_detail_email'),
										array('eci_admin','eci_admin.eci_admin_sno = eci_event_list.eci_event_user_id')
									)
			                  );
			$result_booking  = $this->my_model->select_data('eci_payment_detail.*,eci_admin.eci_admin_name','eci_payment_detail',array('eci_payment_detail_email'=>$_POST['selectedevent']),'','','',$join_array);
		 
			$event_detail 	 = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['selectedevent']));
		
			if(!empty($result_booking)) {
				$action		=	'Edit/View';
				$heading 	=	'<th>Vendor Name</th>';
				if($this->session->userdata('user_type')==2){
					$action		=	'View';
					$heading	=	'';
				}
					$tbl_display_Str = '<div class="col-lg-12"><div class="table-responsive"> <table class="table"><thead><tr><th>SNo.</th><th>Name</th>	<th>Email</th><th>Amount</th><th>Ticket Count</th><th>Unique Ticket No.</th>'.$heading.'<th>Date</th><th>'.$action.'</th></tr></thead><tbody>';
					$count			 = 0;
					foreach($result_booking as $solo_result_booking) {
						if($this->session->userdata('user_type') == 2){
						$heading_Data = '';}
						else{
							$heading_Data = '<td>'.$solo_result_booking['eci_admin_name'].'</td>';
						}
					$ticket_count[] = $solo_result_booking['eci_payment_detail_ccode'];
					$booking_detail = json_decode($solo_result_booking['eci_payment_detail_msg']);
					$count++;
					if(isset($booking_detail->first_name) && isset($booking_detail->last_name))
						$uname = $booking_detail->first_name.' '.$booking_detail->last_name;
					else
						$uname = $booking_detail->first_name;
					if($booking_detail->txn_id == 'Stripe' ){
						$type  = '<i class="fa fa-rss"  title="Stripe"></i>';
					}else if($booking_detail->txn_id != 'Manual' && $booking_detail->txn_id != 'Offline' && $booking_detail->txn_id != 'Stripe' ){
						$type  = '<i class="fa fa-paypal" title="Paypal"></i>';
					}
					else{
						$type  = '<i class="fa fa-exchange" title="Manual"></i>';
					}
						$tbl_display_Str .= '<tr><td>'.$type.$count.'</td><td>'.$uname.'</td><td>'.$booking_detail->payer_email.'</td><td>'.$booking_detail->mc_fee.'</td><td>'.$solo_result_booking['eci_payment_detail_ccode'].'</td></td><td>'.$solo_result_booking['eci_payment_detail_tckt_no'].'</td>'.$heading_Data.'<td>'.$booking_detail->payment_date.'</td><td><span><a href="'.base_url().'eventadmin/add_booking_manually/'.$solo_result_booking['eci_payment_detail_sno'].'" title="EDIT"><img src="'.base_url().'assets/back/images/icons/edit.png" alt="EDIT"> </a></span></td></tr>';
					}
					$tbl_display_Str 	 .='</tbody></table></div></div>';
					$total_tickets 		= array_sum($ticket_count);
					$top_display_Str	= ' <div class="col-lg-12"><p>Total number of tickets booked : <b>'.$total_tickets.'</b> Out Of <b>'.$event_detail[0]['eci_event_list_max_user'].'</b> </p><a href="'.base_url().'eventadmin/booking_csv/'.$_POST['selectedevent'].'" class="btn btn-default btn-sm pull-right margin-top">Download CSV</a></div>';
					$onlinesell			= $this->Homemodel->getsellcount($_POST['selectedevent'],'Online');
					$offlinesell		= $this->Homemodel->getsellcount($_POST['selectedevent'],'Offline');
					echo $top_display_Str.$tbl_display_Str;
					$selldetail			= '<div class="col-lg-12">
					<div class="bkng_sell_detail">
					<h4>Sell Detail: (Those ticket their payment status is paid)</h4>
					<table class="table table-bordered">
				    <tr><th>Online Sell</th><th>Offline Sell</th></tr>
				    <tr><th>'.$onlinesell.'</th><th>'.$offlinesell.'</th></tr>
					</table></div></div>';
					echo $selldetail;
					$table='<table class="table table-bordered">
					   <tr><th>Online Sell</th><th>Offlinesell</th></tr>
					   <tr><td>2</td><td>3</td></tr>
					</table>';
				}
				else
				{
					echo '<div class="col-lg-12"><p>No Data Available</p></div>';
				}
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function change_payment_status(){
		checkAdminOnly();
		$this->Homemodel->event_payment_detail_tbl('update',array('eci_payment_detail_type'=>$_POST['newstatus']),array('eci_payment_detail_sno'=>$_POST['pay_sno']));
	}
	
	function add_booking_manually($bookingid=''){
		if(isset($this->session->userdata['eci_super_login']))
		{
			if(isset($bookingid) && $bookingid !='')
			{	
				$data['edit_booking'] 		= $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>$bookingid));
				if(!empty($data['edit_booking'])){
					$data['event_detail'] 	= $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$data['edit_booking'][0]['eci_payment_detail_email']));
				} 
			}
			$data['list_of_payment_sts'] = $this->Homemodel->event_payment_status_tbl('select','','');
			$data['event_list'] 		 = $this->Homemodel->event_tbl('select','',array('eci_event_list_status'=>1));
			$data['msg']				 = 0;
			$this->load->view('include/header');
			$this->load->view('admin/add_booking_manually',$data);
			$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function save_manual_booking(){
		 checkAdminOnly();
			if(isset($this->session->userdata['eci_super_login']))
			{
			if($_POST['id_of_booking']=='0') 
			{
				$eventid 	 = $_POST['eci_book_event_list'];
				$booked_cate = 'null';
				if(isset($_POST['noofcat'])  && $_POST['noofcat'] !=0 )
				{
					$event_detail 	= $this->Common->select('eci_event_list',"where eci_event_list_sno='$eventid'",' eci_event_list_catwise_seat_left');
					$total_leftseat = json_decode($event_detail[0]['eci_event_list_catwise_seat_left']);
					$booked_cate 	= $_POST['booked_ticket_cat'];
					$arry_str 		= array();
					$json_de  		= json_decode($booked_cate);
					for($i=0; $i<sizeof($total_leftseat); $i++)
					{
						$totalseat_left_arr[$i] = explode(',',$total_leftseat[$i]);
						$booked_seat_arr[$i] 	= explode(',',$json_de[$i]);
						if($totalseat_left_arr[$i][0] = $booked_seat_arr[$i][0])
						{
							$totalseat_left_arr[$i][1] = $totalseat_left_arr[$i][1] - $booked_seat_arr[$i][1];
						}
						$arry_str[$i] =$totalseat_left_arr[$i][0].','.$totalseat_left_arr[$i][1].','.$totalseat_left_arr[$i][2]; 
					}
					$json_data   = json_encode($arry_str);
					$updatedata  = array('eci_event_list_catwise_seat_left'=>$json_data);
					$this->my_model->update_data('eci_event_list',$updatedata,array('eci_event_list_sno'=>$eventid));
					$this->my_model->insert_data('eci_test',array('response'=> $booked_cate));
				} 
				$ipn_arr=array(
								'payment_date'		=> date('H:i:s M d, Y'),
								'first_name'		=> $_POST['eci_book_cust_name'],
								'mc_fee'			=> $_POST['eci_book_amount'],
								'payer_email'		=> $_POST['eci_book_cust_email'],
								'txn_id'			=> 'Manual',
								'manual_cntc_no'	=> $_POST['eci_book_cust_cntct'],
								'manual_address'	=> $_POST['eci_book_cust_add'],
								);
				$uniq_tckt_no = 'EV'.date('idsm').rand();
				$data_arr=array(
									'eci_payment_detail_email'			=> $_POST['eci_book_event_list'],
									'eci_payment_detail_ccode'			=> $_POST['eci_book_cust_no_ticket'],
									'eci_payment_detail_msg'			=> json_encode($ipn_arr),
									'eci_payment_detail_em_temp'		=> 'user',
									'eci_payment_detail_type'			=> $_POST['eci_book_cust_payment_sts'],
									'eci_payment_detail_tckt_no'		=> $uniq_tckt_no,
									'eci_payment_booking_source'		=>'Offline',
									'eci_payment_detail_persons_name'	=> json_encode($_POST['person']),
									'eci_booked_tickets_cate'			=> $booked_cate
									);
				// $event_uniq_arr2[0] -- event code
				// $event_uniq_arr2[1] -- ticket count
				$this->Homemodel->event_payment_detail_tbl('insert',$data_arr,'');
			}
			else
			{	
				$eventid 	 = $_POST['eci_book_event_list'];
				$booked_cate = 'null';
				if(isset($_POST['noofcat'])  && $_POST['noofcat'] !=0 )
				{
					$event_detail   = $this->Common->select('eci_event_list',"where eci_event_list_sno='$eventid'",' eci_event_list_catwise_seat_left');
					$total_leftseat = json_decode($event_detail[0]['eci_event_list_catwise_seat_left']);
					$booked_cate    = $_POST['booked_ticket_cat'];
					$old_booking    = json_decode($_POST['booked_ticket_cat_old']);
					$arry_str 		= array();
					$diffrence		= array();
					$json_de  		= json_decode($booked_cate);
					for($i=0; $i<sizeof($total_leftseat); $i++)
					{
						$totalseat_left_arr[$i] = explode(',',$total_leftseat[$i]);
						$booked_seat_arr[$i]    = explode(',',$json_de[$i]);
						$old_bookseat[$i]       = explode(',',$old_booking[$i]);
						if($totalseat_left_arr[$i][0] = $booked_seat_arr[$i][0])
						{	$diffrence[$i] =  $booked_seat_arr[$i][1] - $old_bookseat[$i][1];
							 
							$totalseat_left_arr[$i][1] = $totalseat_left_arr[$i][1] - $diffrence[$i];
						}
						$arry_str[$i] =$totalseat_left_arr[$i][0].','.$totalseat_left_arr[$i][1].','.$totalseat_left_arr[$i][2]; 
					}
					$json_data 		= json_encode($arry_str);
					$updatedata  	= array('eci_event_list_catwise_seat_left'=>$json_data);
					$this->my_model->update_data('eci_event_list',$updatedata,array('eci_event_list_sno'=>$eventid));
					$tid = $this->my_model->insert_data('eci_test',array('response'=> $booked_cate));
				}
				$uniq_tckt_no 	= $_POST['uniq_tckt_no'];
				$ipn_arr=array(
									'payment_date'		=> date('H:i:s M d, Y'),
									'first_name'		=> $_POST['eci_book_cust_name'],
									'mc_fee'			=> $_POST['eci_book_amount'],
									'payer_email'		=> $_POST['eci_book_cust_email'],
									'txn_id'			=> 'Manual',
									'manual_cntc_no'	=> $_POST['eci_book_cust_cntct'],
									'manual_address'	=> $_POST['eci_book_cust_add'],
									);
				
				$data_arr=array(
									'eci_payment_detail_ccode'			=> $_POST['eci_book_cust_no_ticket'],
									'eci_payment_detail_msg'			=> json_encode($ipn_arr),
									'eci_payment_detail_type'			=> $_POST['eci_book_cust_payment_sts'],
									'eci_payment_detail_persons_name'	=> json_encode($_POST['person']),
									'eci_booked_tickets_cate'			=> $booked_cate
									);
				$this->Homemodel->event_payment_detail_tbl('update',$data_arr,array('eci_payment_detail_sno'=>$_POST['id_of_booking']));
			}
			if(isset($_POST['send_cust_email']))
			{
				$event_data			 = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['eci_book_event_list']));
				$contactus_setting 	 = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
				$cnt_data 			 = json_decode($contactus_setting[0]['eci_contactus_data']);
				$paysetting 		 = $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_sno'=>1));
				$email_template 	 = $paysetting[0]['eci_payment_detail_em_temp'];
				$email_template 	 = str_replace("[cus_name]", $_POST['eci_book_cust_name'], $email_template);
				$email_template 	 = str_replace("[event_name]", $event_data[0]['eci_event_list_name'], $email_template);
				$email_template 	 = str_replace("[no_tckt]", $_POST['eci_book_cust_no_ticket'], $email_template);
				$email_template 	 = str_replace("[break]", '<br>', $email_template);
				$email_template		 = str_replace("[uniq_tckt]",$uniq_tckt_no, $email_template);
				$em_msg="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> ".$event_data[0]['eci_event_list_name']."</title></head><body><p>".$email_template."</p></body></html>";
				if($cnt_data->eci_contact_email != '')
					$from = $cnt_data->eci_contact_email;
				else
					$from = 'support@eventmanagement.com';
				$headers = 'From: '.$from.'  '."\r\n";
				$headers .= 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$to = $_POST['eci_book_cust_email'];
				//$to = 'vivek.tiwari@himanshusofttech.com';
				$subject = 'Thanks For Booking Ticket for '.$event_data[0]['eci_event_list_name'];
				mail($to,$subject,$em_msg,$headers);
			}
			redirect(base_url().'eventadmin/bookingdetail/1');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}
	
	function get_booking_count(){
		if(isset($this->session->userdata['eci_super_login']))
		{
			$result 		  = $this->Homemodel->event_tbl('select','',array('eci_event_list_sno'=>$_POST['eventid']));
			$result_booking   = $this->Homemodel->event_payment_detail_tbl('select','sum_booking',array('eci_payment_detail_email'=>$_POST['eventid']));
			$str['tableData'] = '';
			if($result[0]['eci_event_list_categories'] !='null' && $result[0]['eci_event_list_max_user'] == 0)
			{		 
				$arr 		= json_decode($result[0]['eci_event_list_catwise_seat_left']);
				$total_Tic  = json_decode($result[0]['eci_event_list_categories']);
				$record		= array();
				$row		= '';
				$cnt		= 0;
				$row 		.= '<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p><label for="eci_event_name">Tickets Details </label><table class="detail_table"><tr>
								<th>Category</th>
								<th>Left Seats</th>
								<th>Cost/Person</th>
								<th>How Many Seats?</th>
								</tr>';
				for($i=0;$i<count($arr);$i++){
					$cnt++;
					$record = explode(',',$arr[$i]);
					$total  = explode(',',$total_Tic[$i]);
					if(isset($record[1]) && $record[1] != 0){
						$seat       = $record[1];
						$seat_input = '<td data-target="'.$total[1].'"><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="" data-counter="'.$cnt.'"  class="noofseat_count"></td>';
					}else{
						$seat       = 'All Tickets are Sold';
						$seat_input = '<td data-target="'.$total[1].'"><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="" data-counter="'.$cnt.'"  Disabled></td>';
					}
					$row .= '<tr>
							<td class="cat_name'.$cnt.'">'.$record[0].'</td>
							<td class="cat_left_seat'.$cnt.'">'.$seat.'</td>
							<td class="cat_tic_price'.$cnt.'">'.$record[2].'</td>
							'.$seat_input.'
						</tr>';
				} 
				$row 			  .='</table>';
				$str['tableData']  = $row;
			}
			echo json_encode(array_merge($result,$result_booking,$str));
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function seosetting()
	{
		checkAdminOnly();
		if(isset($this->session->userdata['eci_super_login']))
		{
		if(isset($_POST['save_seo_settings']))
		{			
			$seo_arr=array(
							'eci_title'		=>	$_POST['eci_title'],
							'eci_meta_key'	=>	$_POST['eci_meta_key'],
							'eci_meta_desc'	=>	$_POST['eci_meta_desc']
						);
			$this->Homemodel->website_setting_tbl('update',array('eci_website_setting_value'=>json_encode($seo_arr)),array('eci_website_setting_name'=>'seo'));
			$data['msg'] = 1;
		}
		else
			$data['msg'] = 0;
		$data['seo_setting'] = $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'seo'));
		$this->load->view('include/header');
		$this->load->view('admin/seosetting',$data);
		$this->load->view('include/footer');
		}
		else
		{
			redirect(base_url().'eventadmin');
		}
	}

	function contactquery()
	{
		checkAdminOnly();
		$data['contactquery_list'] = $this->Homemodel->event_contactus_tbl('select','','');
		$this->load->view('include/header');
		$this->load->view('admin/contactquery',$data);
		$this->load->view('include/footer');
	}
	function subscriber(){
		checkAdminOnly();
		$data['subscriber_list'] = $this->my_model->select_data('*','eci_email_subscriber','','',array('sub_id','DESC'));
		$this->load->view('include/header');
		$this->load->view('admin/subscriber',$data);
		$this->load->view('include/footer');
	}
	
	function contactquery_csv()
	{
		checkAdminOnly();
		$filename = "ContactQueryUsers.xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: text/xls;");
		$csv_data = $this->Homemodel->event_contactus_tbl('select','','');
		echo "S.No. \t Name \t Email \t Subject \t Message \t Date \r\n";
		$sno=0;
		foreach($csv_data as $solo_csv_data)
		{
			$sno++;
			$cnt_data = json_decode($solo_csv_data['eci_contactus_data']);
			echo $sno." \t ".$cnt_data->users_name." \t ".$cnt_data->users_email." \t ".$cnt_data->users_subject." \t ".$cnt_data->users_message." \t ".date("d M Y", strtotime($cnt_data->date))." \r\n";
		}
	}
	
	function booking_csv($eventid)
	{
		checkAdminOnly();
		$filename 	= "BookingDetails.xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: text/xls;");
		$result_booking = $this->Homemodel->event_payment_detail_tbl('select','',array('eci_payment_detail_email'=>$eventid));
		if(!empty($result_booking)) {
			echo "S.No. \t Name \t Email \t Amount \t Ticket Count \t Date \t Mode of Payment \t Status \t Unique Ticket No. \r\n";
			$count=0;
			foreach($result_booking as $solo_result_booking) {
			$payment_sts = $this->Homemodel->event_payment_status_tbl('select','',array('eci_payment_status_num'=>$solo_result_booking['eci_payment_detail_type']));
			$booking_detail=json_decode($solo_result_booking['eci_payment_detail_msg']);
			if(isset($booking_detail->first_name) && isset($booking_detail->last_name))
				$uname=$booking_detail->first_name.' '.$booking_detail->last_name;
			else
				$uname=$booking_detail->first_name;
			if($booking_detail->txn_id != 'Manual')
				$type='Paypal';
			else
				$type='Manual';
			$count++;
			echo $count." \t ".$uname." \t ".$booking_detail->payer_email." \t ".$booking_detail->mc_fee." \t ".$solo_result_booking['eci_payment_detail_ccode']." \t ".$booking_detail->payment_date." \t ".$type."\t ".$payment_sts[0]['eci_payment_status_type']."\t ".$solo_result_booking['eci_payment_detail_tckt_no']." \r\n";
			}
		}
		else
		{
			echo 'No Data Available';
		}
	}

	public function dashboard(){
       checkAdminOnly();
       $data['activeuser']		= $this->Common->getcount('eci_admin',"where status=1 and user_type !=1");
       $data['totalevent']		= $this->Common->getcount('eci_event_list');
       $data['totalbooking']	= $this->Common->select('eci_payment_detail','','SUM(eci_payment_detail_ccode)');
       $data['activeservices']	= $this->Common->getcount('eci_event_service_list','where eci_event_service_list_status=1');
       $data['activeplan']		= $this->Common->getcount('eci_membership_plan','where eci_plan_status=1');
       $data['newplanrequest']	= $this->Common->getcount('eci_user_membership_plan','where eci_user_plan_status="Inactive"');
       $this->load->view('include/header');
	   $this->load->view('admin/dashboard',$data);
	   $this->load->view('include/footer');
	}
    
    public function mailtemplate(){
    	$this->load->view('include/header');
    	$this->load->view('mailtemplate/mailtemplatesetting');
    	$this->load->view('include/footer');
    }

    public function updatemailtemplate(){
    	if(isset($_POST['email_text'])){
    		 $emailtext		     = str_replace(["\n","\r"],"",$_POST['email_text']);
    	  	 $emailtextupdate    = array("eci_website_setting_value"=>$emailtext);
    	  	 $emaillinktextupdate= array("eci_website_setting_value"=>$_POST['email_link_text']);
    	 	  $t=$this->Common->update("eci_website_setting",$emailtextupdate,array('eci_website_setting_name'=>'registrationemail_text'));
    	 	  $l=$this->Common->update("eci_website_setting",$emaillinktextupdate,array('eci_website_setting_name'=>'registrationemail_linktext'));
    	 	  if($t && $l){
    	  	 	echo "Email Template Update Succesfully";die;
    	 	  }else{
    	 	  	echo "Email Template not Updated";die;
    	 	  }
    	}
    	if(isset($_POST['femail_text'])){
    	  		$femailtext				= str_replace(["\n","\r"],"",$_POST['femail_text']);
    	  		$femailtextupdate		= array("eci_website_setting_value"=>$femailtext);
    	   		$femaillinktextupdate	= array("eci_website_setting_value"=>$_POST['femail_link_text']);
    	   		$t=$this->Common->update("eci_website_setting",$femailtextupdate,array('eci_website_setting_name'=>'forgotpwdemail_text'));
    	   		$l=$this->Common->update("eci_website_setting",$femaillinktextupdate,array('eci_website_setting_name'=>'forgotpwdemail_linktext'));
    	   if($t && $l){
    	   	echo "Forget Password Template Update Succesfully";die;
    	   }else{
    	   	echo "Forget Password Template not Updated";die;
    	   }
    	}
    	if(isset($_POST['pemail_text'])){
    	  	$pemailtext		  =	str_replace(["\n","\r"],"",$_POST['pemail_text']);
    	   	$pemailtextupdate = array("eci_website_setting_value"=>$pemailtext);
    	   	$t				  = $this->Common->update("eci_website_setting",$pemailtextupdate,array('eci_website_setting_name'=>'activeplanemail_text'));
    	   if($t ){
    	   	echo "Active plan template update succesfully";die;
    	   }else{
    	   	echo "Active plan template not updated";die;
    	   }
    	}
    	if(isset($_POST['auseremail_text'])){
    	   $auseremailtext		= str_replace(["\n","\r"],"",$_POST['auseremail_text']);
    	   $pemailtextupdate	= array("eci_website_setting_value"=>$auseremailtext);
    	   $t					= $this->Common->update("eci_website_setting",$pemailtextupdate,array('eci_website_setting_name'=>'adduseremail_text'));
    	   if($t){
    	   	echo "Add user template update succesfully";die;
    	   }else{
    	   	echo "Add user template not updated";die;
    	   }
    	}
    }

    public function test(){
    	$noofservices	= '8,9,11';
    	$noofservices	= explode(',', $noofservices);
		foreach ($noofservices as  $value) {
			$service_list[]=$this->Common->select('eci_event_service_list',"where eci_event_service_list_sno='$value'");
		}
	    echo "<pre>";print_r($service_list);
    }
	
	function event_category(){
		$category='';
		if(isset($_POST['eventNo']) && !empty($_POST['eventNo'])){
			$this->my_model->select_data('*','eci_event_list',array('eci_event_list_sno' => $_POST['eventNo']));
			$currencycode = select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");
		}else{
				$currencycode = select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");
				$c=0;
				for($i=0;$i<$_POST['catNo'];$i++){
					$c++;
					$category.='<div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_namecat_err" ></p><label for="eci_no_service">Ticket Category Name<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 catName" id="eci_cat_name" name="eci_cat_name'.$c.'" placeholder="'.$c.' [Category Name]" value=""><p class="help-block">For ex Golden ,Silver, platinum. </p></div><div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_catmem_err" ></p><label for="eci_no_service">Maximum Members<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 maxMember" id="eci_max_users" name="eci_max_users'.$c.'" placeholder="" value="" ><p class="help-block">Booking will be ON by default. </p></div><div class="form-group col-lg-4 eci_max_user"><p class="eci_error" id="eci_event_catamt_err" ></p><label for="eci_book_amount">Amount to be paid by 1 member('.$currencycode.')<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12 bookAmt" id="eci_book_amount" name="eci_book_amount'.$c.'" placeholder="" value="" ><p class="help-block">Do not type currency symbol.</p></div>';
				}
		}
		echo json_encode($category); 
	}
	
	function get_amount(){
		if(isset($_POST)){
			$data = $this->my_model->select_data('eci_event_list_categories,eci_event_list_noofcat','eci_event_list',array('eci_event_list_sno' => $_POST['eventid']));
			if(!empty($data)){
				$arr 	= json_decode($data[0]['eci_event_list_categories']);
				$record	= array();
				$row	= '';
				for($i=0;$i<count($arr);$i++){
					$record   = explode(',',$arr[$i]);
						$row .= '<tr>
								<td>'.$record[0].'</td>
								<td>'.$record[2].'</td>
								</tr>';
				} 
				echo $row;
			}
		}
	}
}
?>
