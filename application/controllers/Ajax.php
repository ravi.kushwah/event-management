<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	function __Construct(){
        parent::__construct();
        error_reporting(E_ALL);
        $_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->load->helper(array('form','url','common'));
        $this->load->library(array('form_validation','session'));
        $this->load->model(array('Homemodel','Profilemodel','Common')); // database model
        $this->load->database();
	}

	function planpaypalform(){
	    $planid             = $_POST['planid'];
		$plandetail         = $this->Common->select('eci_membership_plan',"where eci_plan_id=$planid");
        $payment_detail     = $this->Homemodel->event_payment_detail_tbl('select','wherein',array('1'));
        $item_name          = $plandetail[0]['eci_plan_name'];
        $item_number        = $plandetail[0]['eci_plan_id'];
        $amount             = $plandetail[0]['eci_plan_price'];
        $bussiness          = $payment_detail[0]['eci_payment_detail_email'];
        $notify_url         = base_url().'eventadmin/ipn';
        $currency_code      = $payment_detail[0]['eci_payment_detail_ccode'];
        $unique_id          = randomString(8);
        $user_id            = $this->session->userdata('eci_super_id');

        $data   = array(
                        "eci_plan_id"       => $item_number,
                        "eci_plan_price"    => $amount,
                        "eci_unique_id"     => $unique_id,
                        "eci_user_id"       => $user_id
                       );
			
        $this->Common->data_insert("eci_membership_payment_detail",$data);
	    $custom1 = array(
	                    'for'=>'plan',
		                'unique_id'=>$unique_id
                        );
		$custom     = json_encode($custom1);
		$returnurl  = base_url().'home/thanks/plan';
		              base_url().'assets/front/images/logo_paypal.png';

		echo"<input type='hidden' name='cmd' value='_xclick'>
             <input type='hidden' id='business' name='business' value='$bussiness'>
             <input type='hidden' name='currency_code' value='$currency_code'> 
             <input type='hidden' name='item_name' value='$item_name' id='event_uniqueid'>
             <input type='hidden' name='item_number' value='$item_number' id='event_uniqueid'>
             <input type='hidden' name='notify_url' value='$notify_url'>    
             <input type='hidden' name='return' value='$returnurl'>		
             <input type='hidden' name='amount' value='$amount' id='event_amnt'>
             <input type='hidden' name='custom' value='$custom'/>";
	}

    function check_coupon_code(){
	
	  	$code           = $_POST['code'];
		$codedetail     = $this->Common->select('eci_coupon',"where eci_coupon_code='$code'");
		$checkEvent     = explode(',',$codedetail[0]['eci_coupon_event']);
		$eci_event_list = $this->db_model->select_data('eci_event_list_sno,eci_event_list_amount,eci_event_list_name,eci_event_list_desc','eci_event_list',array('eci_event_list_sno'=>$_POST['eventid']),1,'','')[0];
		
		if(!$codedetail){
	        $response = array('msg'=>'Wrong_Code','type'=>'','discount'=>0);
            	echo json_encode($response);
			die;
		}
        
		if(in_array($_POST['eventid'],$checkEvent)){
			if($codedetail[0]['eci_coupon_status'] == 'Inactive'){
				$response=array("msg"=>"expired",
								"type"=>"",
								"discount"=>0);
				echo json_encode($response);
				die;
			}
			
			$codeexpiredate     = $codedetail[0]['eci_coupon_expire_date'];
			$expireddate        = strtotime($codeexpiredate)-time();
			if($expireddate < 0){
				$response=array("msg"=>"expired",
								"type"=>"",
								"discount"=>0);
				echo json_encode($response);
				die;
			}
			
			$codelimit      = $codedetail[0]['eci_coupon_maximum_usages'];
			if($codelimit!='unlimited'){
				$remaininglimit=$codelimit-$codedetail[0]['eci_coupon_used'];
				if($remaininglimit<=0)
				{
					$response=  array("msg"=>"expired",
									"type"=>"",
									"discount"=>0);
			    	echo json_encode($response);die;
			    }
		    }
		    $response =   array("msg"=>"success","type"=>$codedetail[0]['eci_coupon_type'],"discount"=>$codedetail[0]['eci_coupon_amount']);
	
    		if($response['type']=="flat"){
    			$new_total=$eci_event_list['eci_event_list_amount']-$response['discount'];
    
    		}
    		if($response['type']=="commision"){
    			$deduct_amount=($response['discount']/100)*$eci_event_list['eci_event_list_amount'];
    			$new_total=$eci_event_list['eci_event_list_amount']-$deduct_amount;
    
    		}
		
        	echo json_encode($response);
	    }else{
    		$response=array("msg"=>"Wrong_Code","type"=>"","discount"=>0);
    		echo json_encode($response);
    	
        }
	}
    
	function do_booking(){

		$coupon_code        = $_POST['couponcode'];
	 	$eventid            = $_POST['eventid'];
	 	$noofticket         = $_POST['em_person'];
	 	$bookingtype        = $_POST['bookingtype'];
	 	$cat_total_amount   = $_POST['cat_amount'];
	 	$cat_seat_left      = $_POST['cat_Sleft'];
	 	$email      = $_POST['email'];
   
		$event_detail       = $this->Common->select('eci_event_list',"where eci_event_list_sno='$eventid'",'eci_event_list_amount,eci_event_list_name,eci_event_pro_price_id,eci_event_list_desc');
     
		$eventprice         = $event_detail[0]['eci_event_list_amount'];
		if($eventprice == 'FREE')
		{
			$eventprice = 0;
		}
		$eventname      = $event_detail[0]['eci_event_list_name'];
		$initialtotal   = $eventprice*$noofticket;
		$newtotal       = $initialtotal;
		if(!empty($cat_total_amount) && $cat_total_amount != 0)
		{
			$initialtotal  = $cat_total_amount;
			$newtotal      = $cat_total_amount;
		}
		if($coupon_code != "")
		{
			$codedetail   =  $this->Common->select('eci_coupon',"where eci_coupon_code='$coupon_code'");
		
         	if($codedetail)
         	{
				$codestatus     = $codedetail[0]['eci_coupon_status'];
				$codeexpiredate = $codedetail[0]['eci_coupon_expire_date'];
				$maximumusages  = $codedetail[0]['eci_coupon_maximum_usages'];
				$codelimit      = $codedetail[0]['eci_coupon_maximum_usages'];
				$remaininglimit = 1;
				$expireddate    = strtotime($codeexpiredate)-time();
				if($codelimit != 'unlimited')
				{
					$remaininglimit = $codelimit-$codedetail[0]['eci_coupon_used'];
				}
				if($codestatus == 'Active' && $remaininglimit >0 && $expireddate >0)
				{
					$codetype   = $codedetail[0]['eci_coupon_type'];
					if($codetype == 'flat'){
						$newtotal   = $initialtotal-$codedetail[0]['eci_coupon_amount'];
						$couponused = $codedetail[0]['eci_coupon_used']+1;
                    	$this->Common->update('eci_coupon',array('eci_coupon_used'=>$couponused),array('eci_coupon_code'=>$coupon_code));
					}
					if($codetype == 'commision'){
						$newtotal      = $initialtotal-$codedetail[0]['eci_coupon_amount'];
						$deduct_amount = ($codedetail[0]['eci_coupon_amount']/100)*$initialtotal;
                    	$newtotal      = $initialtotal-$deduct_amount;
                    	$couponused    = $codedetail[0]['eci_coupon_used']+1;
                    	$this->Common->update('eci_coupon',array('eci_coupon_used'=>$couponused),array('eci_coupon_code'=>$coupon_code));
					}
        		   	$curr = $this->db_model->select_data('eci_payment_secret_key,eci_payment_detail_ccode,eci_payment_publish_key','eci_payment_detail','',1,'','')[0];
            		$allProd = stripeGetAllProduct($curr['eci_payment_secret_key']);
            	    $proId ="";
            	    foreach($allProd as $pv){
            	        if($pv['name'] == $eventname){
            	            $proId .= $pv['id'];
            	            break;
            	        }
            	    }
        	    	$temp_price_id = stripeProductUpdate($curr['eci_payment_secret_key'],$proId,$newtotal,$curr['eci_payment_detail_ccode']);
        	        $temp_id   = $temp_price_id['id'];
				}
         	}
		}else{
		    $temp_id = $event_detail[0]['eci_event_pro_price_id'];
		}
    
		$tid            = $this->my_model->insert_data('eci_test',array('response'=> $cat_seat_left));
        $personname     = explode(",",$_POST['person_name']);
        $jsonpersonname = json_encode($personname);
        $custom1        = array('for'=>'event',
                                    'personname'=>$jsonpersonname,'coupon_code'=>$coupon_code,'test_id'=>$tid);
        $custom         = json_encode($custom1);
        $dataresponce   = array("event_amount"=>$newtotal,
                                    "event_id"=>$eventid,
                                    "ticket_count"=>$noofticket,"paypal_custom"=>$custom, 'cat_seat_left'=>$cat_seat_left);
        if($bookingtype == 'stripe')
        {
			$newtotal                   =  $newtotal*100;
			$unique_id                  =  'ev'.$eventid.'ent'.$noofticket;
			$_SESSION['stripeSession']  =  $unique_id.'@#'.$newtotal.'@#'.$custom.'@#'.$personname[0].'@#'.$eventname.'@#'.$cat_seat_left.'@#'.$email;
			$publishableKey             =  select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_publish_key");
			$eci_payment_secret_key             =  select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_secret_key");
			$stripe_form                = '<form action="'.base_url().'eventadmin/payNow" method="POST" name="stripe_form" id="stripe_form">
												<input type="hidden" name="stripe_publishKey" value="'.$publishableKey.'" />
												<input type="hidden" name="stripe_secretkey" value="'.$eci_payment_secret_key.'" />
												<input type="hidden" name="stripePriceId" value="'.$temp_id.'" />
												<button type="submit" id="checkout-button" class="stripe-button btn PayNow" data-key="'.$publishableKey.'"  data-name="'.$eventname.'" data-description="'.$eventname.'" data-amount="'.$newtotal.'" data-locale="auto">Pay Now</button>
											</form>											
											';
			$stripe_responce =   array("stripe_form"=>$stripe_form);
			echo json_encode($stripe_responce);	
		}else{
			echo json_encode($dataresponce);	
		}
	 }

   function paymentmode(){
      $for          = $_POST['purpose'];
      $method       = ltrim($_POST['method'],",");
      $updatedata   = array('eci_website_setting_value'=>$method);
      if($for == 'plan'){
              $this->Common->update('eci_website_setting',$updatedata,array('eci_website_setting_name'=>'plan_payment_mode'));
              echo "Plan payment mode has been updated";
              die;
      }
      if($for == 'event'){
              $this->Common->update('eci_website_setting',$updatedata,array('eci_website_setting_name'=>'event_payment_mode'));
              echo "Event payment mode has been updated";
              die;
      }
   }
   
   function share_email(){
	   if($_POST){
			$email      = $_POST['share_email'];
			$checkmail  = $this->Common->select('eci_email_subscriber',"where sub_email='$email'");
			if(!$checkmail)
			{
                $this->Common->data_insert('eci_email_subscriber',array('sub_email'=>$email));
                echo 1 ;die;
			}else{
				echo 2;die;
			}		
	             }
   }
   function SendMail($from,$to,$subject,$body){
	$smtp_encryption = 'tsl';
	$smtp_port = '587';
	$smtp_user = 'omprakash.jha@himanshusofttech.com';
	$smtp_password = 'GNmf3zKrOBHtdJ17';
	$smtp_server  = 'Smtp-relay.sendinblue.com';
	
	$this->load->library('email'); // load library 
	
	 $config = array(
		'protocol' => 'SMTP',
		'smtp_host' => $smtp_server,
		'smtp_port' => $smtp_port,
		'smtp_user' => $smtp_user,
		'smtp_pass' => $smtp_password,
		'mailtype'  => 'html', 
		'charset'   => 'utf-8',
		'newline'   =>  "\r\n",
		'smtp_crypto'   =>  $smtp_encryption,
		// 'SMTPAuth'   => true,
	);
	$this->email->initialize($config);
	$this->email->from($from);
	$this->email->to($to);
	$this->email->subject($subject);
	$this->email->message($body);
	if(! $this->email->send()){
		$c =  $this->email->print_debugger();
		return $ress = array('status'=>1,'message'=>$c);
	}else{
		return $ress = array('status'=>1,'message'=>'Mail has been sent successfully.');
	}

}
    function testing(){
        	$curr = $this->db_model->select_data('eci_payment_secret_key,eci_payment_detail_ccode,eci_payment_publish_key','eci_payment_detail','',1,'','')[0];
    // 		$allProd = stripeGetAllProduct($curr['eci_payment_secret_key']);
    // 	    $proId ="";
    // 	    foreach($allProd as $pv){
    // 	        if($pv['name'] =="cyber security workshop"){
    // 	            $proId .= $pv['id'];
    // 	            break;
    // 	        }
    // 	    }
	   // 	$temp_price_id = stripeProductUpdate($curr['eci_payment_secret_key'],$proId,50,$curr['eci_payment_detail_ccode']);
	   	$allProd = priceRetrieve();
    	    echo "<pre>";print_r($allProd);
    }
}
