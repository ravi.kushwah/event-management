<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __Construct()
	{
		parent::__construct();
		$_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->load->helper(array('form', 'url','text','common'));
		$this->load->library(array('form_validation','session'));
		$this->load->model(array('Homemodel','Profilemodel','Common'));
		$this->load->database();
	}
	function index(){
		if(!isset($this->session->userdata['eci_super_login']))
		{
		$data['contactus_setting'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting']		= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['auth_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('admin/logins',$data);
		$this->load->view('home/common/footer',$data);	  
		}
		else
		{
			redirect(base_url().'eventadmin/addevent');
		}
	}
	function register(){
		$data['msg']		=	"";
		if($_POST){
			$name			=	$this->input->post('runame');
	     	$email			=	$this->input->post('ruemail');
		    $password		=	md5($this->input->post('rupassword'));
			$activationcode	=	mt_rand();
			$where 			= " where eci_admin_email = '$email'";
			$result 		= $this->Common->select('eci_admin',$where);
			if(empty($result)){
	    		$contact_sett 		= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			    
				$cnt_data 			= json_decode($contact_sett[0]['eci_contactus_data'],true);
                // echo "<pre>";print_r($cnt_data['eci_contact_email']);die;
				$data=array(
								"eci_admin_name"	=> $name,
								"eci_admin_email"	=> $email,
								"eci_admin_pwd"		=> $password,
								"user_type"			=> 2,
								"status"			=> 0,
								"activationcode"	=> $activationcode
				);
			    $result 			= $this->Common->data_insert('eci_admin', $data);
			    $additional_data	= array('eci_admin_sno'=>$result);
			    $result 			= $this->Common->data_insert('eci_admin_additional_info', $additional_data);
		
				if($cnt_data->send_contact_query == 'no')
					$admin_email	=	$cnt_data->eci_contact_new_email;
				else
					$admin_email	=	$cnt_data->eci_contact_email;

				$email_data	=	array('username'=>$name,'activationcode'=>$activationcode);
				$message	=	getemailmsg('registration',$email_data);
				$headers 	= 	"MIME-Version: 1.0" . "\r\n";
				$headers 	.=  "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 	.=  'From:' .$admin_email. "\r\n";
				
				$res = $this->SendMail($cnt_data['eci_contact_email'],$email,'User Registration',$message);	
				// $send		=	mail($email,'User Registration ',$message,$headers );
				if($res){
					$this->session->set_flashdata('success', "Thank you for registration. An email has been sent to your email address.Please check your mail and use the enclosed link to finish registration");
				}else{
					$this->session->set_flashdata('success', "Thank you for registration. An email has been sent to your email address.Please check your mail and use the enclosed link to finish registration");
				}
				redirect(base_url().'user');
			}else{
				$this->session->set_flashdata('error', "This e-mail address already exists");
				redirect(base_url().'user');
			}
		}else{
		redirect(base_url());
	   }
	}

	public function verification($verifier=""){
		if($verifier!=''){
		        $where  = " where activationcode = '$verifier'";
				$result = $this->Common->select('eci_admin',$where);
				if(!empty($result)){
					$ver_user_id = $result[0]['eci_admin_sno'];
					$where 		 = array('eci_admin_sno' => $ver_user_id);
					$update_data = array('activationcode' => '',
					                     'status' 		  => 1);
				    $this->Common->update('eci_admin',$update_data,$where);
					redirect(base_url().'eventadmin/login/success');
				}else{
					$this->session->set_flashdata('error', "Invalid verification code");
				    redirect(base_url());
					}
	      	}else{
				redirect(base_url());
			}
	}

	public function users(){
		checkAdminOnly();
		$this->db->select('u.eci_admin_sno,u.eci_admin_name,u.eci_admin_email,u.status,up.eci_membership_plan_id');
		$this->db->distinct();
		$this->db->from('eci_admin as u');
		$this->db->join('eci_user_membership_plan as up', 'u.eci_admin_sno=up.eci_user_id');
		$this->db->where('u.user_type !=', 1);
		$this->db->group_by('up.eci_user_id');
		$q					=	$this->db->get();
		$data['user_list']	=	$q->result_array();
		$this->load->view('include/header');
		$this->load->view('user/users',$data);
		$this->load->view('include/footer');
	}

	public function single_user($userid=""){
		checkAdminOnly();
     	if($userid!=""){
     		$data['user_detail']=$this->Profilemodel->getsingleuser($userid);
     		$this->load->view('include/header');
			$this->load->view('user/single_user',$data);
			$this->load->view('include/footer');
     	}else{
     		redirect(base_url().'user/users');
    	 }
	}

	public function add_user(){
		checkAdminOnly();
		$data['msg']		=	"";
		$data['alertclass']	=	"";
		if($_POST){
			$name		=	$this->input->post('eci_user_name');
	     	$email		=	$this->input->post('eci_user_email');
		    $password	=	md5($this->input->post('eci_user_password'));
			$where 		= " where eci_admin_email = '$email'";
			$result 	= $this->Common->select('eci_admin',$where);
			
			if(empty($result)){
				$data=array(
						"eci_admin_name"	=> $name,
						"eci_admin_email"	=> $email,
						"eci_admin_pwd"		=> $password,
						"user_type"			=> 2,
						"status"			=> 1
				);
			    $newuser_id 	 = $this->Common->data_insert('eci_admin', $data);
			    $additional_data = array('eci_admin_sno'=>$newuser_id);
			    $result 		 = $this->Common->data_insert('eci_admin_additional_info', $additional_data);
			    $plan_data		 = array('eci_user_id'				 => $newuser_id,
											"eci_membership_plan_id" => $_POST['eci_user_plan_name'],
											'eci_user_plan_status'	 => 'Active',
											'startdate'				 =>	time()
											);
			    $this->Common->data_insert('eci_user_membership_plan', $plan_data);
				if(isset($_POST['send_cust_email']))
				{
				$contact_sett 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
				$cnt_data 		= json_decode($contact_sett[0]['eci_contactus_data']);
				if($cnt_data->send_contact_query == 'no')
					$admin_email	=	$cnt_data->eci_contact_new_email;
				else
					$admin_email	=	$cnt_data->eci_contact_email;
				
				$email_data	 =	array('useremail'=>$email,'username'=>$name,'password'=>$this->input->post('eci_user_password'));
				$message	 =	getemailmsg('addnewuser',$email_data);
				$headers 	 = 	"MIME-Version: 1.0" . "\r\n";
				$headers 	.=	"Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 	.= 	'From:' .$admin_email. "\r\n";
				
				$res = $this->SendMail($contact_sett[0]['eci_contact_email'],$email,'Registration detail',$message);	
				// mail($email,'Registration detail ',$message,$headers );
			}
				$data['msg']	    = "User Added Succesfully";
		        $data['alertclass'] = "alert-success";
			}else{
				$data['msg']		= "This email already exists";
		        $data['alertclass']	= "alert-danger";
			}
		}
		$data['plan']	=	$this->Common->select('eci_membership_plan');
		$this->load->view('include/header');
		$this->load->view('user/add_user',$data);
		$this->load->view('include/footer');
	}

	public function forget_password(){
		$data['msg']="";
		if($_POST){
			$email		= $this->input->post('uemail');
			$checkemail	= $this->Common->select("eci_admin","where eci_admin_email='$email'");
			if($checkemail){
           	$resetcode=mt_rand();
           	$this->Common->update("eci_admin",array('eci_admin_resetcode'=>$resetcode),array('eci_admin_email'=>$email));
 			$contact_sett = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
			$cnt_data = json_decode($contact_sett[0]['eci_contactus_data'],true);
				if($cnt_data['send_contact_query'] == 'no')
					$admin_email=$cnt_data['eci_contact_new_email'];
				else
					$admin_email=$cnt_data['eci_contact_email'];
				$email_data	 =	array('username'=>$checkemail[0]['eci_admin_name'],'resetcode'=>$resetcode);
				$message	 =	getemailmsg('forgetpassword',$email_data);
				$headers 	 =  "MIME-Version: 1.0" . "\r\n";
				$headers 	.=  "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 	.=  'From:' .$admin_email. "\r\n";
				
				$res = $this->SendMail($cnt_data['eci_contact_email'],$email,'Reset Password',$message);
				// mail($email,'Reset Password ',$message,$headers );
			   	$this->session->set_flashdata('error', "Password reset link has been sent to your email address.Please check");
			}else{
				$this->session->set_flashdata('error', "This detail is not with us");
			}
		}
		$data['forget']				= 1;
		$data['contactus_setting']  = $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['auth_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('user/forget_password',$data);
		$this->load->view('home/common/footer',$data);	  
	}

	public function resetpassword($recetcode=""){
		$data['resetcode']=$recetcode;
		if($_POST){
			$resetcode	=	$this->input->post('eci_resetcode');
			$password	=	$this->input->post('upassword');
			$password	=	md5($password);
			$chk=$this->Common->select("eci_admin","where eci_admin_resetcode='$resetcode'");
			if($chk){
				$updatedata=array('eci_admin_pwd'=>$password,
								  'eci_admin_resetcode'=>"");
		        $this->Common->update("eci_admin",$updatedata,array('eci_admin_resetcode'=>$resetcode));
		        redirect(base_url().'eventadmin/login/passwordreset');
			}else{
				$this->session->set_flashdata('error', "Verification code is invalid.Please enter correct verification code");
			}
		}
		$data['reset']				= 1;
		$data['contactus_setting'] 	= $this->Homemodel->event_contactus_tbl('select','',array('eci_contactus_sno'=>1));
		$data['social_setting'] 	= $this->Homemodel->website_setting_tbl('select','',array('eci_website_setting_name'=>'social'));
		$data['auth_active']		= 1;
		$this->load->view('home/common/header',$data);
		$this->load->view('user/forget_password',$data);
		$this->load->view('home/common/footer',$data);	  
	}

	function SendMail($from,$to,$subject,$body){
	  
	    $smtp_encryption = 'tsl';
        $smtp_port = '587';
        $smtp_user = 'omprakash.jha@himanshusofttech.com';
        $smtp_password = 'GNmf3zKrOBHtdJ17';
        $smtp_server  = 'Smtp-relay.sendinblue.com';
        
        $this->load->library('email'); // load library 
        
         $config = array(
            'protocol' => 'SMTP',
    		'smtp_host' => $smtp_server,
    		'smtp_port' => $smtp_port,
    		'smtp_user' => $smtp_user,
    		'smtp_pass' => $smtp_password,
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'newline'   =>  "\r\n",
            'smtp_crypto'   =>  $smtp_encryption,
            // 'SMTPAuth'   => true,
        );
        $this->email->initialize($config);
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($body);
        if(! $this->email->send()){
    		$c =  $this->email->print_debugger();
    	    return $ress = array('status'=>1,'message'=>$c);
    	}else{
	    	return $ress = array('status'=>1,'message'=>'Mail has been sent successfully.');
    	}
    
	}
}
?>
