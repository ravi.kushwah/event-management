<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Profilemodel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function getuserdetail(){
		$user_id    =   $this->session->userdata('eci_super_id');
		$this->db->select('u.*,ui.*');
		$this->db->from('eci_admin as u');
        $this->db->join('eci_admin_additional_info as ui', 'u.eci_admin_sno=ui.eci_admin_sno','left');
        $this->db->where('u.eci_admin_sno',$user_id);
        $q  =   $this->db->get();
        return $q->row_array();
    }

    function getallusers(){
    	$this->db->select('u.eci_admin_sno,u.eci_admin_name,u.eci_admin_email,u.status,up.eci_membership_plan_id');
		$this->db->from('eci_admin as u');
        $this->db->join('eci_user_membership_plan as up', 'u.eci_admin_sno=up.eci_user_id','left');
        $this->db->where('u.user_type !=', 1);
        $q  =   $this->db->get();
        return $q->result_array();
    }

	function getsingleuser($user_id){
		$this->db->select('u.eci_admin_sno,u.eci_admin_name,u.eci_admin_email,u.status,ui.eci_admin_contact,ui.eci_admin_address,ui.eci_admin_gender,up.eci_membership_plan_id,up.eci_user_plan_status,up.startdate,up.eci_user_plan_id');
		$this->db->from('eci_admin as u');
        $this->db->join('eci_admin_additional_info as ui', 'u.eci_admin_sno=ui.eci_admin_sno','left');
        $this->db->join('eci_user_membership_plan as up', 'u.eci_admin_sno=up.eci_user_id','left');
        $this->db->where('u.eci_admin_sno',$user_id);
        $q=$this->db->get();
        return $q->row_array();
    }
}
