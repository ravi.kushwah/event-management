<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Homemodel extends CI_Model
{

	function __construct()
	{
		$this->load->database();
		parent::__construct();
	}

	function admin_login($whr,$mode='',$data='')
	{
	if($mode=='') {
		$this->db->select('*');
		$this->db->from('eci_admin');
		$this->db->where($whr);
		$rs=$this->db->get();
		return $rs->result_array();
	}
	elseif($mode=='update')
	{
		$this->db->where($whr);
		$this->db->update('eci_admin',$data);
	}
	
	}
	
		// event table data
	function event_tbl($mode,$data,$whr,$limit='',$fet='')
	{
		if($mode=='select') {
			$this->db->select('*');
			$this->db->from('eci_event_list');
			if($whr!='')
				$this->db->where($whr);
			if($limit!='')
				$this->db->limit($limit);
			if($data=='recent')
			{
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				$this->db->where('eci_event_list_timestamp <',$timestamp);
			}
			elseif($data=='upcoming')
			{
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				if($fet=='no'){
				$this->db->where('eci_event_list_featured !=',1);
				}
				$this->db->where('eci_event_list_timestamp >',$timestamp);
				$this->db->where('eci_event_list_upcoming !=',0);
				$this->db->join('eci_event_image_list', 'eci_event_image_list.eci_event_image_list_sno = eci_event_list.eci_event_list_img','left');
				$this->db->order_by("eci_event_list_timestamp","asc");
				
			}
			$this->db->order_by("eci_event_list_sno","desc");
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_event_list',$data);
			return $this->db->insert_id();
		}
		elseif($mode=='update'){
			if($whr!='')
				$this->db->where($whr);
			$this->db->update('eci_event_list',$data);
		}
		elseif($mode=='delete'){
			$this->db->delete('eci_event_list',$whr);
		}
	}
	// event service table data
	function event_service_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			$this->db->select('*');
			$this->db->from('eci_event_service_list');
			if($whr!='')
				$this->db->where($whr);
			$this->db->order_by("eci_event_service_list_name", "asc");
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_event_service_list',$data);
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_event_service_list',$data);
		}
		elseif($mode=='delete'){
			$this->db->delete('eci_event_service_list',$whr);
		}
				
	}

	// event image table data
	function event_image_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			$this->db->select('*');
			$this->db->from('eci_event_image_list');
			
			if($data=='arr')
				$this->db->where_in('eci_event_image_list_sno', $whr);
			elseif($whr!='')
				$this->db->where($whr);
			if($this->session->userdata('user_type')==2){
			 $userid=$this->session->userdata('eci_super_id');
			 $this->db->where('eci_admin_sno',$userid);
			}
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_event_image_list',$data);
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_event_image_list',$data);
		}
	}

	// website setting table data
	function website_setting_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			$this->db->select('*');
			$this->db->from('eci_website_setting');
			if($whr!='')
				$this->db->where($whr);
			if($data=='bg')
			{
				$this->db->where('eci_website_setting_name !=','social');
				$this->db->where('eci_website_setting_name !=','seo');
				$this->db->join('eci_event_image_list', 'eci_event_image_list.eci_event_image_list_sno = eci_website_setting.eci_website_setting_value');
			}
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_website_setting',$data);
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_website_setting',$data);
		}
	}
	// contact us table data
	function event_contactus_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			$this->db->select('*');
			$this->db->from('eci_contactus');
			if($whr!='')
				$this->db->where($whr);
			else
				$this->db->where('eci_contactus_sno !=',1);
				
			$this->db->order_by("eci_contactus_sno", "desc");
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_contactus',$data);
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_contactus',$data);
		}
				
	}
	// payment table data
	function event_payment_detail_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			if($data == 'sum_booking')
				$this->db->select('SUM(eci_payment_detail_ccode) as tot_count');
			else
				$this->db->select('*');
			$this->db->from('eci_payment_detail');
			if($data != 'wherein'){
				if($whr!='')
					$this->db->where($whr);
			}
			else
			{
				$this->db->where_in('eci_payment_detail_sno',$whr);
			}
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_payment_detail',$data);
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_payment_detail',$data);
		}
	}
	// payment status table data
	function event_payment_status_tbl($mode,$data,$whr)
	{
		if($mode=='select') {
			$this->db->select('*');	
			$this->db->from('eci_payment_status');
			if($whr!='')
				$this->db->where($whr);
			$rs=$this->db->get();
			return $rs->result_array();
		}
		elseif($mode=='update'){
			$this->db->where($whr);
			$this->db->update('eci_payment_status',$data);
		}
		elseif($mode=='insert'){
			$this->db->insert('eci_payment_status',$data);
		}
				
	}

	function getuserpaypalemail($eventid){
     $sql = "SELECT eci_admin.eci_user_paypal_email FROM `eci_event_list` join eci_admin on eci_event_list.eci_event_user_id=eci_admin.eci_admin_sno WHERE eci_event_list.eci_event_list_sno=$eventid";
     $query = $this->db->query($sql);
     $result = $query->row_array();
     return $result;
   }

  function getsellcount($event_id,$payment_mode=""){
    //   print_r($event_id);
    //   die();
  	if($payment_mode==""){
         $sql = "SELECT sum(eci_payment_detail_ccode) as sellcount FROM `eci_payment_detail` where eci_payment_detail_sno=$event_id and eci_payment_detail_type=4";
  	}else{
		$sql = "SELECT
					SUM(eci_event_list_amount) as sellcount
				FROM
					eci_payment_detail
				JOIN eci_event_list ON
					(
					eci_payment_detail.eci_payment_detail_email = eci_event_list.eci_event_list_sno
					)
				WHERE
					eci_payment_detail.eci_payment_detail_email = $event_id AND eci_payment_detail.eci_payment_booking_source = '$payment_mode'";
  	}
    $query  = $this->db->query($sql);
    $result = $query->row_array();
    if($result['sellcount'] > 0){
    	return $result['sellcount'];
    }else{
    	return 0;
    }
  }
	
	public function get_image_name($sec=""){
		$sql = "SELECT i.eci_event_image_list_name from eci_website_setting as s join eci_event_image_list as i on s.eci_website_setting_value=i.eci_event_image_list_sno WHERE s.eci_website_setting_name='$sec'";
		$query  = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}
	public function countAll($tbl_name,$where='',$like='',$where1='',$likes='',$join_array='',$group='',$or_like='',$where_in=''){
        $this->db->from($tbl_name);
        if($where !=''){
            $this->db->where($where);
        }
        if($like!=''){
            $this->db->or_like($like);
        }
		
        if($where1!=''){
            $this->db->where($where1);
        }
        if($where_in !='' ){
           
           $this->db->where_in($where_in[0],explode(',',$where_in[1]));
        }
        
		
		
		if($likes != ""){
			$like_key = explode(',',$likes['0']);
			$like_data = explode(',',$likes['1']);
			for($i='0'; $i<count($like_key); $i++){
				if($like_data[$i] != ''){
					$this->db->like($like_key[$i] , $like_data[$i]);
				}
			} 
		}

		if($or_like != ""){
			if(is_array($or_like)){
				foreach($or_like as $like){
					$this->db->or_like($like[0] , $like[1]);
				}
			}
		}

		if($join_array != ''){
			if(in_array('multiple',$join_array)){
				foreach($join_array['1'] as $joinArray){
					
					if(isset($joinArray[2])){
						$this->db->join($joinArray[0], $joinArray[1] , $joinArray[2]);
					}else{
						$this->db->join($joinArray[0], $joinArray[1]);
					}
					
				}
			}else{
				if(isset($join_array[2])){
					$this->db->join($join_array[0], $join_array[1] , $join_array[2]);
				}else{
					$this->db->join($join_array[0], $join_array[1]);
				}
				
			}
		}
		if($group != ""){
			$this->db->group_by($group);
		}
        return $this->db->count_all_results();
		die();
	}
	
    function aggregate_data($table , $field_nm , $function , $where = NULL , $join_array = NULL){
		$this->db->select("$function($field_nm) AS MyFun");
        $this->db->from($table);
		if($where != ''){
			 $this->db->where($where);
		}
		
		if($join_array != ''){
			if(in_array('multiple',$join_array)){
				foreach($join_array['1'] as $joinArray){
					$this->db->join($joinArray[0], $joinArray[1]);
				}
			}else{
				$this->db->join($join_array[0], $join_array[1]);
			}
		}
		
        $query1 = $this->db->get();
		
        if($query1->num_rows() > 0){ 
			$res = $query1->row_array();
			return $res['MyFun'];													
        }else{
			return array();
		}  
		die();  
	}
	function get_users($mode,$data,$whr,$limit='',$start,$fet='')
	{
		
			if($data=='recent')
			{
				$date = new DateTime();
				$timestamp = $date->getTimestamp();				
				$this->db->where('eci_event_list_timestamp <',$timestamp);
			}
			elseif($data=='upcoming')
			{
				$date = new DateTime();
				$timestamp = $date->getTimestamp();
				if($fet=='no'){
				$this->db->where('eci_event_list_featured !=',1);
				}
				$this->db->where('eci_event_list_timestamp >',$timestamp);
				$this->db->where('eci_event_list_upcoming !=',0);
				$this->db->join('eci_event_image_list', 'eci_event_image_list.eci_event_image_list_sno = eci_event_list.eci_event_list_img','left');
				$this->db->order_by("eci_event_list_timestamp","asc");
				
			}
			$this->db->limit($limit,$start);
			$this->db->order_by("eci_event_list.eci_event_list_sno","desc");
			
			$query=$this->db->get("eci_event_list");
			return $query->result_array();
	}
}
?>
