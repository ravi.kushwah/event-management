<?php

class Common extends CI_Model {

   
    function select($table, $where ='',$coloumn = '*'){
        $sql = "SELECT $coloumn FROM $table";
		if(!empty($where)){
		 $sql .= " $where";
		}
		$query = $this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
    }
	
	public function update($mytable,$data,$where)
	{
		if(is_array($where)){
			foreach ($where as $key => $value){
			  $this->db->where($key, $value);
			}
		}
		$this->db->update($mytable, $this->db->escape_str($data));
		return true;
	}

	public function data_insert($table,$data){
		$query = $this->db->insert($table, $data);
		$id = $this->db->insert_id();
		return $id;
	}

	public function getcount($table,$where=""){
		$sql = "SELECT * FROM $table";
		if(!empty($where)){
		 $sql .= " $where";
		}
		$query = $this->db->query($sql);
        return $query->num_rows();
	}

	public function delete($mytable,$where)
	{
		if(is_array($where)){
			foreach ($where as $key => $value){
			  $this->db->where($key, $value);
			}
		}
		$this->db->delete($mytable);
		return true;
	}
}
