<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Manage Coupon</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	  
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="phone">Name</th>
                  <th data-toggle="true">Code</th>
                  <th data-toggle="phone">Amount</th>
                  <th data-hide="phone">Type</th>
                  <th data-hide="phone">Expire Date</th>
                  <th data-hide="phone">Usages Limit</th>
                  <th data-hide="phone">Status</th>
                  <th data-hide="phone">Action</th>
                </tr>
              </thead>
              <tbody>

			  <?php if(!empty($coupon_list)) { 
			  
			  foreach($coupon_list as $solo_coupon) {
			 
			  ?>
                <tr>
                  <td><?php echo $solo_coupon['eci_coupon_name'];?> </td>
				          <td><?php echo $solo_coupon['eci_coupon_code'];?> </td>
                  <?php $type='FLAT';
                  if($solo_coupon['eci_coupon_type']=='commision'){
                    $type='PERCENTAGE';
                  }
                  ?>
				          <td><?php echo $solo_coupon['eci_coupon_amount'];
                  if($type=='PERCENTAGE') echo ' %';
                  ?> </td>
                  
                  <td><?php echo $type;?> </td>
                 <td><?php echo $solo_coupon['eci_coupon_expire_date'];?></td>
                  <td><?php echo $solo_coupon['eci_coupon_maximum_usages'];?> </td>
                   <td><?php echo $solo_coupon['eci_coupon_status'];?> </td>
                  <td>
                  <span><a href="<?php echo base_url();?>event/edit_coupon/<?php echo $solo_coupon['eci_coupon_id'];?>" title="EDIT"><img src="<?php echo base_url();?>assets/back/images/icons/edit.png" alt="EDIT" /></a></span>
				  
				  
                  <span class="hide"><a href="<?php echo base_url();?>event/delete_coupon/<?php echo $solo_coupon['eci_coupon_id'];?>" title="DELETE"><img src="<?php echo base_url();?>assets/back/images/icons/delete.png" alt="DELETE" onclick="return confirm('Are you sure you want to delete this coupon?');" /></a></span>
                  </td>
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
          
          </div>
				
				
				
				
	   </div>
        </div>
    </div>