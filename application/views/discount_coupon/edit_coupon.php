    <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Update Coupon</h4>
                    <hr>
					<?php if(isset($msg)): ?>
					<div class="alert alert-danger">
					  <?php echo $msg; ?>
					</div>
					<?php endif;?>
                    </div>
                </div>
				
		<form role="form" action="<?php echo base_url()?>event/edit_coupon/<?php echo $coupan_detail['eci_coupon_id']; ?>" method="post">
				
                  <div class="col-lg-8">
                  <div class="form-group">
                  	<p class="eci_error" id="eci_coupon_name_err"> </p>
                    <label for="eci_service_name">Coupon Name <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_coupon_name" name="eci_coupon_name" placeholder="Enter Coupon Name" value="<?php echo $coupan_detail['eci_coupon_name']; ?>">
                  </div>
                  </div>
                <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_code_err"> </p>
                    <label for="eci_service_name">Coupon Code <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_coupon_code" name="eci_coupon_code" placeholder="Enter Coupon Code" readonly value="<?php echo $coupan_detail['eci_coupon_code'];?>">
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_desc_err"> </p>
                    <label for="eci_coupon_desc">Coupon Description <span class="eci_req_star">*</span></label>
                     <textarea rows="3" class="form-control" id="eci_coupon_desc" name="eci_coupon_desc"> <?php echo $coupan_detail['eci_coupon_description']; ?></textarea>
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_amount_err"> </p>
                    <label for="eci_service_name">Coupon Amount <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_coupon_amount" name="eci_coupon_amount" placeholder="Number Only" value="<?php echo $coupan_detail['eci_coupon_amount'];?>">
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_type_err"> </p>
                    <label for="eci_service_name">Coupon Type <span class="eci_req_star">*</span></label>
                    <select class="form-control" id="eci_coupon_type" name="eci_coupon_type">
                        <option value="flat" <?php if($coupan_detail['eci_coupon_type']=='flat') echo 'selected'; ?>>FLAT</option>
                        <option value="commision" <?php if($coupan_detail['eci_coupon_type']=='commision') echo 'selected'; ?>>PERCENTAGE</option>
                    </select>
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_expire_date"> </p>
                    <label for="eci_service_name">Coupon Expire Date <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_coupon_expir_date" Placeholder="Expire Date" name="eci_coupon_expir_date" value="<?php echo $coupan_detail['eci_coupon_expire_date'];?>">
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_usages_err"> </p>
                    <label for="eci_service_name">Maximum Usages<span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_coupon_usages_limit" name="eci_coupon_usages_limit" placeholder="Usages Limit" value="<?php echo $coupan_detail['eci_coupon_maximum_usages'];?>">
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_coupon_status_err"> </p>
                    <label for="eci_service_name">Coupon Status <span class="eci_req_star">*</span></label>
                    <select class="form-control" id="eci_coupon_status" name="eci_coupon_status">
                        <option value="Active" <?php if($coupan_detail['eci_coupon_status']=='Active') echo 'selected'; ?>>Active</option>
                        <option value="Inactive" <?php if($coupan_detail['eci_coupon_status']=='Inactive') echo 'selected'; ?>>Inactive</option>
                    </select>
                  </div>
                  </div>
				  <div class="col-lg-8">
					  <div class="form-group">
						<p class="eci_error" id="eci_coupon_event_err"> </p>
						<label for="eci_service_name">Select Event For Coupon <span class="eci_req_star">*</span></label>
						<select class="form-control" id="eci_coupon_event" name="eci_coupon_event[]" multiple="multiple">
						<option value="">Select Event</option>
						<?php 
						$event_Arr = explode(',',$coupan_detail['eci_coupon_event']);
						if(isset($eventList) && !empty($eventList)){
								foreach($eventList as $elist){
									$selected='';
									if(in_array($elist['eci_event_list_sno'] ,$event_Arr)){
										 $selected='selected'; }
									 echo '<option value="'.$elist['eci_event_list_sno'].'" '.$selected.'>'.$elist['eci_event_list_name'].'</option>'; 
								}
							}?>
						</select>
						<p class="help-block">NOTE:- To select multiple events hold ( Ctrl ) button and then select event.</p>
					  </div>
                  </div>
                    <div class="col-lg-12 text-center">
                  <input type="submit" class="btn btn-default" onclick="return check_add_coupon_form()" name="sub_service_save" value="Save" />
				  
                  
                  </div>
                </form>
           </div>
        </div>
    </div>
   