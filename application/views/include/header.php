<!DOCTYPE html>
<!-- 
Template Name: Event Management System - Perfect Day
Version: 1.0
Author: Kamleshyadav
Website: http://himanshusofttech.com/
Purchase: http://codecanyon.net/user/kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title>Event Management-Dashboard</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description"  content="Event Management"/>
<meta name="keywords" content="Event Management">
<meta name="author"  content="Kamleshyadav"/>
<meta name="MobileOptimized" content="320">
<link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.ico" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?php echo base_url();?>assets/back/stylesheet/jquery.toastmessage.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/back/stylesheet/dropzone.css" rel="stylesheet" type="text/css" />
<!--start theme style -->
<link href="<?php echo base_url();?>assets/back/stylesheet/main.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/back/javascript/plugin/css/datatables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/back/stylesheet/timepicki.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/common.css" rel="stylesheet" type="text/css" />
<!-- end theme style -->


<!--Main js start-->
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/jquery.toastmessage.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/dropzone.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/plugin/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/timepicki.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/my_custom.js?<?php echo date('h:i:s') ?>"></script>

</head>
<?php
 $controller= $this->router->fetch_class(); 
 $method    = $this->router->fetch_method();

?>
<body>


<div class="container">
  <header>
  
    <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand"> <img src="http://placehold.it/50x50" class="img-circle"><span><?php echo $this->session->userdata['eci_super_name'];?></span> </a> </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url();?>" target="_blank"><i class="fa fa-eye"></i><span class="badge">View Live Site</span></a></li>
          
          </ul>
          <div class="eci_logo"> <a href=""><img class="img-responsive" src="<?php echo base_url();?>assets/back/images/logo.png" alt="logo" /></a> </div>

       <div class="eci_logout">
          <a href="<?php echo base_url();?>eventadmin/logout" class="btn btn-default btn-sm pull-right"><i class="fa fa-align-left fa-sign-out"></i> Logout</a>
          </div>
        
      
        </div>
        <!-- /.navbar-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </header>
  
  <div class="eci_main_container">
     <div class="eci_page_sidebar_wrapper">
     <div class="eci_page_sidebar navbar-collapse collapse">
      <ul>
      <?php if($this->session->userdata('user_type')==1){ ?>
      <li <?php if($method == 'dashboard'){ echo 'class="active"';} ?>><a href="<?php echo base_url();?>eventadmin/dashboard"><i class="fa fa-tachometer"></i>Dashboard</a></li>
      <?php } ?>
         <li <?php if($method == 'addevent' || $method == 'manageevent' || $method == 'updateexistingevent' || $method == 'commision_event' || $method == 'updatecommmisonevent' || $method == 'total_collection'){ echo 'class="active"';} ?>><a><i class="fa fa-calendar"></i> event</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/addevent"><i>-</i> Add Event</a></li>
             <li><a href="<?php echo base_url();?>eventadmin/manageevent"><i>-</i> Manage Event</a></li>
             <li><a href="<?php echo base_url();?>event/commision_event"><i>-</i> Commision Event</a></li>
             <li><a href="<?php echo base_url();?>event/total_collection"><i>-</i> Total Collection</a></li>
           </ul>
        </li>
        <?php 
            if($_SESSION['user_type']==1){
                ?>
                <li <?php if($method == 'uploadimage' || $method == 'setimage' || $method == 'delete_image'){ echo 'class="active"';} ?>><a><i class="fa fa-image"></i>Images</a>
                  <ul class="eci_submenu">
                     <li><a href="<?php echo base_url();?>eventadmin/uploadimage"><i>-</i> Upload Image</a></li>
                     <li class="hide"><a href="<?php echo base_url();?>eventadmin/setimage"><i>-</i> Choose Image</a></li>
                      <li><a href="<?php echo base_url();?>event/delete_image"><i>-</i> Delete Image</a></li>
              
                   </ul>
                 </li>
                 <?php
            }
        ?>
         <li <?php if($method == 'recentevents'){ echo 'class="active"';} ?>><a href="<?php echo base_url();?>eventadmin/recentevents"><i class="fa fa-anchor"></i>Past Events</a></li>
         <li <?php if($method == 'bookingdetail' || $method == 'add_booking_manually'){ echo 'class="active"';} ?>><a><i class="fa fa-bookmark"></i>Booking</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/bookingdetail"><i>-</i> Booking Detail</a></li>
             <?php if($this->session->userdata('user_type')==1){ ?>
             <li><a href="<?php echo base_url();?>eventadmin/add_booking_manually"><i>-</i> Manual Booking</a></li>
             <?php } ?>
           </ul>
         </li>
         <li <?php if($controller =='profile' && ($method=='index'|| $method=='editprofile' || $method=='changepassword')){ echo 'class="active"';} ?>><a><i class="fa fa-building-o"></i>Profile</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>profile"><i>-</i>Myprofile</a></li>
             <li><a href="<?php echo base_url();?>profile/editprofile"><i>-</i>Edit Profile</a></li>
             <li><a href="<?php echo base_url();?>profile/changepassword"><i>-</i>Change Password</a></li>
           </ul>
         </li>
         <?php if($this->session->userdata('user_type')==2){ ?>
         <li <?php if($method == 'plan'){ echo 'class="active"';} ?>><a href="<?php echo base_url();?>profile/plan"><i class="fa fa-anchor"></i>Purchase Plan</a></li>
         <?php } ?>
     <?php if($this->session->userdata('user_type')==1){ ?>
     <li <?php if($method == 'users' || $method =='single_user' || $method=='add_user'){ echo 'class="active"';} ?>><a><i class="fa fa-users"></i>Users</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>user/users"><i>-</i> Userlist</a></li>
             <li><a href="<?php echo base_url();?>user/add_user"><i>-</i>Add User</a></li>
            <!--  <li><a href="<?php echo base_url();?>event/adduserplan"><i>-</i>Assign Plan TO User </a></li>-->
           </ul>
         </li>
        <li <?php if($method == 'add_coupon' || $method == 'manage_coupon' || $method =='edit_coupon'){ echo 'class="active"';} ?>><a><i class="fa fa-gift"></i>Coupon</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>event/add_coupon"><i>-</i>Add Coupon </a></li>
             <li><a href="<?php echo base_url();?>event/manage_coupon"><i>-</i> Manage Coupon </a></li>
           </ul>
         </li> 
     <!--
     <li <?php if($method == 'addservices' || $method == 'manageservices' || $method =='updateexistingservices'){ echo 'class="active"';} ?>><a><i class="fa fa-list-alt"></i>Services</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/addservices"><i>-</i> Add Services </a></li>
             <li><a href="<?php echo base_url();?>eventadmin/manageservices"><i>-</i> Manage Services</a></li>
           </ul>
         </li>-->
    
     
     
       <li <?php if($method == 'managemembershipplan' || $method == 'manageuserplan' || $method == 'userplanrequest' || $method == 'updatemembershipplan' || $method == 'updateuserplan' || $method == 'add_plan'){ echo 'class="active"';} ?>><a><i class="fa fa-building-o"></i>Plan</a>
          <ul class="eci_submenu">
          <li><a href="<?php echo base_url();?>event/add_plan"><i>-</i>Add New Plan</a></li>
             <li><a href="<?php echo base_url();?>event/managemembershipplan"><i>-</i>Membership Plan</a></li>
         
             <li><a href="<?php echo base_url();?>event/manageuserplan"><i>-</i>User Plan</a></li>
             <li><a href="<?php echo base_url();?>event/userplanrequest"><i>-</i>Plan Request</a></li>

           </ul>
         </li>
     
      <li <?php if($method == 'paymentsetting' || $method == 'paymentmode'){ echo 'class="active"';} ?>><a><i class="fa fa-money"></i>Payment</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/paymentsetting"><i>-</i>Payment Setting</a></li>
             <li><a href="<?php echo base_url();?>event/paymentmode"><i>-</i> Payment Mode</a></li>
      
           </ul>
         </li>
     
     <li <?php if($method == 'contactus' || $method == 'contactquery' || $method == 'subscriber'){ echo 'class="active"';} ?>><a><i class="fa fa-map-marker"></i>Contact us</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/contactus"><i>-</i> Contact Setting</a></li>
             <li><a href="<?php echo base_url();?>eventadmin/contactquery"><i>-</i> Contact List</a></li>
             <li><a href="<?php echo base_url();?>eventadmin/subscriber"><i>-</i> Subscriber List</a></li>
           </ul>
         </li>
    
     
     <li <?php if($method == 'socialsetting' || $method == 'seosetting' || $method == 'backgroundimage'){ echo 'class="active"';} ?>><a><i class="fa fa-cog"></i>Website Setting</a>
          <ul class="eci_submenu">
             <li><a href="<?php echo base_url();?>eventadmin/socialsetting"><i>-</i> General Setting</a></li>
             <li><a href="<?php echo base_url();?>eventadmin/seosetting"><i>-</i> SEO Setting</a></li>
       <li><a href="<?php echo base_url();?>eventadmin/backgroundimage"><i>-</i> Background/Slider</a></li>
       <li><a href="<?php echo base_url();?>eventadmin/mailtemplate"><i>-</i>Mail Template</a></li>
           </ul>
         </li>
     <?php } ?>
     
       </ul>
    </div>
     </div> 
   <input type="hidden" value="<?php echo base_url();?>" id="basepath">
