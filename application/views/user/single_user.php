<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4> User Detail</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	
  
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#basic">Basic Information</a></li>
    <li><a data-toggle="tab" href="#plandetail">Plan Detail</a></li>
    
  </ul>

  <div class="tab-content">
    <div id="basic" class="tab-pane fade in active">
    <?php if(!empty($user_detail)): ?>
      <table class="table table-bordered" >
    
    <tbody>
      <tr>
        <th>Name</th>
        <td><b><?php echo ucfirst($user_detail['eci_admin_name']); ?></b></td>
        
      </tr>
      <tr>
        <th>Email</th>
        <td><?php echo $user_detail['eci_admin_email']; ?></td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
        <?php if($user_detail['status']==1){ ?>
        <span class="label label-success">Active</span>
        <?php } else { ?>
         <span class="label label-danger">Inactive</span>

        <?php } ?>
        </td>
      </tr>
      <tr>
        <th>Mobile Number</th>
        <td>
            <?php
            if($user_detail['eci_admin_contact'] != null){
                echo $user_detail['eci_admin_contact'];
            }
            else{
                echo "Not available";
            }
             ?>
        </td>
      </tr>
      <tr>
        <th>Address</th>

        <td>
            <?php
            if($user_detail['eci_admin_address'] != null){
                echo $user_detail['eci_admin_address'];
            }
            else{
                echo "Not available";
            }
             ?>
        </td>
      </tr>
      <tr>
        <th>Gender</th>
        <td><?php
            if($user_detail['eci_admin_gender'] != null){
                echo $user_detail['eci_admin_gender'];
            }
            else{
                echo "Not available";
            }
             ?>
        </td>
      </tr>
      
    </tbody>
  </table>
<?php endif; ?>
    </div>
    <div id="plandetail" class="tab-pane fade">
     <?php if(!empty($user_detail)):
         ?>

      <table class="table table-bordered" >
    <?php
                 $planid=$user_detail['eci_membership_plan_id'];
                 if(!empty($planid)):
                  $planname=select_single_data('eci_membership_plan',"where eci_plan_id=$planid",'eci_plan_name');
                    $plandate="";
                     if($user_detail['startdate']!=""){
                      $startdate=date("d-F-y",$user_detail['startdate']);
                     }else{
                      $startdate='00/00/00';
                     }
                  endif;
            
    ?>
    <tbody>

      <tr>
        <th>Plan name</th>
        <th>Startdate</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
     <?php if(!empty($planid)): ?>
      <tr>
      <td><?php echo $planname ;?></td>
      <td><?php echo $startdate; ?></td>
      <td><?php echo $user_detail['eci_user_plan_status']; ?></td>
      <td><span><a href="<?php echo base_url();?>event/updateuserplan/<?php echo $user_detail['eci_user_plan_id'];?>" title="EDIT"><img src="<?php echo base_url();?>assets/back/images/icons/edit.png" alt="EDIT" /> </a></span></td>
      </tr>
    <?php endif; ?>
        </tbody>
        </table>
<?php endif; ?>
    </div>
    
    
  
    </div> 
           
         
          

        </div>
				
				
				
				
	   </div>
        </div>
    </div>
