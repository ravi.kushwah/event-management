<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4> Add User</h4>
                    <hr>
                    </div>
                </div>
				<?php if(!empty($msg)): ?>
			<div class="col-lg-12" id="msg_div">	
		<div class="alert <?php echo $alertclass; ?> alert-dismissible">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <?php echo $msg ;?>
          </div></div>
	  <?php endif; ?>
	 <form  class="ad_user_form" role="form" action="<?php echo base_url() ?>user/add_user" method="post">
	  
       
	<div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_user_name_err"> </p>
		<label for="eci_event_name">Name of User <span class="eci_req_star">*</span></label>
		<input type="text"  class="form-control" placeholder="Name of customer" name="eci_user_name" id="eci_user_name" value=""/>
	  </div>
	  </div>
	 <div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_user_email_err"> </p>
		<label for="eci_event_name">Email of User <span class="eci_req_star">*</span></label>
		<input type="text"  class="form-control" placeholder="Email of customer" name="eci_user_email" id="eci_user_email" value=""/>
	  </div>
	  </div>
      
	<div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_user_password_err"> </p>
		<label for="eci_event_name">Password <span class="eci_req_star">*</span></label>
		<input type="text"  class="form-control" placeholder="Password of customer" name="eci_user_password" id="eci_user_password" value=""/>
		<button type="button" class="pwd_btn btn btn-sm btn-default" onclick="generate_pass();">Generate Random Password</button>
	  </div>

	</div>
	<div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_plan_name_err"> </p>
                    <label for="eci_service_name">Plan Name <span class="eci_req_star">*</span></label>
                    <select class="form-control" name="eci_user_plan_name" id="eci_user_plan_name">
                    
                      <option value="">Select plan</option>
                      <?php foreach ($plan as $plan):?>
                     <option value="<?php echo $plan['eci_plan_id']?>" ><?php echo $plan['eci_plan_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>

                  <div class="form-group">
		
		<label for="eci_event_name">Send customer a confirmation email</label>
		<div class="checkbox">
		<input id="send_cust_email" name="send_cust_email" type="checkbox" value="1">
		<label for="send_cust_email"></label>
	  </div>
	  </div>
    </div>

    
  	
  	
                  
  	</div>
	
       	 				  
   <div class="col-lg-12 text-center">
		 <input type="submit" class="btn btn-default" id="check_add_booking_manual_form" onclick="return check_add_user()" name="sub_evnt_save" value="Save"/>
		
                  <button type="reset" class="btn btn-group">Reset</button>
                  </div>
        </form>
	

	   </div>
        </div>
    </div>

        <script type="text/javascript" src="<?php echo base_url();?>assets/js/admin.js"></script>
