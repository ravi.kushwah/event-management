<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Change Password</h4>
                    <hr>
                  </div>
                </div>
           	</div>
        
              <div class="row">
             
             <form id="editprofile" role="form" action="<?php echo base_url() ?>profile/changepassword" method="post">
                <div class="col-md-9 col-lg-9 "> 
                  <div class="form-group">
                    
                    <label for="eci_old_password">Old Password <span class="eci_req_star">*</span></label>
                    <input type="Password" class="form-control" id="eci_old_password" name="eci_old_password" placeholder="Enter old password" value="">
                    <p class="font-color-red" id="eci_old_pass_err"> </p>
                  </div>
                  
                  <div class="form-group">
                    
                    <label for="eci_new_password">New Password <span class="eci_req_star">*</span></label>
                    <input type="password" class="form-control" id="eci_new_password" name="eci_new_password" placeholder="Enter new password" value="">
                    <p class="font-color-red" id="eci_new_pass_err"> </p>
                  </div>
                  
                  <div class="form-group">
                    
                    <label for="eci_cnew_password">Confirm Password <span class="eci_req_star">*</span></label>
                    <input type="password" class="form-control" id="eci_cnew_password" name="eci_cnew_password" placeholder="Confirm password" value="">
                    <p class="font-color-red" id="eci_c_pass_err"> </p>
                  </div>
                  <div class="text-center">
                  <input type="submit" class="btn btn-default " name="profileupdate" value="Update" onclick="return check_confirm_password();">
                  </div> 
                  <?php if(isset($msg)){ ?>
                    <div class="<?php echo $class ?>">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <strong> <?php echo $msg ?>.</strong>
                    </div>
                   <?php } ?>
                   <?php echo validation_errors(); ?>
                </div>
                </form>
              </div>
        
            
        </div>
    </div>

