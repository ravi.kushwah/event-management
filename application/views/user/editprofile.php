<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Manage Profile</h4>
                    <hr>
                  </div>
                </div>
           	</div>
        <?php
        if($user_detail['eci_admin_image']==""){
         $src=base_url().'assets/front/images/profile/defaultprofile.jpg';
         }else{
          $src=base_url().'assets/front/images/profile/'.$user_detail['eci_admin_image'];
          }?>
              <div class="row">
              <form id="changepicture" method='post' enctype='multipart/form-data'>
                <div class="col-md-3 col-lg-3 " align="center"> 
				 <div class="upload_thumb_prt">
                   <img alt="User Pic"  id="userimage" src="<?php echo $src; ?>" class="img-responsive div-profile">
					  <span class="upload_btn">
						upload pic
                       <input type="file" name="userfile" accept="image/*" required="true" id="userfile">
					 </span>
                    <?php $loader=base_url().'assets/back/images/loader/ajax_loader_blue_350.gif';?>
                     <!-- <input type="submit" class="btn btn-primary btn-block" name="submit" value="Update Picture">-->
                     <button type="submit" class="btn btn-default" name="submit" id="imgupdate"><i class="fa fa-spinner fa-spin fa-1x fa-fw hide" id="loader"></i> Update Picture </button> 
                  </div>
				</div>
                </form>
                
                <form id="editprofile" role="form" action="<?php echo base_url() ?>profile/editprofile" method="post">
                <div class="col-md-9 col-lg-9 "> 
                  <div class="form-group">
                    
                    <label for="eci_event_name">Name <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_admin_name" name="eci_admin_name" placeholder="Enter user name" value="<?php echo $user_detail['eci_admin_name'];?>">
                    <p  id="eci_event_name_err" class="font-color-red"></p>
                  </div>
                  <div class="form-group">
                    
                    <label for="eci_event_contact">Contact <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_admin_contact" name="eci_admin_contact" placeholder="Enter contact number" value="<?php echo $user_detail['eci_admin_contact'];?>">
                    <p id="eci_event_contact_err" class="font-color-red"></p>
                  </div>
                  <div class="form-group">
                  <?php  $gender=$user_detail['eci_admin_gender'];?>
                    
                    <label for="eci_admin_gender">Gender<span class="eci_req_star">*</span></label>
                      <label class="radio-inline"><input class="display-inline" type="radio" id="eci_admin_gender" name="eci_admin_gender" <?php if($gender=='Male') echo "checked"; ?> value="Male">Male</label>
                      <label class="radio-inline"><input type="radio" id="eci_admin_gender" name="eci_admin_gender" value="Female" class="display-inline" <?php if($gender=='Female') echo "checked"; ?>>Female</label>
                        <p id="eci_event_gender_err" class="font-color-red"></p>
                  </div>
                  <div class="form-group">
                    
                    <label for="eci_admin_address">Address <span class="eci_req_star">*</span></label>
                   
                  <textarea name="eci_admin_address" id="eci_admin_address" class="form-control whitespace-inline" wrap="hard"><?php echo str_ireplace("<br>", "\r\n", $user_detail['eci_admin_address']);?></textarea>
                  <p id="eci_event_add_err" class="font-color-red"></p>
                  </div>
                  <div class="text-center">
                  <input type="submit" class="btn btn-default" name="profileupdate" value="Update" onclick="return check_edit_profile()"> 
                </div>
                </div>
                </form>
              </div>
        
            
        </div>
    </div>
    
