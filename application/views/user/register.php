<?php $this->load->view('home/include/header'); ?>

					<form action="<?php echo base_url();?>user/register" method="post">
					
					  <div class="form-group">
						<label>Name<span class="eci_req_star">*</span></label>
						<input type="text" name="uname" class="form-control" id="eci_uname"  placeholder="Enter Username">
						<p class="login_error" id="eci_uname_err"></p>
					  </div>

					  <div class="form-group">
						<label>Email<span class="eci_req_star">*</span></label>
						<input type="email" name="uemail" class="form-control" id="eci_uemail"  placeholder="Enter User Email">
						<p class="login_error" id="eci_uemail_err"></p>
					  </div>
					  
					  <div class="form-group">
						<label>Password<span class="eci_req_star">*</span></label>
						<input type="password" name="upassword"  class="form-control" id="eci_upassword"  placeholder="Password">
						<p class="login_error" id="eci_password_err"></p>
					  </div>
					  <div class="form-group">
						<label>Confirm Password<span class="eci_req_star">*</span></label>
						<input type="password" name="ucpassword"  class="form-control" id="eci_ucpassword"  placeholder="Password">
						<p class="login_error" id="eci_cpassword_err"></p>
					  </div>
					  
					  
					  <input type="hidden" id="rem_inp" name="rem_inp" value=""/>
              <input class="btn btn-default" type="submit" value="Register" name='getin_btn' onclick="return check_user_register();"/>
					<p class="login_error">  <?php if(isset($msg)) { echo $msg; }?></p>
					</form>
				</div>
				<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
				<a href="<?php echo base_url();?>user">Already Register Click Here To Login</a>
				</div>			
			</div>
		</div>
  </div>
  
</div>


    <script type="text/javascript" src="<?php echo base_url();?>assets/js/admin.js"></script>
