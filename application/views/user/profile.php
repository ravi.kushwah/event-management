<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Profile</h4>
                    <hr>
                  </div>
                </div>
           	</div>
           

      
      
        
   
   
          
            <?php 

              if(empty($user_detail['eci_admin_image']))
               $src=base_url().'assets/front/images/profile/defaultprofile.jpg'; 
              else
             $src=base_url().'assets/front/images/profile/'.$user_detail['eci_admin_image'];
           

            ?>
           
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="<?php echo $src; ?>" class="img-responsive div-profile"> </div>
                
               
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      
                      <tr>
                        <td>User Name:</td>
                        <td><?php echo $user_detail['eci_admin_name'];?></td>
                      </tr>
                      
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td><?php echo $user_detail['eci_admin_gender'];?></td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td><?php echo $user_detail['eci_admin_address'];?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $user_detail['eci_admin_email'];?></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><?php echo $user_detail['eci_admin_contact'];?></td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  <hr>
               <?php if($this->session->userdata('user_type')==2){ ?>
                  <h4>Addtional Info</h4>
                  
               <table class="table table-user-information">
                    <tbody>
                      
                      <tr>
                      <?php
                      $userid=$this->session->userdata('eci_super_id');
                       $userplanid=select_single_data('eci_user_membership_plan',"where eci_user_id=$userid",'eci_membership_plan_id');
                        $planname="you are not subscribe to any plan";
                        if($userplanid!=""){
                          $planname=select_single_data('eci_membership_plan',"where eci_plan_id=$userplanid",'eci_plan_name');
                          
                          $planstatus=select_single_data('eci_user_membership_plan',"where eci_user_id=$userid",'eci_user_plan_status');
                          
                        }
                        ?>
                        <td>Membership Plan:</td>
                        <td><?php echo $planname; ?></td>
                        
                      </tr>

                   <?php if($userplanid!=""){
                    $startdate=select_single_data('eci_user_membership_plan',"where eci_user_id=$userid",'startdate');
                    ?>
                      <tr>
                   <td>Plan Subscription Date:</td>
                   <?php if($startdate !=""){ ?>
                   <td><?php echo date("d-F-y",$startdate); ?></td>
                   <?php } ?>
                   </tr>
                   <tr>
                   <td>Plan Status:</td>
                   
                   <td><?php echo $planstatus ?></td>
                   
                   </tr>
				   <?php } ?>
                      
                        
                     
                    </tbody>
                  </table>
              <?php } ?>    
                </div>
              </div>
            
                
            
          
        
      
   
        </div>
    </div>
