<div class="page_wrapper" data-stellar-background-ratio=".5">
    <div class="banner_overlay">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="page_title">
                        <h3>Authenticate</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="breacrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="active">Authenticate</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php 
	if(!empty($contactus_setting))
		$cnt_data = json_decode($contactus_setting[0]['eci_contactus_data']);
	else
		$cnt_data = array();
		?>
 <div class="contactus_wraper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="rs_heading_wrapper">
                    <div class="rs_heading">
					<?php if(isset($forget)){?>
                     <h3>Forget Password</h3>
					<?php } ?>
					<?php if(isset($reset)){ ?>
                     <h3>Reset Password</h3>
					<?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="contact_section">
			<?php if(isset($forget)){?>
				<form id="login_form" action="<?php echo base_url();?>user/forget_password" method="post">
                <div class="col-lg-12 col-md-12">
                    <div class="contactus_form_wraper">
						<div class="form-group">
							<input type="text" name="uemail" class="form-control" id="eci_uemail" value="" placeholder="Enter Your  Email">
						</div> 
						<div class="btn_wrapper">
							<button type="submit" class="btn rs_btn" name='getin_btn' onclick="return check_forget_password();">Submit</button> 
						</div>
                    </div>
                </div>
				</form>
			<?php } ?>
			<?php if(isset($reset)){?>
			<form action="<?php echo base_url();?>user/resetpassword" method="post">
                <div class="col-lg-12 col-md-12">
                    <div class="contactus_form_wraper">
                       <div class="form-group ">
						<input type="text" name="eci_resetcode" class="form-control" id="eci_resetcode"  placeholder="Enter Reset Code" value="<?php if(isset($resetcode)) echo $resetcode; ?>">
					  </div>   
                       <div class="form-group">
						<input type="password" name="upassword"  class="form-control" id="eci_upassword"  placeholder="Password">
					  </div>
					  <div class="form-group">
						<input type="password" name="ucpassword"  class="form-control" id="eci_ucpassword"  placeholder="Password">
						<p class="login_error" id="eci_cpassword_err"></p>
					  </div>
                        <div class="btn_wrapper">
                            <button type="submit" class="btn rs_btn" name='getin_btn' onclick="return check_reset_password();">Submit</button> 							
                        </div>
                    </div>
                </div>
				</form>
			<?php } ?>
            </div>
        </div>
    </div>
</div>

