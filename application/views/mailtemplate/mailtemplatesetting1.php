<link href="<?php echo base_url();?>assets/css/common.css" rel="stylesheet" type="text/css"/>
<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Email Temlates </h4>
                    <hr>
                    </div>
                </div>

                <div class="col-lg-12">
                	<div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Registration Template</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse ">
        <div class="panel-body">
        <div class="alert alert-info th_setting_text">
			<p>
			<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
			Use below shortcodes.
			</p>
		</div>	
			<div class="th_shortcode_wrapper">
                            <p> [username] : To use register user's name </p>
                            <p> [linktext] : Activation link will appear here</p>
                            <p> [break] : To break the line</p>
			</div>
		<div class="alert alert-info ">
                            <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Use above shortcodes.</p>
         </div>
         <div class="emailtemplateform">
         <div class="form-group">
                            <div class="col-lg-3 col-md-3">
                            <label >Template text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <textarea rows="8" class="form-control" id="registrationemail_text"><?php echo select_single_data('eci_website_setting','where eci_website_setting_name="registrationemail_text"','eci_website_setting_value'); ?></textarea>
                            </div>
           </div>
           <div class="clearfix"></div>
           <div class="form-group margin-top-25">
                            <div class="col-lg-3 col-md-3">
                            <label>Activation link text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <input id="registrationemail_linktext" class="form-control" value="<?php echo select_single_data('eci_website_setting','where eci_website_setting_name="registrationemail_linktext"','eci_website_setting_value'); ?>" type="text">
                            </div>
           </div>
           <div class="row">
           <div class="col-lg-12 col-md-12 text-center margin-top-25">
           <button class="btn btn-default" id="emailtemplateupdate">Update</button>
           </div>
           <div class="col-lg-12 col-md-12 hide margin-top-25" id="eupdatesuccess"">
              <div class="alert alert-success alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <span id="msg"></span>
              </div>
           </div>

         </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Forget Password Template</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
        <div class="alert alert-info th_setting_text">
      <p>
      <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
      Use below shortcodes.
      </p>
    </div>  
      <div class="th_shortcode_wrapper">
                            <p> [username] : To use registered user name </p>
                            <p> [linktext] : Reset password link will appear here</p>
                            <p> [break] : To break the line</p>
      </div>
    <div class="alert alert-info ">
                            <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Use above shortcodes.</p>
         </div>
         <div class="emailtemplateform">
         <div class="form-group">
                            <div class="col-lg-3 col-md-3">
                            <label>Template text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <textarea rows="8" class="form-control" id="forgotpwdemail_text"><?php echo select_single_data('eci_website_setting','where eci_website_setting_name="forgotpwdemail_text"','eci_website_setting_value'); ?></textarea>
                            </div>
           </div>
           <div class="clearfix"></div>
           <div class="form-group margin-top-25">
                            <div class="col-lg-3 col-md-3">
                            <label>Forgot password link text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <input id="forgotpwdemail_linktext" class="form-control" value="<?php echo select_single_data('eci_website_setting','where eci_website_setting_name="forgotpwdemail_linktext"','eci_website_setting_value'); ?>" type="text">
                            </div>
           </div>
           <div class="row">
           <div class="col-lg-12 col-md-12 text-center margin-top-25">
           <button class="btn btn-default" id="fmailtemplateupdate">Update</button>
           </div>
           <div class="col-lg-12 col-md-12 hide margin-top-25" id="fupdatesuccess">
              <div class="alert alert-success alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <span id="msg"></span>
              </div>
           </div>

         </div>
        </div>
      </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Plan Activate/Update Template</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
         <div class="panel-body">
        <div class="alert alert-info th_setting_text">
      <p>
      <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
      Use below shortcodes.
      </p>
    </div>  
      <div class="th_shortcode_wrapper">
                            <p> [username] : To use registered user's name </p>
                            
                            <p> [break] : To break the line</p>
      </div>
    <div class="alert alert-info ">
                            <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Use above shortcodes.</p>
         </div>
         <div class="emailtemplateform">
         <div class="form-group">
                            <div class="col-lg-3 col-md-3">
                            <label>Template text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <textarea rows="8" class="form-control" id="planpwdemail_text"><?php echo select_single_data('eci_website_setting','where eci_website_setting_name="activeplanemail_text"','eci_website_setting_value'); ?></textarea>
                            </div>
           </div>
           <div class="clearfix"></div>
           
           <div class="row">
           <div class="col-lg-12 col-md-12 text-center margin-top-25">
           <button class="btn btn-default" id="pmailtemplateupdate">Update</button>
           </div>
           <div class="col-lg-12 col-md-12 hide margin-top-25" id="pupdatesuccess">
              <div class="alert alert-success alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <span id="msg"></span>
              </div>
           </div>

         </div>
        </div>
      </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Add New User Template</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
         <div class="panel-body">
        <div class="alert alert-info th_setting_text">
      <p>
      <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
      Use below shortcodes.
      </p>
    </div>  
      <div class="th_shortcode_wrapper">
                      <p> [username]     : User's name </p>
					  <p> [useremail]     : User's email </p>
                      <p> [password]     : Password </p>
                      <p> [website_link] : Website link </p>
                      <p> [break] : To break the line</p>
      </div>
    <div class="alert alert-info ">
                            <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Use above shortcodes.</p>
         </div>
         <div class="emailtemplateform">
         <div class="form-group">
                            <div class="col-lg-3 col-md-3">
                            <label>Template text</label>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <textarea rows="8" class="form-control" id="adduseremail_text"><?php echo select_single_data('eci_website_setting','where eci_website_setting_name="adduseremail_text"','eci_website_setting_value'); ?></textarea>
                            </div>
           </div>
           <div class="clearfix"></div>
           
           <div class="row">
           <div class="col-lg-12 col-md-12 text-center margin-top-25">
           <button class="btn btn-default" id="ausermailtemplateupdate">Update</button>
           </div>
           <div class="col-lg-12 col-md-12 hide margin-top-25" id="adduserupdatesuccess">
              <div class="alert alert-success alert-dismissible">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <span id="msg"></span>
              </div>
           </div>

         </div>
        </div>
      </div>
      </div>
    </div>
  </div> 
                </div>    
            </div>
        </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/admin.js"></script>
