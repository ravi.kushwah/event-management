  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
<?php $pay_currency_code = $payment_detail[0]['eci_payment_detail_ccode'];?>
<div class="page_wrapper" data-stellar-background-ratio=".5">
   <div class="banner_overlay">
      <div class="container">
         <div class="col-lg-12">
            <div class="row">
               <div class="page_title">
                  <h3> Events</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="breacrumb_wrapper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <ol class="breadcrumb">
               <li><a href="<?php echo base_url(); ?>">Home</a></li>
               <li class="active">Events</li>
            </ol>
         </div>
      </div>
   </div>
</div>
<div class="property_view_wrapper">
<div class="container">
   <div class="row">
      <div class="col-lg-12 col-md-12">
         <div class="row">
            <div class="col-lg-12">
               <div class="rs_heading_wrapper">
                  <div class="rs_heading">
                     <div class="col-lg-6 col-md-6 col-sm-10 padding">
                        <h3>  Events</h3>
                     </div>
                     <div class="col-lg-6 col-md-4 col-sm-6 news_search_wrapper">
                        <form method="post" >                        
                              <input type="text" class="form-control search_input" placeholder="Enter your event name" id="searchValue">
                              <input type="submit" class="btn btn-primary btn search_btn searchEvent" baseurl="<?php echo base_url();?>" id="searchValue" value="Search">                        
                        </form>
                     </div>                    
                     <div class="col-lg-2 col-md-2s col-sm-2 padding">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php if(!empty($upcoming_event)) { ?>
         <div class="list_view animated fadeIn searchEventData">
            <?php 
				foreach($upcoming_event as $solo_upcoming_event) {
               		$event_uniqueid=$solo_upcoming_event['eci_event_list_uniqid'];?>
					<div class="property_wrapper">
					<div class="row">
						<div class="property_img_wrapper">
							<?php 
								$upcoming_img_src=base_url().'assets/back/events/default_event_image.jpg';
								if(!empty($solo_upcoming_event['eci_event_list_image'])){
								$upcoming_img_src=base_url().'assets/back/events/'.$solo_upcoming_event['eci_event_list_image'];
								} ?>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<img  src="<?php echo  $upcoming_img_src; ?>" alt="">
								<?php if($solo_upcoming_event['eci_event_list_timestamp']>time()){ ?>
								<!--<div class="tag_sale"><a href="#" class="btn btn-primary" onclick="return book_now(<?php echo $solo_upcoming_event['eci_event_list_sno'];?>)">book now</a></div>-->
								<div class="tag_sale">
								     <button type="button" class="btn btn-primary book_now" data-toggle="modal" data-target="#myModal" bid="<?php echo $solo_upcoming_event['eci_event_list_sno'];?>">book now</button>
								    <!--<input type="button" class="btn btn-primary book_now " value="book now" bid="<?php echo $solo_upcoming_event['eci_event_list_sno'];?>">-->
								</div>
								<?php } ?>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="property_detail">
								<div class="property_content">
									<h5><a href="<?php echo base_url().'details/'.$event_uniqueid ?>"><?php echo $solo_upcoming_event['eci_event_list_name'];?></a></h5>
									<span><?php if(is_numeric($solo_upcoming_event['eci_event_list_amount'])) {  echo $pay_currency_code.' '.$solo_upcoming_event['eci_event_list_amount']; } else { echo 'FREE';}?>  /Person</span>
									<?php echo word_limiter($solo_upcoming_event['eci_event_list_desc'], 30);?>
									<div>
										<p class="pull-left"><a href="<?php echo base_url().'details/'.$event_uniqueid ?>">Read More......</a></p>
										<p class="pull-right">Date : <?php   echo date("d M Y", strtotime($solo_upcoming_event['eci_event_list_evnt_date'])); ?>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         Time : <?php echo str_replace(' ', '',$solo_upcoming_event['eci_event_list_evnt_time']) ?></p>
										<p class="pull-right"></p>
									</div>
									<?php 
										$date_Arr=array();
										$date=$solo_upcoming_event['eci_event_list_evnt_date'];
										$date_Arr=explode('/',$date);
										$datasolotime=$string =str_replace(' ', '',$solo_upcoming_event['eci_event_list_evnt_time']);
										$time_data=explode(':',$datasolotime);
															$datasolotime  = date("H:i", strtotime($time_data[0].':'.$time_data[1] .$time_data[2]));
										?>
									<div class="em_event_date_counter">
										<div data-countdown="<?php echo $date_Arr[2];?>/<?php echo $date_Arr[0];?>/<?php echo $date_Arr[1];?> <?php echo $datasolotime; ?>"></div>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
					</div>              
			<?php } ?>	 
         <div class="col-lg-12 paginationdata">
            <div class="paginationData">
               <nav class="paginationLink">
                  <?php echo $links;?>  
               </nav>
            </div>  
         </div>          
      <?php  } ?>
         </div>
      </div>
      <!--row--> 
   </div>
</div>

 <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
             <div class="col-md-7">
                <h3 class="modal-title" id="myModalLabel"></h3>
             </div>
             <div class="col-md-4">
                <h4 id="em_seats_left" class="pull-right"></h4>
             </div>
          </div>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
           <div class="hide" id="couto">
                     <div class="col-md-8" id="coupon">
                        <div class="row">
                           Apply Coupon:
                           <input type="text" id="couponcode"><button id="coupon_submit" class="btn btn-primary check_coupon" >Apply</button><br>
                           <span class="couponResult"></span>
                        </div>
                     </div>
                     <div class="col-md-2" id="fullpayment">
                        <div class="em_cost">Cost:<br><span id="total"></span></div>
                     </div>
                     <div class="col-md-2" id="fullpayment">
                        <div class="em_cost">Seat:<span id="totalseat"></span></div>
                     </div>
                  </div>
                  <div class="form-group withoutcat">
                     <div class="em_person_seats_wrapper">
                        <div class="em_person_seats"> 
                           <span class="em_person_sec em_decrease_per">-</span>
                           <span class="em_person_sec em_increase_per">+</span>
                        </div>
                        <input type="text" class="form-control em_person_sec" id="em_person" value="1"/>
                     </div>
                  </div>
                  <input type="hidden" value="" id="em_ini">
                  <input type="hidden" value="" id="event_id">
                  <input type="hidden" value="" id="max_users">
                  <input type="hidden" value="" id="total_count">
                  <input type="hidden" value="" id="event_id_cat">
                  <input type="hidden" value="" id="max_users_cat">
                  <input type="hidden" value="" id="total_count_cat">
                  <input type="hidden" value="" id="cat_count">
                  <input type="hidden" value="" id="noofperson">
                  <input type="hidden" value="" id="cat_left">
                  <div id="cattable" class="hide"></div>
                  <div id="hiden" class="hide"></div>
                  <!--<a class="btn btn-primary " id="eci_proceed_to_pay" onclick="proceed_to_pay()">Proceed To Pay<img src="<?php echo base_url()?>assets/front/images/wait.gif" id="eci_proceed_to_pay_wait"/></a>	 -->
                  <input type="button" class="btn btn-primary proceed_to_pay" value="Proceed To Pay" pay_id="">
                  <?php $pay_currency_code = $payment_detail[0]['eci_payment_detail_ccode'];	?>
                  <div class="paypal_container hide">
                     <form id="onlinebookingform" name='_xclick' action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'>
                        <input type='hidden' name='cmd' value='_xclick'>
                        <input type='hidden' name='business' value='<?php echo $payment_detail[0]['eci_payment_detail_email']?>'>
                        <input type='hidden' name='currency_code' value='<?php echo $payment_detail[0]['eci_payment_detail_ccode']?>'> 
                        <input type='hidden' name='item_name' value='Event' id="event_uniqueid"> 
                        <input type='hidden' name='return' value='<?php echo base_url()?>home/thanks'> 
                        <input type='hidden' name='amount' value='' id="event_amnt">
                        <input name="custom" id="paypal_custom" value="" type="hidden"> 
                        <input type="hidden" name="notify_url" value="<?php echo base_url()?>eventadmin/ipn">
                        <input type="submit" class="btn btn-primary btn-lg" value="Pay with Paypal" id="eci_pay_paypal"><img src="<?php echo base_url();?>assets/front/images/paypal.png" alt="Paypal"  title="Paypal" class="eci_pay_paypal_logo" />
                     </form>
                  </div>
                  <?php 
                     $pay_currency_code = isset($payment_detail[1]['eci_payment_detail_ccode']) ? $payment_detail[1]['eci_payment_detail_ccode'] : '';
                     $invoicedetail = json_decode(isset($payment_detail[1]['eci_payment_detail_email'])?$payment_detail[1]['eci_payment_detail_email']:'');
                     ?>		
                  <div class="form-group" id="bookingform">
                     <form id="offlinebookingform" action="<?php echo base_url();?>home/save_info_invoice" method="post" class="eci_pay_paypal_logo">
                        <input type="hidden" value="" id="em_invoice_amnt_hidden"/>
                        <input type='hidden' value='' id="event_amnt" name="event_amnt"> 
                        <input type='hidden' value='' id="em_invoice_eventid" name="em_invoice_eventid"> 
                        <input type='hidden' value='' id="em_invoice_ticketcount" name="em_invoice_ticketcount"> 
                        <input type='hidden'  id="em_invoice_coupon_code" name="em_invoice_coupon_code" value=""> 
                        <input type='hidden'  id="em_invoice_cat_seat_left" name="em_invoice_cat_seat_left" value=""> 
                        <?php if($invoicedetail->name == 1 ) { ?>
                        <input type="text" class="form-control" placeholder="Full Name"  id="em_invoice_name" name="em_invoice_name"/>
                        <?php } if($invoicedetail->email == 1 ) { ?>
                        <input type="text" class="form-control" placeholder="Email"  id="em_invoice_email" name="em_invoice_email"/>
                        <?php } if($invoicedetail->cntctno == 1 ) { ?>
                        <input type="text" class="form-control" placeholder="Contact Number" id="em_invoice_cntct" name="em_invoice_cntct"/>
                        <?php } if($invoicedetail->add == 1 ) { ?>
                        <input type="text" class="form-control" placeholder="Address" id="em_invoice_add" name="em_invoice_add"/>
                        <?php } ?>
                        <input type="text" class="form-control" placeholder="Email"  id="em_invoice_email_free" name="em_invoice_email_free"/>
                        <div class="person-info"></div>
                        <p id="em_person_err"></p>
                        <input type="hidden" name="em_invoice_sendemail" value="<?php echo $invoicedetail->link; ?>"/>
                        <input type="hidden" class="btn btn-primary btn-lg" value="Offline Payment" id="eci_pay_paypal" name="eci_pay_paypal" onclick="return pay_via_invoice_check()">
                        <?php
                           $loader=base_url().'assets/back/images/loader/ajax-loader.gif';
                           $eventpaymentmod=select_single_data("eci_website_setting","where eci_website_setting_name='event_payment_mode'","eci_website_setting_value");
                            if($eventpaymentmod){
                           $eventpaymentmod=explode(',',$eventpaymentmod);  
                            }else{
                            $eventpaymentmod=array(); 
                            }
                           ?>
                        <?php if(in_array('offline',$eventpaymentmod)){ ?>
                        <a href="javascript:" class="book btn btn-primary  offline" data-type="offline"><span class="spinner hide loader" id="loader"><img src="<?php echo $loader; ?>" ></span>Offline payment</a>
                        <?php } ?>
                        <?php if(in_array('paypal',$eventpaymentmod)){ ?>
                        <a href="javascript:" class="book btn btn-primary online"  data-type="online"><span class="spinner hide loader" id="loader"><img src="<?php echo $loader; ?>" ></span>Pay with Paypal</a>
                        <?php } ?>
                        <?php if(in_array('stripe',$eventpaymentmod)){ ?>
                        <a href="javascript:" class="book btn btn-primary stripe"  data-type="stripe"><span class="spinner hide loader " id="loader"><img src="<?php echo $loader; ?>" ></span>Pay with Stripe</a>
                        <?php } ?>                        
                        <input type="submit" class="btn btn-primary free free" value="Book Now" data-type="free"><span class="spinner hide loader " id="loader"><img src="<?php echo $loader; ?>" ></span>
                     </form>
                  </div>
                  <div class="stripe_container text-center"></div>
        </div>
      </div>
    </div>
  </div>
