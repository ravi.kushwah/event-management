<div class="page_wrapper" data-stellar-background-ratio=".5">
    <div class="banner_overlay">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="page_title">
                        <h3>Thank You</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="breacrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="active">Thanks</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="error_page_wraper">
    <div class="container">
        <div class="row">
            <div class="error_wrapper">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
               
                    <h3><?php  if(!empty($payment_detail)) {  echo $payment_detail[0]['eci_payment_detail_msg']; } ?></h3>
					
					
                
            </div>
        </div>
    </div>
</div>