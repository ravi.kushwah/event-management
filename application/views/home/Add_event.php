<!---- Editor dependent JS on TOP -------->
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/jquery-3.6.0.js"></script>

<?php $h_arr = explode('http://', base_url());
	if( count($h_arr) == 2 ) {
?>
<script src="http://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<?php } else { ?>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<?php } ?>
<!---- Editor dependent JS on TOP -------->
<div class="eci_page_content_wrapper">
        <div class="eci_page_content"> 
           <div class="row">
           	<div class="col-lg-12">
				<?php if(!empty($msg)): ?>
			    <div class="alert alert-<?php echo $class; ?>  alert-dismissible fade in" role="alert">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
				    </button>
				   <?php echo $msg; ?>
			    </div>
				<?php endif; ?>
				<div class="eci_heading">
					  <?php $checksubscription=checksubcription();
					  if(!$checksubscription):?>
					  <div class="alert alert-danger alert-dismissible fade in" role="alert">You cannot add event because
					  you are not subscribed to any plan or your plan has been expired please purchase plan.
					  </div>
					<?php endif; ?>
						<h4>Add Event </h4>
						<?php if(!empty($solo_event)){?>
					<a href="<?php echo base_url()?>details/<?php echo $solo_event[0]['eci_event_list_uniqid']; ?>" class="btn btn-default btn-sm pull-right margin-top" target="_blank">View Event</a>
						<?php } ?>
						<hr>
				</div>
            </div>
            <?php $currencycode=select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");?>
           	<form role="form" action="" method="post" enctype="multipart/form-data" class="eci_event_add_form">
			 <input type="hidden" id="baseUrl" value="<?php echo base_url();?>"> 
			 <input type="hidden" id="nofcat" name="noofcat" value="<?php echo isset($noofcat)? $noofcat :'';?>"> 
                <div class="col-lg-8">
                    <div class="form-group">
                  	    <p class="eci_error" id="eci_event_name_err"></p>
                        <label for="eci_event_name">Event Name <span class="eci_req_star">*</span></label>
                        <input type="text" class="form-control" id="eci_event_name" name="eci_event_name" placeholder="Enter event name" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_name']; }?>">
                    </div>
                </div>
                <div class="col-lg-6">
					<div class="form-group">
						<p class="eci_error" id="eci_event_date_err"></p>
						<label for="eci_event_date">Event Date<span class="eci_req_star">*</span></label>
						<input type="text" class="form-control" id="eci_event_date" name="eci_event_date" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_evnt_date']; }?>">
					</div>
                </div>
                <div class="col-lg-6">
				    <div class="form-group col-lg-12 col-md-12 eci_event_time_h">
                  	    <p class="eci_error"  id="eci_event_time_h_err"></p>
                        <label for="eci_event_time_h">Event Time<span class="eci_req_star">*</span></label>
					    <input type="text" class="form-control col-md-3" id="eci_event_time_h" name="eci_event_time_h" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_evnt_time']; }?>">
                    </div>
				</div>
				<div class="col-lg-12">
				    <div class="form-group col-lg-12 col-md-12 eci_event_time_h">
						<label>Do You Want Categories For Tickets</label>
						<?php $checked ='';
						    if(!empty($solo_event) && $solo_event[0]['eci_event_list_cat'] == 1){
								echo '<input type="radio" class="rbutton" id="tcatyes" name="tcat" value="1" checked>Yes'; 
						        echo '<input type="radio" class="rbutton" id="tcatno" name="tcat" value="0" disabled>No';
							} else if(!empty($solo_event) && $solo_event[0]['eci_event_list_cat'] == 0) {
								echo '<input type="radio" class="rbutton" id="tcatyes" name="tcat" value="1" disabled>Yes'; 
								echo '<input type="radio" class="rbutton" id="tcatno" name="tcat" value="0" checked>No';
							}else{?>
									<input type="radio" class="rbutton" id="tcatyes" name="tcat" value="1">Yes 
									<input type="radio" class="rbutton" id="tcatno" name="tcat" value="0" checked>No
					   <?php } ?>
					</div>
				</div>
				
					 
				<?php if(isset($solo_event[0]['eci_event_list_sno']) && $solo_event[0]['eci_event_list_max_user'] != 0){?>
				<div id="withoutTicketCat" class="">
					<div class="form-group col-lg-6 eci_max_user">
						<p class="eci_error" id="eci_no_member_err" ></p>
						<label for="eci_no_service">Maximum Members<span class="eci_req_star">*</span></label>
						<input type="text" class="form-control col-md-12" id="eci_max_users" name="eci_max_users" placeholder="" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_max_user']; }?>" >
						<p class="help-block">Booking will be ON by default. </p>
					</div>
					<div class="form-group col-lg-6 eci_max_user">
						<p class="eci_error" id="eci_no_service_err" ></p>
						<label for="eci_booking_amount">Amount to be paid by 1 member (<?php echo $currencycode; ?>)<span class="eci_req_star">*</span></label>
						<input type="text" class="form-control col-md-12" id="eci_booking_amount" name="eci_booking_amount" placeholder="" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_amount']; }?>" >
						<p class="help-block">Don't type currency symbol. </p>
					</div>
					<div class="col-lg-12">
						<div class="form-group">     
							<div class="alert alert-warning">
								<p>To make an event free just type <i>FREE</i> in <strong>Amount to be paid by 1 member</strong> text box.</p>
							</div>
						</div>
					</div>         
				</div>  
				<?php } else{
                             if(!isset($solo_event[0]['eci_event_list_sno'])){	?>
					<div id="withoutTicketCat" class="">
					<div class="form-group col-lg-6 eci_max_user">
						<p class="eci_error" id="eci_no_member_err" ></p>
						<label for="eci_no_service">Maximum Members<span class="eci_req_star">*</span></label>
						<input type="text" class="form-control col-md-12" id="eci_max_users" name="eci_max_users" placeholder="" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_max_user']; }?>" >
						<p class="help-block">Booking will be ON by default. </p>
					</div>
					<div class="form-group col-lg-6 eci_max_user">
						<p class="eci_error" id="eci_no_service_err" ></p>
						<label for="eci_booking_amount">Amount to be paid by 1 member (<?php echo $currencycode; ?>)<span class="eci_req_star">*</span></label>
						<input type="text" class="form-control col-md-12" id="eci_booking_amount" name="eci_booking_amount" placeholder="" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_amount']; }?>" >
						<p class="help-block">Don't type currency symbol. </p>
					</div>
					<div class="col-lg-12">
						<div class="form-group">     
							<div class="alert alert-warning">
								<p>To make an event free just type <i>FREE</i> in <strong>Amount to be paid by 1 member</strong> text box.</p>
							</div>
						</div>
					</div>         
				</div> 
							 <?php } } ?>
				<div id="withTicketCat" class="addTicketCat"></div>
				<?php if(isset($categoryRecords) && !empty($categoryRecords)) { ?>
				<div id="" class=""><?php echo $categoryRecords;?></div>
				<?php } ?>
                  <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_event_desc_err"></p>
                       <label for="eci_event_desc">Event Description <span class="eci_req_star">*</span></label>
                       <textarea rows="3" class="form-control" id="eci_event_desc" name="eci_event_desc"><?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_desc']; }?></textarea>
					   <script>
					    var editor = CKEDITOR.replace('eci_event_desc' , {
						} );
					   </script>
					</div>
                  </div>
				  <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_event_desc_err"></p>
                       <label for="eci_event_desc">Event Address</label>
                       <textarea rows="3" class="form-control" id="eci_event_address" name="eci_event_address"><?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_address']; }?></textarea>
					  
                       
                    </div>
                  </div>
				  <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_img_err"></p>
                       <label for="eci_img">Event Image </label>
                       <input type="file" class="form-control" id="eci_img" name="eci_img" placeholder="Enter url" >
                    </div>
                  </div>
				  
				   <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_ser_err"></p>
                       <label for="eci_ser">Services </label>
                       <input type="text" class="form-control" id="eci_ser" name="eci_ser" placeholder="Enter services"  value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_fetu']; }?>">
					   <p>Enter services with , seprated</p>
                    </div>
                  </div>
                  
                  <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_yt_url_err"></p>
                       <label for="eci_yt_url">YouTube </label>
                       <input type="text" class="form-control" id="eci_yt_url" name="eci_yt_url" placeholder="Enter url" value="<?php if(!empty($solo_event)){ echo $solo_event[0]['eci_event_list_video']; }?>">
                    </div>
                  </div>
				<div class="col-lg-12">
                  	<div class="form-group">     
						<div class="alert alert-info">
							<p><strong>Don't forget to choose a featured image for the event.</strong> </p>
						</div>
						<?php $checksubscription=checksubcription();
						if(!$checksubscription):?>
						<div class="alert alert-danger alert-dismissible fade in" role="alert">You cannot add event because
                  you are not subscribed to any plan or your plan has been expired.Please purchase plan.
						</div>
						<?php endif; ?>
					</div>
                </div>
				<div class="col-lg-12 text-center">
					<input type="submit" class="btn btn-default"  name="sub_evnt_save" value="Save" onclick="return check_add_event();" <?php if(!$checksubscription) echo "style='pointer-events:none'" ?>/>
					<button type="reset" class="btn btn-group">Reset</button>
                </div>
            </form>
           </div>
        </div>
</div>
 	
<div class="modal animated bounceInDown event_modal" id="ticketCat_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close closeBTN" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<div class="row">
					<div class="col-md-7">
						<h4 class="modal-title" id="myModalLabel">Add No.Of Categories</h4>
					</div>
					<!--<div class="col-md-4">
						<h4 class="pull-right">Add No.Of Categories</h4>
					</div>-->
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="em_person_seats_wrapper"> 
						<!-- <div class="em_person_seats"> 
							<span onclick="em_decrease_per()" class="em_person_sec">-</span>
							<span onclick="em_increase_per()"  class="em_person_sec">+</span>
						</div>-->
						<input type="number" class="form-control em_person_sec" id="ems_ticket_cat" value="2" min="1"/>
					</div>
				</div>
				<a class="btn btn-primary " id="proceed_to_category">Proceed</a>	 		  
			</div>	  
		</div>
	</div>
</div>

  <script type="text/javascript" src="<?php echo base_url();?>assets/js/admin.js"></script>


