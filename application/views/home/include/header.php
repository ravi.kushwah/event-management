<!DOCTYPE html>
<!-- 
Template Name: Event Management System - Perfect Day
Version: 1.0
Author: Kamleshyadav
Website: http://himanshusofttech.com/
Purchase: http://codecanyon.net/user/kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title>Event Management System - Perfect Day</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description"  content="Event Management"/>
<meta name="keywords" content="Event Management">
<meta name="author"  content="Kamleshyadav"/>
<meta name="MobileOptimized" content="320">
<link rel="stylesheet" href="<?php echo base_url();?>assets/back/stylesheet/jquery-ui.css">
<link href="<?php echo base_url();?>assets/back/stylesheet/jquery.toastmessage.css" rel="stylesheet" type="text/css" />
<!--start theme style -->
<link href="<?php echo base_url();?>assets/back/stylesheet/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/common.css" rel="stylesheet" type="text/css" />
<!-- end theme style -->
<link rel="shortcut icon" href="favicon.png" />

<!--Main js start-->
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/jquery.toastmessage.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/login_custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/front/javascript/em_user.js"></script>  
<script>

</script>
</head>
<body>

<div class="container">
  <div class="eci_main_container">
		<div class="eci_login_page">
			<div class="row eci_login">
				<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="eci_login_logo">
						<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>assets/back/images/login-logo.png" alt="" /></a>
					</div>
