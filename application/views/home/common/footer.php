 <?php 
		if(!empty($social_setting))
			$social_set=json_decode($social_setting[0]['eci_website_setting_value']);
		else
			$social_set=array();
		?>
 <?php 
 $subscriber_image=$this->Homemodel->get_image_name('sec_upcoming');
 ?>		
<div class="suscribe_wrapper" data-stellar-background-ratio=".5" <?php if(!empty($subscriber_image)) { ?> style="background: url(<?php echo base_url().'assets/back/eventimage/'.$subscriber_image['eci_event_image_list_name']; ?>); <?php } ?>">
    <div class="banner_overlay">
        <div class="container">
            <div class="row">
                <div class="suscribe_content">
                    <div class="col-lg-8 col-md-8">
                        <div class="suscribe_content_wrapper">
                            <div class="news_detail">
                                <h4>Subscribe Newsletter</h4>
                                
                            </div>
                            <div class="news_search_wrapper">
                                <input type="text" class="form-control search_input" placeholder="Enter your email address " id="share_email">
                                <a  class="btn search_btn share_email" >subscribe</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="social_wrapper">
                            <ul>
                               
								 <?php if(!empty($social_set)) { 
                                    if($social_set->show_to_user_fb == '1') { ?>
                                    <li><a href="<?php echo $social_set->eci_fb;?>"target="_blank"><i class="flaticon-facebook55"></i></a></li>
                                    <?php }  if($social_set->show_to_user_twitter == '1') { ?>  
                                    <li><a href="<?php echo $social_set->eci_twitter;?>" target="_blank"><i class="flaticon-twitter1"></i></a></li>
                                    <?php }  if($social_set->show_to_user_gplus == '1') { ?>
                                    <li><a href="<?php echo $social_set->eci_gplus;?>" target="_blank"><i class="flaticon-google116"></i></a></li>
                                    <?php } if($social_set->show_to_user_pinterest == '1') { ?>
                                    <li><a href="<?php echo $social_set->eci_pinterest;?>" target="_blank"><i class="flaticon-pinterest34"></i></a></li>
                                    <?php } } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="bottom_footer_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="bottom_footer">
				<?php 
			if(!empty($social_set)) { 
			if($social_set->show_to_user_copy_txt == '1') { 
				echo '<p>'.$social_set->eci_copy_txt.'</p>';
			}  } ?>
                   
                </div>
            </div>
        </div>
    </div>
	<input type="hidden" id="basepath" value="<?php echo base_url(); ?>">
</footer>
<!--main js file start--> 
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.6.0.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <!--<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>-->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquerynew.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrapnew.min.js"></script>  -->
<!--Plugin--> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/smoothscroll/smoothscroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/animate/wow.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/parallax/stellar.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/priceslider/bootstrap-slider.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugin/countdown.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/toastr.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/valid.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/em_custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/em_user.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<?php
if($this->session->flashdata('success')){?>
<script>
var flashmsg='<?php echo $this->session->flashdata('success'); ?>';
toastr.success(flashmsg,'','');
</script>
<?php }
if($this->session->flashdata('error')){?>
   <script>
var flashmsg='<?php echo $this->session->flashdata('error'); ?>';
toastr.error(flashmsg,'','');
</script>
<?php } ?>
</body>
</html>
