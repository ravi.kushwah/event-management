<!DOCTYPE html>
<!-- 
Event Management System - Perfect Day 
Version: 3.1.0
-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!--Head Start-->
<head>
<meta charset="utf-8" />
<?php 
	$seo_set ='';
	if(!empty($seo_setting)){
		if($seo_setting[0]['eci_website_setting_value'] != ''){
			$seo_set = json_decode($seo_setting[0]['eci_website_setting_value']);
		}
	} 
?>
<title><?php if(!empty($seo_set)) {  if($seo_set->eci_title != '') { echo $seo_set->eci_title; } } else { echo 'Event Management System - Perfect Day';}?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="keywords"  content="<?php if(!empty($seo_set)) { if($seo_set->eci_meta_key != '') { echo $seo_set->eci_meta_key; } } ?>"/>
<meta name="description" content="<?php if(!empty($seo_set)) { if($seo_set->eci_meta_desc != '') { echo $seo_set->eci_meta_desc; } } ?>">
<meta name="author"  content=""/>
<meta name="MobileOptimized" content="320">
<!--CSS Start-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="<?php echo base_url(); ?>assets/css/main.css?<?php echo date('h:i:s'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/css/common.css" rel="stylesheet" type="text/css"/>
    <!--CSS End-->
<!-- favicon links-->
<link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.ico" />
<?php  $breadcum_image=$this->Homemodel->get_image_name('sec_contact');
if(!empty($breadcum_image)) { ?> 
<style>
.page_wrapper {
    background-image: url(<?php echo base_url().'assets/back/eventimage/'.$breadcum_image['eci_event_image_list_name']; ?>);
}
</style>
<?php } ?>
</head>

<!--Head End-->
<!--Body Start-->
<body>      

<?php 
	if(!empty($contactus_setting))
		$cnt_data = json_decode($contactus_setting[0]['eci_contactus_data']);
	else
		$cnt_data = array();
		?>
		<?php 
		if(!empty($social_setting))
			$social_set=json_decode($social_setting[0]['eci_website_setting_value']);
		else
			$social_set=array();
		?>
<div id="preloader">
	<div id="status"> 
		<img src="<?php echo base_url(); ?>assets/images/preloader.svg" alt="" />
	</div>
</div>
<div class="top_header_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="header_top_wrapper">
				<?php if(!empty($cnt_data)) {
					if($cnt_data->show_to_user_address=='1') { ?>
                    <div class="address_wrapper"><i class="flaticon-pin56"></i><span><?php echo $cnt_data->eci_contact_address; ?></span></div>
					<?php } ?>
                    <div class="contact_wrapper">
                        <ul>
                       <?php if($cnt_data->show_to_user_email=='1') { ?>   <li><i class="flaticon-new100"></i><a href="<?php echo $cnt_data->eci_contact_email; ?>"><?php echo $cnt_data->eci_contact_email; ?></a></li><?php } ?>
                           <?php if($cnt_data->show_to_user_phone=='1') { ?>  <li><i class="flaticon-phone41"></i><span><?php echo $cnt_data->eci_contact_phone; ?></span></li><?php } ?>
                        </ul>
                    </div>
				<?php } ?>	
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                <div class="navbar_header">
                    <div class="logo"><a class="navbar_brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.png" alt=""> </a></div>
                </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="col-lg-10 col-md-10">
                <div class="menu_toggle"><span></span><span></span><span></span></div>
                <div class="real_menu">
                    <div class="close_btn"></div>
                    <div class="menu_overlay"></div>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url(); ?>" class="<?php if(isset($home_active)) echo 'active' ?>">home</a></li>
                        <!--<li><a href="<?php echo base_url('add_events'); ?>" class="<?php if(isset($add_event_active)) echo 'active' ?>">Add Event</a></li>-->
						 <?php if(!empty($social_set)) { if($social_set->show_to_user_events == '1') { ?>
					   
                        <li><i class="flaticon-arrow483"></i><a href="javascript:" class="<?php if(isset($event_active)) echo 'active' ?>">events</a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo base_url('events'); ?>">All</a></li>
                                <li><a href="<?php echo base_url('upcoming'); ?>">Upcoming</a></li>
                                <li><a href="<?php echo base_url('past'); ?>">Past</a></li>
                            </ul>
                        </li>
						 <?php }} if(!empty($social_set)) { if($social_set->show_to_user_contact == '1') { ?> 
                        <li><a href="<?php echo base_url('contact');?>" class="<?php if(isset($contact_active)) echo 'active' ?>">contact us</a></li>
						 <?php }} if(!($this->session->userdata('eci_super_id'))){ ?>
                        <li><a href="<?php echo base_url('user') ?>"  class="<?php if(isset($auth_active)) echo 'active' ?>">SignUp/Login</a></li>
					   <?php } else{ ?>
					   <li><i class="flaticon-arrow483"></i><a href="javascript:" class="<?php if(isset($auth_active)) echo 'active' ?>">My Account</a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo base_url('profile');?>">Profile</a></li>
                                <li><a href="<?php echo base_url('eventadmin/logout'); ?>">Logout</a></li>
                            </ul>
                        </li>
					   <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
