<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php $pay_currency_code = $payment_detail[0]['eci_payment_detail_ccode']; ?>
<?php 
   if(!empty($background_imgs)) {
   $sliderimgs=$background_imgs[0]['eci_website_setting_value']; 
   $sliderimgs_Arr=explode(',',$sliderimgs);
   $slider_images = $this->Homemodel->event_image_tbl('select','arr',$sliderimgs_Arr);
   }
   else
   {
   $sliderimgs=array();
   $slider_images=array();
   }?>
<?php if(!empty($slider_images) && count($slider_images) > 0) { ?>
<div class="banner_wrapper" >
<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
         <?php $slider_count=0;
            foreach($slider_images as $solo_slider_images) {
            ?>
         <li data-target="#myCarousel" data-slide-to="<?php echo $slider_count; ?>" class="<?php if($slider_count==0)  echo 'active'  ?>"></li>
         <?php $slider_count++;} ?> 
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
         <?php $slider_count=0;
            foreach($slider_images as $solo_slider_images) {
            ?>
         <div class="item <?php if($slider_count==0)  echo 'active'  ?>">
            <img src="<?php echo base_url()?>assets/back/eventimage/<?php echo $solo_slider_images['eci_event_image_list_name'];?>" alt="Los Angeles"">
         </div>
         <?php $slider_count++;} ?> 
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
      </a>
   </div>
</div>
<?php } ?>
</div>
<?php 
   if(!empty($social_setting))
   	$social_set=json_decode($social_setting[0]['eci_website_setting_value']);
   else
   	$social_set=array();
   ?>
<?php if(!empty($social_set)) { if($social_set->show_to_user_events == '1') { ?>
<?php if(!empty($mainevent)) { ?>
<div class="top_rated_wraper main_event">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="ts_single_theme_box">
               <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <div class="ts_single_theme_box_img">
                     <?php 
                        $main_img_src=base_url().'assets/back/events/default_event_image.jpg';
                        if(!empty($mainevent[0]['eci_event_list_image'])){
                         $main_img_src=base_url().'assets/back/events/'.$mainevent[0]['eci_event_list_image'];
                        }
                        $main_id=$mainevent[0]['eci_event_list_uniqid']; 
                              ?>
                     <a href="<?php echo base_url().'details/'.$main_id ?>"><img src="<?php echo $main_img_src; ?>" title="Live streaming" class="img-responsive"></a>
                  </div>
               </div>
               <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <div class="ts_single_theme_box_detail ">
                     <h3><?php  echo $mainevent[0]['eci_event_list_name'];  ?></h3>
                     <?php echo word_limiter($mainevent[0]['eci_event_list_desc'], 30);?>
                     <div class="btn_wrapper"><a href="<?php echo base_url().'details/'.$main_id ?>" class="btn rs_btn">Read more</a></div>
                     <div class="pull-right btn_wrapper">
                        <h3> <span class="label label-info"> <?php if(is_numeric($mainevent[0]['eci_event_list_amount'])) {  echo $pay_currency_code.' '.$mainevent[0]['eci_event_list_amount']; } else { echo 'FREE';}?> /Person</span></h3>
                     </div>
                     <div class="clearfix"></div>
                     <div class="feature_property_wrapper">
                        <table>
                           <tbody>
                              <tr>
                                 <?php 
                                    $date_Arr=array();
                                      $date=$mainevent[0]['eci_event_list_evnt_date'];
                                      $date_Arr=explode('/',$date);
                                      $datasolotime=$string =str_replace(' ', '',$mainevent[0]['eci_event_list_evnt_time']);
                                       $time_data=explode(':',$datasolotime);
                                                             $datasolotime  = date("H:i", strtotime($time_data[0].':'.$time_data[1] .$time_data[2]));
                                      ?>
                                 <td>Date</td>
                                 <td>-</td>
                                 <td><?php   echo date("d M Y", strtotime($date)); ?></td>
                              </tr>
                              <tr>
                                 <td>TIme</td>
                                 <td>-</td>
                                 <td><?php echo str_replace(' ', '',$mainevent[0]['eci_event_list_evnt_time']) ?></td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="em_event_date_counter">
                           <div class="data-countdown"   data-countdown="<?php echo $date_Arr[2];?>/<?php echo $date_Arr[0];?>/<?php echo $date_Arr[1];?> <?php echo $datasolotime; ?>"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="rs_tag_box" >FEATURED</div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<div class="top_rated_wraper">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="rs_heading_wrapper">
               <div class="rs_heading text-center">
                  <h3>UPCOMING EVENTS</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <div class="row">
         <?php if(!empty($upcoming_event)) { 
            foreach($upcoming_event as $solo_upcoming_event) {
            	$event_uniqueid=$solo_upcoming_event['eci_event_list_uniqid']; 
            ?> 
         <a href="<?php echo base_url().'details/'.$event_uniqueid ?>">
            <div class="col-lg-6 col-md-6">
               <div class="rated_property_wrapper left">
                  <?php 
                     $upcoming_img_src=base_url().'assets/back/events/default_event_image.jpg';
                     if(!empty($solo_upcoming_event['eci_event_list_image'])){
                      $upcoming_img_src=base_url().'assets/back/events/'.$solo_upcoming_event['eci_event_list_image'];
                     }
                     ?>
                  <div class="rated_property_img">
                     <div class="img_wrapper"> <img src="<?php echo $upcoming_img_src; ?>" alt="<?php echo $solo_upcoming_event['eci_event_list_name'];?>"> </div>
                  </div>
                  <div class="img_overlay">
                     <div class="overlay">
                        <h5><?php echo $solo_upcoming_event['eci_event_list_name'];?> </h5>
                        <span><?php if(is_numeric($solo_upcoming_event['eci_event_list_amount'])) {  echo $pay_currency_code.' '.$solo_upcoming_event['eci_event_list_amount']; } else { echo 'FREE';}?> /Person</span>
                        <?php echo word_limiter($solo_upcoming_event['eci_event_list_desc'], 20);?>
                        <table>
                           <tbody>
                              <tr>
                                 <?php 
                                    $date_Arr=array();
                                      $date=$solo_upcoming_event['eci_event_list_evnt_date'];
                                      $date_Arr=explode('/',$date);
                                      $datasolotime=$string =str_replace(' ', '',$solo_upcoming_event['eci_event_list_evnt_time']);
                                       $time_data=explode(':',$datasolotime);
                                                             $datasolotime  = date("H:i", strtotime($time_data[0].':'.$time_data[1] .$time_data[2]));
                                    		  ?>
                                 <td>Date</td>
                                 <td>-</td>
                                 <td><?php   echo date("d M Y", strtotime($date)); ?></td>
                              </tr>
                              <tr>
                                 <td>TIme</td>
                                 <td>-</td>
                                 <td><?php echo str_replace(' ', '',$solo_upcoming_event['eci_event_list_evnt_time']) ?></td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="btn_wrapper">
         <a href="<?php echo base_url().'details/'.$event_uniqueid ?>" class="btn rs_btn">Read more</a></div>
         <div class="clearfix"></div>
         <div class="em_event_date_counter">
         <div data-countdown="<?php echo $date_Arr[2];?>/<?php echo $date_Arr[0];?>/<?php echo $date_Arr[1];?> <?php echo $datasolotime; ?>"></div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </a>
         <?php } ?>
         <?php if(count($upcoming_event)>3){?>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="view_more_wrapper text-center">
               <div class="btn_wrapper"> 
                  <a href="<?php echo base_url('upcoming'); ?>" class="btn rs_btn">view all</a>
               </div>
            </div>
         </div>
         <?php } ?>		
         <?php } ?>
      </div>
   </div>
</div>
<?php  } }?>