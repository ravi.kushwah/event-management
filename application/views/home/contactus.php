<div class="page_wrapper" data-stellar-background-ratio=".5">
    <div class="banner_overlay">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="page_title">
                        <h3>Contact Us</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="breacrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php 
	if(!empty($contactus_setting))
		$cnt_data = json_decode($contactus_setting[0]['eci_contactus_data']);
	else
		$cnt_data = array();
		?>
 <div class="contactus_wraper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="rs_heading_wrapper">
                    <div class="rs_heading">
                        <h3>get in touch with us</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="contact_section">
			<form id="contactusform">
                <div class="col-lg-8 col-md-8">
                    <div class="contactus_form_wraper">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_left">
                            <div class="form-group">
                                <input type="text" class="form-control require" placeholder="YOUR NAME" id="em_name">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding_right">
                            <div class="form-group">
                                <input type="email" class="form-control require" placeholder="EMAIL ADDRESS" id="em_email" data-valid="email" data-error="Email should be valid." >
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control require" placeholder="SUBJECT" id="em_subject">
                        </div>
                        <div class="form-group">
                            <textarea rows="11" class="form-control require" placeholder="YOUR MESSAGE" id="em_msg"></textarea>
                        </div>
                        <div class="btn_wrapper">
                            <!--<button type="submit" class="btn rs_btn send_contact_query return" >submit</button>-->
                              <input type="button" class="btn rs_btn send_contact_query return" value="submit">
                            <p id="err"></p>
                        </div>
                    </div>
                </div>
				</form>
                <div class="col-lg-4 col-md-4">
                    <div class="contact_detail_wrapper">
                        <h6>Quick Contact</h6>
                        <ul>
						<?php if($cnt_data->show_to_user_address=='1') { ?>
                            <li> <i class="flaticon-pin56"></i>
                                <p><?php echo $cnt_data->eci_contact_address; ?></p>
                            </li>
						<?php } if($cnt_data->show_to_user_phone=='1') { ?>
                            <li> <i class="flaticon-phone41"></i>
                                <p><?php echo $cnt_data->eci_contact_phone; ?></p>
                            </li>
							<?php } if($cnt_data->show_to_user_email=='1') { ?>
                            <li> <i class="flaticon-mail80"></i>
                                <p><a href="mailTo:<?php echo $cnt_data->eci_contact_email; ?>"><?php echo $cnt_data->eci_contact_email; ?></a></p>
                            </li>
						   <?php  } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="contact_map">
   <iframe frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+<?php echo urlencode($cnt_data->eci_contact_address); ?>+"&amp;output=embed' width="100%" height="450"></iframe>
</div>
