<div class="eci_page_content_wrapper">
	<div class="eci_page_content">
	   <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4> Add Booking Manually</h4>
                    <hr>
                    </div>
                </div>
			
		<a href="<?php echo base_url() ?>eventadmin/bookingdetail" class="btn btn-default btn-sm pull-right">Go Back To Booking Details</a>
	  <form role="form" action="<?php echo base_url() ?>eventadmin/save_manual_booking/<?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_sno'])) { echo $edit_booking[0]['eci_payment_detail_sno']; } ?>" method="post">
	  <div class="col-lg-8">
	 
	  <div class="form-group">
	  
		<p class="eci_error" id="eci_event_name_err"> </p>
		<label for="eci_event_name">Choose An Event </label>
		<select class="form-control" <?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_email'])) { echo "disabled"; } else { ?> id="eci_book_event_list" <?php } ?> name="eci_book_event_list">
		<option value="0"> Choose one </option>
		<?php foreach ($event_list as $solo_event_list) { 
			if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_email']) && $edit_booking[0]['eci_payment_detail_email'] == $solo_event_list['eci_event_list_sno']) {
			$solo_event_Detail[]=$solo_event_list;
			$result_booking = $this->Homemodel->event_payment_detail_tbl('select','sum_booking',array('eci_payment_detail_email'=>$edit_booking[0]['eci_payment_detail_email']));
		?>
		<option value="<?php echo $solo_event_list['eci_event_list_sno'];?>" selected><?php echo $solo_event_list['eci_event_list_name'];?></option>
		<?php } else { ?>
		<option value="<?php echo $solo_event_list['eci_event_list_sno'];?>"><?php echo $solo_event_list['eci_event_list_name'];?></option>
		<?php } } ?>
		</select>
	  </div>
	  </div>
       
	<?php
		if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_msg']))
			$custDetail = json_decode($edit_booking[0]['eci_payment_detail_msg']);
		else
			$custDetail = '';
	?>
	
	<div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_cust_name_err"> </p>
		<label for="eci_event_name">Name of Customer <span class="eci_req_star">*</span></label>
		<input type="text"  class="form-control" placeholder="Name of customer" name="eci_book_cust_name" id="eci_book_cust_name" value="<?php if($custDetail != '') { 
		if(isset($custDetail->first_name) && isset($custDetail->last_name))
			echo $custDetail->first_name.' '.$custDetail->last_name;
		else
			echo $custDetail->first_name; 
			} ?>"/>
	  </div>
	  </div>
       	 				  
  	
	<div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_cust_email_err"> </p>
		<label for="eci_event_name">Email of customer <span class="eci_req_star">*</span></label>
		<input type="text"  class="form-control" placeholder="Email of Customer" name="eci_book_cust_email" id="eci_book_cust_email"  value="<?php if($custDetail != '') { 
		if(isset($custDetail->payer_email))
			echo $custDetail->payer_email; 
			} ?>"/>
	  </div>
	  </div>
       	 				  
  	
	<div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_cust_cntct_err"> </p>
		<label for="eci_event_name">Contact Number of Customer <span class="eci_req_star"></span></label>
		<input type="text"  class="form-control" placeholder="Contact number of customer" name="eci_book_cust_cntct" id="eci_book_cust_cntct"  value="<?php if($custDetail != '') { 
		if(isset($custDetail->manual_cntc_no))
			echo $custDetail->manual_cntc_no; 
			} ?>"/>
	  </div>
	  </div>
       	 				  
  	
	<div class="col-lg-8">
		<div class="form-group">
		<p class="eci_error" id="eci_book_cust_add_err"> </p>
		<label for="eci_event_name">Address of Customer </label>
		<input type="text" class="form-control" placeholder="Address of customer" name="eci_book_cust_add" id="eci_book_cust_add"  value="<?php if($custDetail != '') { 
		if(isset($custDetail->manual_address))
			echo $custDetail->manual_address; 
			} ?>"/>
	  </div>
	</div>
	<div class="col-lg-8">
		<div class="form-group" id="ticket_cate_list">
			<?php if(!empty($edit_booking) && $edit_booking[0]['eci_booked_tickets_cate'] !='null'){ 
				echo '<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p><label for="eci_event_name">Tickets Details </label><table class="detail_table"><tr>
							<th>Category</th>
							<th>Left Seats</th>
							<th>Cost/Person</th>
							<th>How Many Seats?</th>			
							</tr>';
				$total_left_seat  = json_decode($event_detail[0]['eci_event_list_catwise_seat_left']);		 
				$booked_tickets = 	json_decode($edit_booking[0]['eci_booked_tickets_cate']); 
				$cnt= 0;
				for($i=0; $i< sizeof($total_left_seat);$i++)
				{ 	$cnt++;
					$arr1[$i] = explode(',',$total_left_seat[$i]);
					$arr2[$i] = explode(',',$booked_tickets[$i]);
					if($arr1[$i][0]  == $arr2[$i][0]){
						if(isset($arr1[$i][1]) && $arr1[$i][1] != 0){
							$seat = $arr1[$i][1];
							$seat_input = '<td data-target=""><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="'.$arr2[$i][1].'" data-counter="'.$cnt.'"  class="noofseat_count"></td>';
						}else{
							$seat='All Tickets are Sold';
							$seat_input = '<td data-target=""><input type="text" name="noofseats_'.$cnt.'" id="noofseats_'.$cnt.'" value="'.$arr2[$i][1].'" data-counter="'.$cnt.'" class="noofseat_count"  readonly></td>';
						}
						echo '<tr>
								<td class="cat_name'.$cnt.'">'.$arr1[$i][0].'</td>
								<td class="cat_left_seat'.$cnt.'">'.$seat.'</td>
								<td class="cat_tic_price'.$cnt.'">'.$arr1[$i][2].'</td>
								'.$seat_input.
							'</tr>';
					}
				 }
					echo '</table>';

			} ?>
		</div>
	</div>
	<div class="col-lg-8">
		<div class="form-group" id="count_total_seat">
		<?php if(!empty($edit_booking) && $edit_booking[0]['eci_booked_tickets_cate'] !='null'){  ?> 
		<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p>
		<label for="eci_event_name">Number of Tickets</label>
		<input type="text" class="form-control noofseat_count" placeholder="Number of tickets" name="eci_book_cust_no_ticket" id="eci_book_cust_no_ticket" value="<?php if(!empty($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_ccode'])){ echo $edit_booking[0]['eci_payment_detail_ccode']; } ?>" readonly />
		
		<?php }else{ ?>
		<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p>
		<label for="eci_event_name">Number of Tickets [<span class="eci_req_star" id="eci_book_no_ticket"><?php if(isset($solo_event_Detail) && !empty($solo_event_Detail[0]['eci_event_list_max_user'])) { echo ($solo_event_Detail[0]['eci_event_list_max_user'] - $result_booking[0]['tot_count']); }else { echo "0";} ?>   </span> Ticket(s) Left ]</label>
		<input type="text" class="form-control noofseat_count" placeholder="Number of tickets" name="eci_book_cust_no_ticket" id="eci_book_cust_no_ticket" value="<?php if(!empty($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_ccode'])){ echo $edit_booking[0]['eci_payment_detail_ccode']; } ?>" />
		<?php } ?>
		</div>
	</div>
	  
	<div class="col-lg-8">
		<div class="form-group">
		<label for="eci_event_name">Booking Persons Name </label>
		<div class="persons_name">
		<p class="eci_error" id="em_person_err"> </p>
		<?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_persons_name'])):
                  
			 $persons_name= json_decode($edit_booking[0]['eci_payment_detail_persons_name']);
             foreach ($persons_name as  $name) { ?>
            <input type="text" class="form-control" name="person[]" placeholder="Person name" required="true" value="<?php echo $name ;?>">
            <?php }
			endif;?>
		</div>
		</div>
	</div>
	  <?php $currencycode=select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");?>
	  <div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_amount_err"> </p>
		<label for="eci_event_name">Payment Amount (<?php echo $currencycode; ?>)</label>
		<input type="text" class="form-control" placeholder="Payment amount" name="eci_book_amount" readonly id="eci_book_amount"  value="<?php if($custDetail != '') { 
		if(isset($custDetail->mc_fee))
			echo $custDetail->mc_fee; 
			} ?>"/>
			<?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_coupon_code'])):?>
			<span id="booking_coupon_detail_span">coupon <a target="_blank" href="<?php echo base_url(); ?>event/coupon_detail/<?php echo $edit_booking[0]['eci_payment_coupon_code']; ?>"><?php echo $edit_booking[0]['eci_payment_coupon_code']; ?></a> applied</span>
		<?php endif; ?>
	  </div>
	  </div>
	  
	  <?php 
		if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_ccode']))
			echo '<input type="hidden" id="earlierTcktCount" name="earlierTcktCount" value="'.$edit_booking[0]['eci_payment_detail_ccode'].'"> ';
		?>
			
	  <?php 
		if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_sno']))
		{
			echo '<input type="hidden" id="id_of_booking" name="id_of_booking" value="'.$edit_booking[0]['eci_payment_detail_sno'].'"> ';
			echo '<input type="hidden" name="eci_book_event_list" value="'.$edit_booking[0]['eci_payment_detail_email'].'"> ';
			echo '<input type="hidden" name="uniq_tckt_no" value="'.$edit_booking[0]['eci_payment_detail_tckt_no'].'"> ';
			echo '<input type="hidden" name="eci_payment_booking_source0" id="eci_payment_booking_source" value="'.$edit_booking[0]['eci_payment_booking_source'].'"> ';
		}
		else{
			echo '<input type="hidden" id="id_of_booking" name="id_of_booking" value="0">';
			echo '<input type="hidden" name="eci_payment_booking_source1" id="eci_payment_booking_source" value="Offline">';
		} ?>
       <input type="hidden" id="cost_of_a_ticket" name="cost_of_a_ticket" value="<?php if(isset($solo_event_Detail) && !empty($solo_event_Detail[0]['eci_event_list_amount'])) { echo $solo_event_Detail[0]['eci_event_list_amount']; } ?>"> 
       <input type="hidden" id="eci_seats_left" value="<?php if(isset($solo_event_Detail) && !empty($solo_event_Detail[0]['eci_event_list_max_user'])) {  echo ($solo_event_Detail[0]['eci_event_list_max_user'] - $result_booking[0]['tot_count']); } ?>">
	   <input type="hidden" id="noofcat" name="noofcat" value="<?php if(isset($solo_event_Detail) && !empty($solo_event_Detail[0]['eci_event_list_noofcat'])) {  echo $solo_event_Detail[0]['eci_event_list_noofcat']; }else{ echo 0; } ?>">
	   <input type="hidden" id="booked_ticket_cat" name="booked_ticket_cat" value='<?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_booked_tickets_cate'])){ echo $edit_booking[0]['eci_booked_tickets_cate']; }else{ echo ''; } ?>'>
	   <input type="hidden" id="booked_ticket_cat_old" name="booked_ticket_cat_old" value='<?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_booked_tickets_cate'])){ echo $edit_booking[0]['eci_booked_tickets_cate']; }else{ echo ''; } ?>'>
     <div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_payment_err"> </p>
		<label for="eci_event_name">Payment Status<span class="eci_req_star">*</span></label>
		<select class="form-control" id="eci_book_cust_payment_sts" name="eci_book_cust_payment_sts">
		<option value="0"> Choose one </option>
		<?php
			foreach($list_of_payment_sts as $solo_list_of_payment_sts){
				if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_type']) && $solo_list_of_payment_sts['eci_payment_status_num'] == $edit_booking[0]['eci_payment_detail_type'])
				{
					echo  '<option value="'.$solo_list_of_payment_sts['eci_payment_status_num'].'" selected>'.$solo_list_of_payment_sts['eci_payment_status_type'].'</option>';
				}
				else
				{
					echo  '<option value="'.$solo_list_of_payment_sts['eci_payment_status_num'].'">'.$solo_list_of_payment_sts['eci_payment_status_type'].'</option>';
				}
			}
		?>
		</select>
	  </div>
	</div>
      <?php if($this->session->userdata('user_type')==1){ ?>  
     <div class="col-lg-8">
	  <div class="form-group">
		<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p>
		<label for="eci_event_name">Send customer a confirmation email</label>
		<div class="checkbox">
		<input id="send_cust_email" name="send_cust_email" type="checkbox" value="1">
		<label for="send_cust_email"></label>
	  </div>
	  </div>
	  </div>
       	 				  
	<div class="col-lg-12 text-center">
		 <input type="submit" class="btn btn-default" id="check_add_booking_manual_form" onclick="return check_add_booking_manual_form_fun()" name="sub_evnt_save"  <?php if(isset($edit_booking) && !empty($edit_booking[0]['eci_payment_detail_sno'])) { ?> value="Update" <?php } else { ?> value="Save"<?php } ?>/>
		<button type="reset" class="btn btn-group">Reset</button>
	</div>
           <?php } ?>       
        </form>
	

	   </div>
	</div>
</div>
    
     