<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Past Events</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	 
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-hide="phone" data-name="Date Of Birth">Date</th>
                  <th data-hide="phone">Booking Count</th>
                </tr>
              </thead>
              <tbody>
			  <?php if(!empty($event_list)) { 
			  foreach($event_list as $solo_event_list) {
			  ?>
                <tr>
                  <td><?php echo $solo_event_list['eci_event_list_name'];?> </td>
                 
                  <td><?php echo date("d M Y", strtotime($solo_event_list['eci_event_list_evnt_date']));?></td>
                  <td>
				  <?php
				  $total_booked_seats = $this->Homemodel->event_payment_detail_tbl('select','sum_booking',array('eci_payment_detail_email'=>$solo_event_list['eci_event_list_sno']));
				  echo $total_booked_seats[0]['tot_count'];
				  ?>
				  </td>
                  
                  
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
         
          </div>
				
				
				
				
	   </div>
        </div>
    </div>