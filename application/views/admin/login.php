<?php $this->load->view('home/include/header'); ?>


					<?php if(isset($verificationsuccess)) : ?>
					<div class="alert alert-success">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> Your account has been verified  .
					</div>
					<?php endif; ?>
					<?php if(isset($passwordreset)) : ?>
					<div class="alert alert-success">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> Your Password has been reset .
					</div>
					<?php endif; ?>
					<form id="login_form" action="<?php echo base_url();?>eventadmin/super_login" method="post">
					
					  <div class="form-group">
						<label>Email</label>
						<input type="text" name="uemail" class="form-control" id="eci_uemail" value="<?php if(isset($_COOKIE['eci_remember_uemail'])) { echo $_COOKIE['eci_remember_uemail']; } ?>" placeholder="Enter Email">
						<p class="login_error" id="eci_email_err"></p>
					  </div>
					  
					  <div class="form-group">
						<label>Password</label>
						<input type="password" name="password"  class="form-control" id="eci_password" value="<?php if(isset($_COOKIE['eci_remember_password'])) { echo base64_decode($_COOKIE['eci_remember_password']); } ?>" placeholder="Password">
						<p class="login_error" id="eci_password_err"></p>
					  </div>
					  
					  <div class="checkbox">
						
						  <input type="checkbox" id="remember_me" value='1'  <?php if(isset($_COOKIE['eci_remember_uemail'])) { echo 'checked';} ?> >
          <label for="remember_me"> Stay Signed In</label>
					  </div>
					  <input type="hidden" id="rem_inp" name="rem_inp" value=""/>
              <input class="btn btn-default" type="submit" value="Login" name='getin_btn' onclick="return check_admin_login();"/>
					<p class="login_error">  <?php if(isset($err_msg)) { echo $err_msg; }?></p>
					</form>
				</div>	
				<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
				<div class="col-md-6">
				<a href="<?php echo base_url();?>user/register">Create New Account</a>
				</div>
				<div class="col-md-6">
				<a href="<?php echo base_url()?>user/forget_password">Forget Password</a>
				</div>
				</div>	
			</div>

		</div>
  </div>
  
</div>

