
<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Background Images For Section</h4>
                    <hr>
                    </div>
                </div>
				<div class="col-lg-12">
               <?php if($msg == '1'):?>
                 
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Changes has been saved successfully.
                  </div>
                <?php endif; ?>
                </div>
		<form role="form" action="<?php echo base_url()?>eventadmin/saveeventfeaturedimg" method="post">
	  <div class="col-lg-8">
	  <div class="form-group">
	  
		<p class="eci_error" id="eci_event_name_err"> </p>
		<label for="eci_event_name">Choose A Section </label>
		<select class="form-control" id="section_selection" name="section_selection">
		<option value="0"> Choose One </option>
		<option value="sec_top"> Home Slider</option>
		<option value="sec_upcoming">Subscriber section </option>
		<option value="sec_contact"> Breadcrumb  Section</option>
		
		</select>
	  </div>
	  </div>
             
		 <div class="col-lg-12 text-center loading_img">
              	 <div class="eci_setimage_loading"><img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" /></div>
              </div>
              <div class="clearfix"></div>
           
              <div class="alert alert-info setimage_sec">
                	<p><strong>Friendly Note :-</strong> Scroll Down to view images which you have uploaded earlier. You can set only one (1) image at a time as a Background Image for the section but there is no limit in the Slider Images of Top Section. </p>
              </div>
              
              
           
           <div class="eci_setimage_wrapper">
               <div class="row" id="displaysec">
                  
                   
               </div>
           </div>
           
           <div class="col-lg-12 text-center setimage_sec">
           	  <input type="submit" class="btn btn-default eci_pull_down_30" name="sub_save_feature" value="Save Changes" />
           </div>
             
			 				  
		</form>

		
	   </div>
        </div>
    </div>