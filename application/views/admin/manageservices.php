<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Manage Service</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	  
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="phone">#</th>
                  <th data-toggle="true">Name</th>
                  <th data-toggle="phone">Status</th>
                  <th data-hide="phone">Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php if(!empty($service_list)) { 
			  $sno=0;
			  foreach($service_list as $solo_service_list) {
			  $sno++;
			  ?>
                <tr>
                  <td><?php echo $sno;?> </td>
				   
				   <td><?php echo $solo_service_list['eci_event_service_list_name'];?> </td>
				  
				  <td>  <?php if($solo_service_list['eci_event_service_list_status'] == '1') { echo 'Enable' ; } else { echo 'Disable' ; }?> </td>
                 
                  <td>
                  <span><a href="<?php echo base_url();?>eventadmin/updateexistingservices/<?php echo $solo_service_list['eci_event_service_list_sno'];?>" title="EDIT"><img src="<?php echo base_url();?>assets/back/images/icons/edit.png" alt="EDIT" /></a></span>
				  <?php if($solo_service_list['eci_event_service_list_status'] == '1') { ?>
				  
                  <span><a href="<?php echo base_url();?>eventadmin/delete_n_change/serv/<?php echo $solo_service_list['eci_event_service_list_sno'];?>/0" title="DISABLE"><img src="<?php echo base_url();?>assets/back/images/icons/disable.png" alt="DISABLE" /></a></span>
				  <?php } else { ?>
				  
				  <span><a href="<?php echo base_url();?>eventadmin/delete_n_change/serv/<?php echo $solo_service_list['eci_event_service_list_sno'];?>/1" title="ENABLE"><img src="<?php echo base_url();?>assets/back/images/icons/enable.png" alt="ENABLE" /></a></span>
				  
				  <?php } ?>
                  <span><a href="<?php echo base_url();?>eventadmin/delete_n_change/serv/<?php echo $solo_service_list['eci_event_service_list_sno'];?>/del" title="DELETE"><img src="<?php echo base_url();?>assets/back/images/icons/delete.png" alt="DELETE" /></a></span>
                  </td>
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
          
          </div>
				
				
				
				
	   </div>
        </div>
    </div>