<link href="<?php echo base_url();?>assets/css/common.css" rel="stylesheet" type="text/css"/>
<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>List of Contact Query</h4>
					<a href="<?php echo base_url()?>eventadmin/contactquery_csv" class="btn btn-default btn-sm pull-right margin-top">Download CSV</a>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	 
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="true">SNo.</th>
                  <th data-hide="phone" data-name="Date Of Birth">Name</th>
                  <th data-hide="phone">Email</th>
                  <th data-hide="phone">Subject</th>
                  <th data-hide="phone">Message</th>
                  <th data-hide="phone">Date</th>
                </tr>
              </thead>
              <tbody>
			  <?php if(!empty($contactquery_list)) { 
			  $cont_sno=0;
			  foreach($contactquery_list as $solo_contactquery_list) {
			  $cont_sno++;
			  $cnt_data = json_decode($solo_contactquery_list['eci_contactus_data']);
			  ?>
                <tr>
                  <td><?php echo $cont_sno;?> </td>
                  <td><?php echo $cnt_data->users_name;?> </td>
                  <td><?php echo $cnt_data->users_email;?> </td>
                  <td><?php echo $cnt_data->users_subject;?> </td>
                  <td><?php echo $cnt_data->users_message;?> </td>
                  <td><?php echo date("d M Y", strtotime($cnt_data->date));?></td>
                                 
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
          </div>
	   </div>
        </div>
    </div>
