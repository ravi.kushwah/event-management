<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css" />

<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Manage Event</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	 
           	<table id="datatable_tbl" class="display table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
				<th data-toggle="true">Sno</th>
                  <th data-toggle="true">Name</th>
                  <th>Price / Person</th>
                  <th data-hide="phone" data-name="Date Of Birth">Date</th>
                  <th data-hide="phone">Booking Count</th>
				  <?php if($this->session->userdata('user_type')==1){ ?>
                 <th data-hide="phone">Set Featured</th>
				  <?php } ?>
                  <th data-hide="phone">UpComing</th>
                  <th data-hide="phone">Action</th>
                </tr>
              </thead>
              <tbody>
			  <input type="hidden" id="basepath" value="<?php echo base_url();?>">
			  <?php if(!empty($event_list)) {
					$cnt=1;
			  foreach($event_list as $solo_event_list) {
				if(is_numeric($solo_event_list['eci_event_list_amount']) && $solo_event_list['eci_event_list_amount'] !=0)
				{
					$amount = $solo_event_list['eci_event_list_amount'];

     			}else if(isset($solo_event_list['eci_event_list_amount']) && $solo_event_list['eci_event_list_categories']!='null'){
						$amount = '<a onclick="showAmt_popup('.$solo_event_list['eci_event_list_sno'].')">Click Here</a>';
					}else{
							$amount = 'Free';
						}  ?>
						
                <tr>
				<td><?php echo $cnt++;?> </td>
                  <td><?php echo $solo_event_list['eci_event_list_name'];?> </td>
                  <td><?php echo $amount;?> </td>
                  <td><?php echo date("d M Y", strtotime($solo_event_list['eci_event_list_evnt_date']));?></td>
                  <td><?php 
				  $total_booked_seats = $this->Homemodel->event_payment_detail_tbl('select','sum_booking',array('eci_payment_detail_email'=>$solo_event_list['eci_event_list_sno']));
//				  echo $total_booked_seats[0]['tot_count'];
				  if($solo_event_list['eci_event_list_booking'] == '1')
				 {
					$book='checked';
					$booktext='ON';
				 }
				 else
				 {
					$book='';
					$booktext='OFF';
				 }?>
				 
				 <span> <div class="checkbox">
		<input type="checkbox"  id="booking_check_<?php echo $solo_event_list['eci_event_list_sno'];?>" onclick="updateeventssetting_fun(<?php echo $solo_event_list['eci_event_list_sno'];?>,'book')" <?php echo $book; ?> />
		<label  for="booking_check_<?php echo $solo_event_list['eci_event_list_sno'];?>" id="booking_text_<?php echo $solo_event_list['eci_event_list_sno'];?>" class="hidebookingcheck_<?php echo $solo_event_list['eci_event_list_sno'];?>"><?php echo $booktext; ?> </label>
	</div>   </span>
	 <img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" class="loading_img" id="booking_<?php echo $solo_event_list['eci_event_list_sno'];?>" />
	
				  </td>
                  <?php if($this->session->userdata('user_type')==1){ ?>
                  <td>
				  
				 <?php if($solo_event_list['eci_event_list_featured'] == '1')
				 {
					$radio='checked';
				 }
				 else
				 {
					$radio='';
				 }?>
				 <div class="radio">
				<input type="radio" id="setfeature_radio_<?php echo $solo_event_list['eci_event_list_sno'];?>" name="setfeature" onclick="updateeventssetting_fun(<?php echo $solo_event_list['eci_event_list_sno'];?>,'feature')" <?php echo $radio; ?>/> 
				
				<label for="setfeature_radio_<?php echo $solo_event_list['eci_event_list_sno'];?>" class="hideradio_<?php echo $solo_event_list['eci_event_list_sno'];?>" ></label>
                            </div>
				 
				  <img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" class="loading_img" id="feature_<?php echo $solo_event_list['eci_event_list_sno'];?>" />
				  
				  </td>
				  <?php } ?>
                  <td>
				  
				<?php if($solo_event_list['eci_event_list_upcoming'] == '1')
				 {
					$check='checked';
				 }
				 else
				 {
					$check='';
				 }?>

	<div class="checkbox">
		<input type="checkbox"  id="setupcoming_check_<?php echo $solo_event_list['eci_event_list_sno'];?>" onclick="updateeventssetting_fun(<?php echo $solo_event_list['eci_event_list_sno'];?>,'upcom')"  <?php echo $check; ?>/>
		<label  for="setupcoming_check_<?php echo $solo_event_list['eci_event_list_sno'];?>" class="hidecheck_<?php echo $solo_event_list['eci_event_list_sno'];?>"></label>
	</div>
	
				<img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" class="loading_img" id="upcoming_<?php echo $solo_event_list['eci_event_list_sno'];?>" />
				
				  </td>
                   <td>
                  <span><a href="<?php echo base_url();?>eventadmin/updateexistingevent/<?php echo $solo_event_list['eci_event_list_sno'];?>" data-valid="<?php echo $solo_event_list['eci_event_list_sno'];?>" title="EDIT" ><img src="<?php echo base_url();?>assets/back/images/icons/edit.png" alt="EDIT" /> </a></span>
				 <?php if($solo_event_list['eci_event_list_featured'] == '1') { ?>
                  <span><img src="<?php echo base_url();?>assets/back/images/icons/delete.png" alt="DELETE" onclick="show_no_dlt_msg()" class="cursor"/></span>
				   <?php } else { ?>
				   
				    <span><a href="<?php echo base_url();?>eventadmin/delete_n_change/event/<?php echo $solo_event_list['eci_event_list_sno'];?>/del" title="DELETE" onclick=" return confirm('Are you sure you want to delete this event?');"><img src="<?php echo base_url();?>assets/back/images/icons/delete.png" alt="DELETE" /></a></span>
					
				   <?php } ?>
                  </td>
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
         
          </div>
				
				
				
				
	   </div>
        </div>
    </div>
	 <input type="hidden" id="basepath" value="<?php echo base_url();?>">
	 
	 
	 <div class="modal animated bounceInDown event_modal" id="amount_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<div class="row">
					<div class="col-md-7">
						<h4 class="modal-title" id="myModalLabel">Category Wise Price/Person</h4>
						<!--<h4 class="pull-right">Category Wise Price/Person</h4>-->
					</div>
					<!--<div class="col-md-4">
						<h4 class="pull-right">Category Wise Price/Person</h4>
					</div>-->
				</div>
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
					  <tr>
						<th>Category</th>
						<th>Price/Person</th>
					  </tr>
					</thead>
					<tbody id="row">
					  
					</tbody>
				</table>	 		  
			</div>	  
		</div>
	</div>
</div>
