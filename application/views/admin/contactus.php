    <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Details for "Contact us  " section !</h4>
                    <hr>
                    </div>
                </div>
           		<?php
				$cnt_data = json_decode($contactus_detail[0]['eci_contactus_data']);
				?>
                <div class="col-lg-12">
                	<div class="eci_contact_form">
                    <form class="form-horizontal" role="form" method="post" action="">
                      
                      <div class="form-group">
                        <label for="eci_contact_email" class="col-sm-2 control-label">Email :</label>
                        <div class="col-sm-6">
                          <input type="email" class="form-control" id="eci_contact_email"  name="eci_contact_email" placeholder="example@gmail.com" value="<?php if($cnt_data->eci_contact_email != '') { echo $cnt_data->eci_contact_email; } ?>">
                        </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_email" name="show_to_user_email" type="checkbox" <?php if($cnt_data->show_to_user_email == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_email">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                        <label for="eci_contact_phone" class="col-sm-2 control-label">Phone :</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_contact_phone" name="eci_contact_phone" placeholder="0000000000"  value="<?php if($cnt_data->eci_contact_phone != '') { echo $cnt_data->eci_contact_phone; } ?>">
                        </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_phone" name="show_to_user_phone" type="checkbox" <?php if($cnt_data->show_to_user_phone == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_phone">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                        <label for="eci_contact_address" class="col-sm-2 control-label">Address :</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_contact_address" name="eci_contact_address" placeholder="Address"  value="<?php if($cnt_data->eci_contact_address != '') { echo $cnt_data->eci_contact_address; } ?>">
                          </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_address" name="show_to_user_address" type="checkbox" <?php if($cnt_data->show_to_user_address == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_address">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                      	 <label class="col-sm-5 control-label">Email on which user will send contact query :</label>
                        <div class="col-sm-7">
                          <div class="radio">
                            <input id="yes" type="radio" name="send_contact_query" value="yes" <?php if($cnt_data->send_contact_query == 'yes') { echo 'checked'; } ?>  onclick="enter_new_email()">
                            <label for="yes">Use the above email</label>
                            <span><strong>Or</strong></span>&nbsp;&nbsp;&nbsp; 
                            <input id="no" type="radio" name="send_contact_query" value="no" onclick="enter_new_email()" <?php if($cnt_data->send_contact_query == 'no') { echo 'checked'; } ?>>
                            <label for="no">Enter new email</label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group eci_new_email">
                        <div class="col-sm-2">
                        
                        <label for="eci_contact_new_email" class="control-label">Enter new email :</label>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_contact_new_email" name="eci_contact_new_email" placeholder="example@gmail.com"  value="<?php if($cnt_data->eci_contact_new_email != '') { echo $cnt_data->eci_contact_new_email; } ?>">
                          </div>
                        <div class="col-sm-2">
                        </div>
                      </div>
                      
                      
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <div class="checkbox">
                            <input id="save_data_db" name="save_data_db" type="checkbox" <?php if($cnt_data->save_data_db == '1') { echo 'checked'; } ?>>
                            <label for="save_data_db">Save  User's Info to Database <i class="fa fa-database fa-lg"></i></label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default" onclick="return check_contact_setting()" name="save_contact_sub">Save Changes</button>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
                
           </div>
		   
		   
		   
		   
        </div>
    </div>
	
	<?php if($cnt_data->send_contact_query == 'yes') { ?>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/admin.js"></script>
<?php } ?>
