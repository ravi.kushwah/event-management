<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Manage Membership Plan</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	 
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                <?php 
                $currencycode=select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");
                ?>
                  <th data-toggle="true">Name</th>
                  <th>Price(<?php echo $currencycode; ?>)</th>
                  <th data-hide="phone" data-name="Date Of Birth">Duration(DAYS)</th>
                  <th data-hide="phone">Plan Type</th>
                  <th data-hide="phone">Plan Status</th>
				  <th data-hide="phone">Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php if(!empty($plan_list)) { 
			  foreach($plan_list as $solo_plan_list) {
			  ?>
        <?php
                    $plantype=$solo_plan_list['eci_plan_type'];
                    if($plantype==1){
                      $plantype='Commision';
                    }else{
                      $plantype='Time Based';
                    }
                     $planstatus=$solo_plan_list['eci_plan_status'];
                    if($planstatus==1){
                      $planstatus='Active';
                    }else{
                      $planstatus='Inactive';
                    }
                   ?>
                <tr>
                  <td><?php echo $solo_plan_list['eci_plan_name'];?> </td>
                  <td><?php echo $solo_plan_list['eci_plan_price']; if($plantype=='Commision') echo ' % commision';?></td>
                  <td><?php echo $solo_plan_list['eci_plan_duration'];?></td>
                  
                  <td><?php echo $plantype;?></td>
                   <td><?php echo $planstatus;?></td>
				  <td>
                  <span><a href="<?php echo base_url();?>event/updatemembershipplan/<?php echo $solo_plan_list['eci_plan_id'];?>" title="EDIT"><img src="<?php echo base_url();?>assets/back/images/icons/edit.png" alt="EDIT" /> </a></span>
                  
                  
                  </td>
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
         
          </div>
				
				
				
				
	   </div>
        </div>
    </div>