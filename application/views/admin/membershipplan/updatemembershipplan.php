
   <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
              <div class="col-lg-12">
                  <div class="eci_heading">
                    <h4>Update Membership Plan </h4>
                    <hr>
                    </div>
                </div>
        
             <form role="form" action="<?php echo base_url()?>event/updatemembershipplan/<?php echo  $solo_plan[0]['eci_plan_id']; ?>" method="post">
        <?php $currencycode=select_single_data("eci_payment_detail","where eci_payment_detail_sno=1","eci_payment_detail_ccode");?>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_plan_name_err"> </p>
                    <label for="eci_service_name">Plan Name <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_plan_name" name="eci_plan_name" placeholder="Enter plan name" value="<?php echo $solo_plan[0]['eci_plan_name']; ?>">
                  </div>
                  </div>
                   
                  <div class="plan_duration">
                  <input type="hidden" class="" id="eci_plan_type" name="eci_plan_type"  value="<?php echo $solo_plan[0]['eci_plan_type'];?>">
                   <?php if($solo_plan[0]['eci_plan_type']==2) { ?>
                    <div class="col-lg-8">
                      <div class="form-group">
                      <p class="eci_error" id="eci_plan_duration_err"> </p>
                      <label for="eci_service_name">Plan Duration <span class="eci_req_star">*</span></label>
                      <input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Duration in days" value="<?php echo $solo_plan[0]['eci_plan_duration']; ?>">
                      </div>
                    </div> 
                   <?php } ?>
                  </div>

                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_plan_price_err"> </p>
                    <label for="eci_service_name">Plan Price (<?php echo $currencycode; ?> ) <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_plan_price" name="eci_plan_price" placeholder="Enter plan price" value="<?php if(!empty($solo_plan)) { echo $solo_plan[0]['eci_plan_price']; }?>">
                    <span class="label label-info">If plan type is commision then plan price is plan commision</span>
                  </div>
                  </div>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_plan_type_err"> </p>
                    <label for="eci_service_name">Plan Status <span class="eci_req_star">*</span></label>
                    <select id="eci_plan_status" name="eci_plan_status" class="form-control">
                    <option <?php if($solo_plan[0]['eci_plan_status']==1) { echo "selected"; }?> value="1">Active</option>
                    <option <?php if($solo_plan[0]['eci_plan_status']==0) { echo "selected"; }?> value="0">Inactive</option>
                    </select>
                  </div>
                  </div>
            
                  
             <div class="col-lg-12 text-center">
                  <input type="submit" class="btn btn-default" onclick=" return check_add_membership_plan()" name="sub_service_save" value="Save" />
          
                  <button type="reset" class="btn btn-group">Reset</button>
                  </div>
                </form>
           </div>
        </div>
    </div>

