
   <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Add Service</h4>
                    <hr>
                    </div>
                </div>
          <div class="col-lg-12">
                <?php if($msg == '1'):?>
                 
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Service has been saved successfully.
                  </div>
                <?php endif; ?>
                </div> 
				<?php if(!empty($solo_service)) { ?>
		<form role="form" action="<?php echo base_url()?>eventadmin/updateexistingservices/<?php echo $solo_service[0]['eci_event_service_list_sno'];?>" method="post">
				<?php } else { ?>
           		<form role="form" action="<?php echo base_url()?>eventadmin/saveservices" method="post">
				<?php } ?>
                  <div class="col-lg-8">
                  <div class="form-group">
                  	<p class="eci_error" id="eci_service_name_err"> </p>
                    <label for="eci_service_name">Service Name <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_service_name" name="eci_service_name" placeholder="Enter service name" value="<?php if(!empty($solo_service)) { echo $solo_service[0]['eci_event_service_list_name']; }?>">
                  </div>
                  </div>
                  
                  
                  <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_service_desc_err"></p>
                       <label for="eci_service_desc">Service Description <span class="eci_req_star">*</span></label>
        <textarea rows="3" class="form-control" id="eci_service_desc" name="eci_service_desc"> <?php if(!empty($solo_service)) { echo $solo_service[0]['eci_event_service_list_des']; }?></textarea>
                    </div>
                  </div>
                  
				  
				  
                 <div class="col-lg-12">
                  	<div class="form-group">
                    	<p class="eci_error" id="eci_service_desc_err"></p>
                       <label for="eci_service_desc">Choose images for the service<span class="eci_req_star">*</span></label>
                    </div>
                  </div>
                  
    
          <?php
		  if(!empty($images_list)) {
		  echo ' <div class="eci_setimage_wrapper"><div class="row">';
		  foreach($images_list as $solo_images_list)
		{       
			if(!empty($solo_service)) { 
			if(substr_count($solo_service[0]['eci_event_service_list_img'],$solo_images_list['eci_event_image_list_sno']) != 0)
				$checked="checked";
			else
				$checked="";
			}
			else
			{
				$checked="";
			}
			 
            echo '<div class="col-lg-4 col-md-4 col-sm-4"><div class="eci_setimage"> <img src="'.base_url().'assets/back/eventimage/'.$solo_images_list['eci_event_image_list_name'].'" />
			<div class="checkbox">  <input id="checkbox_'.$solo_images_list['eci_event_image_list_sno'].'" type="checkbox" name="serviceimg[]" value="'.$solo_images_list['eci_event_image_list_sno'].'" '.$checked.'> <label for="checkbox_'.$solo_images_list['eci_event_image_list_sno'].'"></label> </div></div> </div>';
			}
			echo ' </div></div>';
			}
			else
			{
				echo ' <div class="col-lg-12">
                  	<div class="form-group"><p class="eci_error service">Please Upload Images first. Click <a href="'.base_url().'eventadmin/uploadimage"> HERE </a> to go to  Upload Page.</p> </div>
                  </div>';
			}
			
			?>
			
              
                                    
                  <div class="col-lg-12 text-center">
                  <input type="submit" class="btn btn-default" onclick="return check_add_service_form()" name="sub_service_save" value="Save" />
				  
                  <button type="reset" class="btn btn-group">Reset</button>
                  </div>
                </form>
           </div>
        </div>
    </div>
