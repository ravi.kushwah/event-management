<div class="page_wrapper" data-stellar-background-ratio=".5">
    <div class="banner_overlay">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="page_title">
                        <h3>Authenticate</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="breacrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li class="active">Authenticate</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php 
	if(!empty($contactus_setting))
		$cnt_data = json_decode($contactus_setting[0]['eci_contactus_data']);
	else
		$cnt_data = array();
		?>
 <div class="contactus_wraper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="rs_heading_wrapper">
                    <div class="rs_heading">
                        <h3>Login</h3>
                    </div>
                </div>
            </div>
			 <div class="col-lg-6">
                <div class="rs_heading_wrapper">
                    <div class="rs_heading">
                        <h3>Register</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="contact_section">
			<form id="login_form" action="<?php echo base_url();?>eventadmin/super_login" method="post">
                <div class="col-lg-6 col-md-6">
                    <div class="contactus_form_wraper">
                            <div class="form-group">
                                <input type="text" class="form-control require" name="uemail" id="eci_uemail" placeholder="Enter Email"  value="<?php if(isset($_COOKIE['eci_remember_uemail'])) { echo $_COOKIE['eci_remember_uemail']; } ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control require" placeholder="Password" id="eci_password" value="<?php if(isset($_COOKIE['eci_remember_password'])) { echo base64_decode($_COOKIE['eci_remember_password']); } ?>" data-valid="email" data-error="Email should be valid." >
                            </div>
							<div class="checkbox_wrapper">
								<div class="checkbox">
								  <input type="checkbox" id="remember_me" value='1'  <?php if(isset($_COOKIE['eci_remember_uemail'])) { echo 'checked';} ?> >
								   <label for="remember_me"> Stay Signed In</label>
								 </div>
								 <div class="forget_link"><a href="<?php echo base_url('forget');?>">forgot password</a></div>
					         </div>
							 <input type="hidden" id="rem_inp" name="rem_inp" value=""/>
                       
                        <div class="btn_wrapper">
                            <button type="submit" class="btn rs_btn" name='getin_btn' onclick="return check_admin_login();">Login</button>
							

                           
                        </div>
                    </div>
                </div>
				</form>
				<form action="<?php echo base_url();?>user/register" method="post" autocomplete="off">
                <div class="col-lg-6 col-md-6">
                   <div class="contactus_form_wraper">
                            <div class="form-group">
                                <input type="text" name="runame" class="form-control" id="reci_uname"  placeholder="Enter Username">
                            </div>
							<div class="form-group">
                                <input type="text" name="ruemail" class="form-control" id="reci_uemail"  placeholder="Enter User Email">
                            </div>
							<div class="form-group">
                                <input type="password" name="rupassword"  class="form-control" id="reci_upassword"  placeholder="Password">
                            </div>
							<div class="form-group">
                                <input type="password" name="rucpassword"  class="form-control" id="reci_ucpassword"  placeholder="Confirm password">
                            </div>
                       
                        <div class="btn_wrapper">
                            <button type="submit" class="btn rs_btn" onclick="return check_user_register();">Sign Up</button>
                            <p id="err"></p>
                        </div>
                    </div>
                </div>
				</form>
            </div>
        </div>
    </div>
</div>

