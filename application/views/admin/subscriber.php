<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Subscriber list</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
          	 
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="true">SNo.</th>
                  <th data-hide="phone">Email</th>
                  <th data-hide="phone">Date</th>
                </tr>
              </thead>
              <tbody>
			  <?php if(!empty($subscriber_list)) { 
			  $cont_sno=0;
			  foreach($subscriber_list as $solo_subscriber_list) {
			  $cont_sno++;
			  ?>
                <tr>
                  <td><?php echo $cont_sno;?> </td>
                  <td><?php echo $solo_subscriber_list['sub_email'];?> </td>
                  <td><?php echo date("d M Y", strtotime($solo_subscriber_list['date']));?></td>
                                 
                </tr>
				<?php } } ?>
              </tbody>
             
            </table>
         
          </div>
				
				
				
				
	   </div>
        </div>
    </div>