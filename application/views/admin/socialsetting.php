    <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Social Setting</h4>
                    <hr>
                    </div>
                </div>
           	<?php
			$social_set=json_decode($social_setting[0]['eci_website_setting_value']);
			?>	
                <div class="col-lg-12">
                	<div class="eci_contact_form">
                    <form class="form-horizontal" role="form" method="post"  enctype="multipart/form-data" action="">
                      
                      <div class="form-group">
                        <label for="eci_fb" class="col-sm-2 control-label"><i class="fa fa-facebook fa-lg"></i></label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_fb"  name="eci_fb" placeholder="Facebook page url"  value="<?php if($social_set->eci_fb != '') { echo $social_set->eci_fb; } ?>">
                        </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_fb" name="show_to_user_fb" type="checkbox" <?php if($social_set->show_to_user_fb == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_fb">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                        <label for="eci_twitter" class="col-sm-2 control-label"><i class="fa fa-twitter fa-lg"></i></label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_twitter" name="eci_twitter" placeholder="Twitter profile url"  value="<?php if($social_set->eci_twitter != '') { echo $social_set->eci_twitter; } ?>" >
                        </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_twitter" name="show_to_user_twitter" type="checkbox"  <?php if($social_set->show_to_user_twitter == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_twitter">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                        <label for="eci_gplus" class="col-sm-2 control-label"><i class="fa fa-google-plus fa-lg"></i></label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_gplus" name="eci_gplus" placeholder="Google plus url"  value="<?php if($social_set->eci_gplus != '') { echo $social_set->eci_gplus; } ?>" >
                          </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_gplus" name="show_to_user_gplus" type="checkbox" <?php if($social_set->show_to_user_gplus == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_gplus">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                       <div class="form-group">
                        <label for="eci_pinterest" class="col-sm-2 control-label"><i class="fa fa-pinterest fa-lg"></i></label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_pinterest" name="eci_pinterest" placeholder="Pinterest url"  value="<?php if($social_set->eci_pinterest != '') { echo $social_set->eci_pinterest; } ?>" >
                          </div>
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_pinterest" name="show_to_user_pinterest" type="checkbox" <?php if($social_set->show_to_user_pinterest == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_pinterest">Show to user</label>
                          </div>
                        </div>
                      </div>
                      
                      
                <div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Enable or disable front sections</h4>
                    <hr>
                    </div>
                </div>
				
				 <div class="col-lg-12">
                  	<div class="form-group">     
			<div class="alert alert-info">
                	<p>uncheck the respective checkbox to disable sections . they will also disappear from the menu. </p>
            </div>
			 </div>
                  </div>
				  
                      <p class="help-block"></p> 
                     <div class="form-group hide">
                        <label for="eci_logo" class="col-sm-2 control-label">Services</label>
                        <div class="col-sm-6">
                        	<div class="checkbox">
                            <input id="show_to_user_services" name="show_to_user_services" type="checkbox" <?php if($social_set->show_to_user_services == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_services">Show to user</label>
                          </div>
                          </div>
                       
                    </div>
                      
                       <div class="form-group">
                        <label for="eci_logo" class="col-sm-2 control-label">Events</label>
                        <div class="col-sm-6">
                        	<div class="checkbox">
                            <input id="show_to_user_events" name="show_to_user_events" type="checkbox" <?php if($social_set->show_to_user_events == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_events">Show to user</label>
                        </div>
                          </div>
                    </div>
                      
                       <div class="form-group hide">
                        <label for="eci_logo" class="col-sm-2 control-label">YouTube Video</label>
                        <div class="col-sm-6">
                        	<div class="checkbox">
                            <input id="show_to_user_ytvideo" name="show_to_user_ytvideo" type="checkbox" <?php if($social_set->show_to_user_ytvideo == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_ytvideo">Show to user</label>
                        </div>
                          </div>
                    </div>
                      
                       <div class="form-group">
                        <label for="eci_logo" class="col-sm-2 control-label">Contact</label>
                        <div class="col-sm-6">
                        	<div class="checkbox">
                            <input id="show_to_user_contact" name="show_to_user_contact" type="checkbox" <?php if($social_set->show_to_user_contact == '1') { echo 'checked'; } ?> >
                            <label for="show_to_user_contact">Show to user</label>
                        </div>
                          </div>
                    </div>
                      
                      
                        
                <div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Site Logo</h4>
                    <hr>
                    </div>
                </div>
                      
                     <div class="form-group">
                        <label for="eci_logo" class="col-sm-2 control-label">Upload Logo (762 * 310 px)</label>
                        <div class="col-sm-6">
                          <input type="file" name="userfile">
		 <p class="help-block"> <img src="<?php echo base_url()?>assets/front/images/logo.png" alt="logo" class="eci_logo_sett"></p>
                          </div>
                       
                    </div>
                      
                      
                     
                      
                <div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Copyright</h4>
                    <hr>
                    </div>
                </div>
                      
                     <div class="form-group">
                        <label for="eci_copy_txt" class="col-sm-2 control-label">Copyright Text</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_copy_txt" name="eci_copy_txt" placeholder="Copyright text"  value="<?php if($social_set->eci_copy_txt != '') { echo $social_set->eci_copy_txt; } ?>"  >
						   <p class="help-block">For &copy; Symbol use <xmp>&copy; </xmp> </p>
                          </div>
                       <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_to_user_copy_txt" name="show_to_user_copy_txt" type="checkbox"  <?php if($social_set->show_to_user_copy_txt == '1') { echo 'checked'; } ?>>
                            <label for="show_to_user_copy_txt">Show to user</label>
                          </div>
                        </div>
                    </div>
                  
				  

					<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Admin Credentials</h4>
                    <hr>
                    </div>
                </div>
                      
                     <div class="form-group">
                        <label for="eci_display_name" class="col-sm-2 control-label">Login Email</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_display_email" name="eci_display_email" placeholder="Login name"  value="<?php echo $admin_credentials[0]['eci_admin_email']; ?>">
                          </div>
                       
                    </div>
                      
                    <div class="form-group">
                        <label for="eci_pwd" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-6">
                          <input type="password" class="form-control" id="eci_pwd" name="eci_pwd"> 
						  <input type="hidden" class="form-control" name="eci_original_pwd" value="<?php echo $admin_credentials[0]['eci_admin_pwd']; ?>">
                          </div>
                       
                    </div>
                    
                     
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default" name="save_web_settings">Save Changes</button>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
                
           </div>
		   
		   
		   
		   
        </div>
    </div>