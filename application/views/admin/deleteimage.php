<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Delete Image</h4>
                    <hr>
                    </div>
                </div>
				
				<div class="col-lg-12">
        <?php if(isset($msg)): ?>
          	 <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $msg; ?>
            </div>
          <?php endif; ?>
           	<table id="datatable_tbl" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th data-toggle="true">Image</th>
                  
                  
				  <th data-hide="phone">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($images)): ?>
              <?php foreach($images as $image) :?>
                <tr>
                
                  
                  <td><img src="<?php echo base_url();?>assets/back/eventimage/<?php echo $image['eci_event_image_list_name'];?>" width=200px height=200px ></td>
                  <td><span><a href="<?php echo base_url();?>event/delete_image/<?php echo $image['eci_event_image_list_sno']?> " title="DELETE" onclick="return confirm('Are you sure you want to delete this image?');"><img src="<?php echo base_url();?>assets/back/images/icons/delete.png" alt="DELETE" /></a></span></td>
                  </tr>
               <?php endforeach; ?>  
               <?php endif; ?> 
                
				
              </tbody>
             
            </table>
         
          </div>
				
				
				
				
	   </div>
        </div>
    </div>