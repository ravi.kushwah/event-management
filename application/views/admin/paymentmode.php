<div class="eci_page_content_wrapper">
  <div class="eci_page_content">
    <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Payment Method Section</h4>
                    <hr>
                    </div>
                </div>
           		
        <div class="col-lg-12 ep-mode">
          <div class="eci_contact_form">
              <?php
               $eventpaymentmod=select_single_data("eci_website_setting","where eci_website_setting_name='event_payment_mode'","eci_website_setting_value");
			  if($eventpaymentmod){
				$eventpaymentmod=explode(',',$eventpaymentmod);  
			  }else{
				 $eventpaymentmod=array(); 
			  }
              ?>
            <form id="eventmethodform">
      				<div class="form-group">
      					<label class="col-sm-5 control-label">Payment Method For Event  :</label>
      					<div class="col-sm-7">
      					<div class="checkbox">
						<input type="checkbox" id="paypal" <?php if(in_array('paypal',$eventpaymentmod)) echo 'checked'; ?> >
						  <label for="paypal">Paypal</label>
						</div>
						<div class="checkbox">
						<input type="checkbox" id="offline" <?php if(in_array('offline',$eventpaymentmod)) echo 'checked'; ?>>
						  <label for="offline">Offline</label>
						</div>	
						<div class="checkbox">
						<input type="checkbox" id="stripe" <?php if(in_array('stripe',$eventpaymentmod)) echo 'checked'; ?>>
						  <label for="stripe">Stripe</label>
						</div>					
      					</div>
              </div>
              <div class="text-center">
                 <button class="btn btn-default">Save</button>
              </div>


          </div>
        </div>
        </form>
          <!--  
		 <div class="col-lg-12 ep-mode">
          <div class="eci_contact_form">
              <?php
               $eventpaymentmod=select_single_data("eci_website_setting","where eci_website_setting_name='event_payment_mode'","eci_website_setting_value");
              ?>
            <form id="eventmethodform">
      				<div class="form-group">
      					<label class="col-sm-5 control-label">Payment Method For Event  :</label>
      					<div class="col-sm-7">
      					<select class="form-control" id="epaymentmode">
                  <option <?php if($eventpaymentmod=='online') echo "selected"; ?> value="online">Online Via Paypal </option>
                  <option <?php if($eventpaymentmod=='offline') echo "selected"; ?> value="offline">Offline Via Invoice</option>
                  <option <?php if($eventpaymentmod=='both') echo "selected"; ?> value="both">Both</option>
                </select>
      					</div>
              </div>
              <div class="text-center">
                 <button class="btn btn-default">Save</button>
              </div>


          </div>
        </div>
        </form>	 -->

        <div class="col-lg-12 pp-mode">
          <div class="eci_contact_form">
                <?php
              $planpaymentmod=select_single_data("eci_website_setting","where eci_website_setting_name='plan_payment_mode'","eci_website_setting_value");
              ?>
          <form id="planmethodform">
              <div class="form-group">
                <label class="col-sm-5 control-label">Payment Method For Plan  :</label>
                <div class="col-sm-7">

                <select class="form-control" id="ppaymentmode">
                  <option <?php if($planpaymentmod=='online') echo "selected"; ?> value="online">Online Via Paypal </option>
                  <option <?php if($planpaymentmod=='offline') echo "selected"; ?> value="offline">Offline Via Invoice</option>
                  <option <?php if($planpaymentmod=='both') echo "selected"; ?> value="both">Both</option>
                </select>

              </div>
              <div class="text-center">
                 <button class="btn btn-default">Save</button>
              </div>
            </form>

          </div>
        </div>
      </div>
		   
		   
		   
		   
  </div>
</div>


