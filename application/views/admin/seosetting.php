<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>SEO Setting</h4>
                    <hr>
                    </div>
                </div>
                <div class="col-lg-12">
               <?php if($msg == '1'):?>
                 
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Changes has been saved successfully.
                  </div>
                <?php endif; ?>
                </div>
           	<?php
			$seo_set=json_decode($seo_setting[0]['eci_website_setting_value']);
			?>	
                <div class="col-lg-12">
                	<div class="eci_contact_form">
                    <form class="form-horizontal" role="form" method="post" action="">
                      
                      <div class="form-group">
                        <label for="eci_title" class="col-sm-2 control-label">SEO Title</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_title"  name="eci_title" placeholder="SEO title"  value="<?php if($seo_set->eci_title != '') { echo $seo_set->eci_title; } ?>">
                        </div>
                       
                      </div>
                      
                       
                      <div class="form-group">
                        <label for="eci_meta_key" class="col-sm-2 control-label">Meta Keywords</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="eci_meta_key"  name="eci_meta_key" placeholder="Meta keywords"  value="<?php if($seo_set->eci_meta_key != '') { echo $seo_set->eci_meta_key; } ?>">
                        </div>
                       
                      </div>
                      
                       
                      <div class="form-group">
                        <label for="eci_meta_desc" class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-6">
						
						 <textarea class="form-control" id="eci_meta_desc"  name="eci_meta_desc"><?php if($seo_set->eci_meta_desc != '') { echo $seo_set->eci_meta_desc; } ?></textarea>
                          
                        </div>
                       
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default" name="save_seo_settings">Save Changes</button>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
                
           </div>
		   
		   
		   
		   
        </div>
    </div>