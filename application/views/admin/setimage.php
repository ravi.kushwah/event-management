<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Set Event Images</h4>
                    <hr>
                    </div>
                </div>
               <div class="col-lg-12">
               <?php if($msg == '1'):?>
                 
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Changes has been saved successfully.
                  </div>
                <?php endif; ?>
                </div>
				
		<form role="form" action="<?php echo base_url()?>eventadmin/saveeventfeaturedimg" method="post">
	  <div class="col-lg-8">
	  <div class="form-group">
	  
		<p class="eci_error" id="eci_event_name_err"> </p>
		<label for="eci_event_name">Choose An Event </label>
		<select class="form-control" id="event_selection" name="event_selection">
		<option value="0"> Choose One </option>
		<?php foreach ($event_list as $solo_event_list) { ?>
		<option value="<?php echo $solo_event_list['eci_event_list_sno'];?>"><?php echo $solo_event_list['eci_event_list_name'];?></option>
		<?php } ?>
		</select>
	  </div>
	  </div>
             
		 <div class="col-lg-12 text-center loading_img">
              	 <div class="eci_setimage_loading"><img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" /></div>
              </div>
              <div class="clearfix"></div>
           
              <div class="alert alert-info setimage_sec">
                	<p><strong>Friendly Note :-</strong> Scroll Down to view images which you have uploaded earlier. You can set only one (1) image at a time as a Featured Image.</p>
              </div>
              
              
           
           <div class="eci_setimage_wrapper">
               <div class="row" id="displaysec">
                  
                   
               </div>
           </div>
           
           <div class="col-lg-12 text-center setimage_sec">
           	  <input type="submit" class="btn btn-default  eci_pull_down_30" name="sub_save_feature" value="Save Changes" />
           </div>
             
			 				  
		</form>

		
	   </div>
        </div>
    </div>