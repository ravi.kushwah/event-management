	<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Payment Section</h4>
                    <hr>
                    </div>
                </div>
           		
                <div class="col-lg-12">
                	<div class="eci_contact_form">
                    <form class="form-horizontal" role="form" method="post" action="">
              
					<div class="form-group">
					<label class="col-sm-5 control-label">Choose A Payment Method  :</label>
					<div class="col-sm-7">
					<div class="radio">
					<input id="paypal" type="radio" name="payment_method" value="paypal" onclick="choose_payment_method()" <?php if($payment_detail[0]['eci_payment_detail_type'] == 1) { echo "checked"; } ?>>
					<label for="paypal">On-Line via Paypal</label>
					<span><strong>Or</strong></span>&nbsp;&nbsp;&nbsp; 
					<input id="invoice" type="radio" name="payment_method" value="invoice" onclick="choose_payment_method()" <?php if($payment_detail[1]['eci_payment_detail_type'] == 1) { echo "checked"; } ?>>
					<label for="invoice">Off-Line via Invoice</label>
					</div>
					</div>
                    </div>


			 <div id="paypal_section" <?php if($payment_detail[1]['eci_payment_detail_type'] == 1) { ?> style="display:none;" <?php } ?>> 
			<div class="alert alert-info">
                	<p><strong>Must do</strong> Please enable the IPN in your paypal account</p>
                	<p><strong>How to get started</strong></p>
					<p>1) Login to payPal. Go to your payPal profile.</p>
					<p>2) Click my selling tools.</p>
					<p>3) Under " Getting paid and managing my risk ", Click on Instant Payment Notification's Update link.</p>
					<p>4) Copy this url <i><?php echo base_url() ?>eventadmin/ipn</i> and Paste in Notification URL box and enable the message delivery checkbox.</p>

              </div>
                      <div class="form-group">
                        <label for="eci_contact_email" class="col-sm-2 control-label">Paypal Email<span class="eci_req_star">*</span></label>
                        <div class="col-sm-6">
                      <input type="text" class="form-control" id="eci_payment_email" name="eci_payment_email" placeholder="Paypal email" value="<?php if(!empty($payment_detail)){ echo $payment_detail[0]['eci_payment_detail_email']; }?>">
                        </div>
                      </div>
					   
					  <div class="form-group">
                        <label for="eci_contact_email" class="col-sm-2 control-label">Stripe Secret Key </label>
                        <div class="col-sm-6">
                      <input type="text" class="form-control" id="eci_payment_secret_key" name="eci_payment_secret_key" placeholder="Stripe secret key" value="<?php if(isset($payment_detail[0]['eci_payment_secret_key'])){ echo $payment_detail[0]['eci_payment_secret_key']; }?>">
                        </div>
                      </div>
					  <div class="form-group">
                        <label for="eci_contact_email" class="col-sm-2 control-label">Stripe Publish Key</label>
                        <div class="col-sm-6">
                      <input type="text" class="form-control" id="eci_payment_publish_key" name="eci_payment_publish_key" placeholder="Stripe publish key" value="<?php if(isset($payment_detail[0]['eci_payment_publish_key'])){ echo $payment_detail[0]['eci_payment_publish_key']; }?>">
                        </div>
                        
                      </div>
                      
                       
                      <div class="form-group">
                        <label for="eci_payment_ccode" class="col-sm-2 control-label">Currency Code<span class="eci_req_star">*</span></label>
                        <div class="col-sm-6">
                     <input type="text" class="form-control" id="eci_payment_ccode" name="eci_payment_ccode" placeholder="USD" value="<?php if(!empty($payment_detail)){ echo $payment_detail[0]['eci_payment_detail_ccode']; }?>">
					<p>Get List Of Paypal Supported Currency Code <a href="https://developer.paypal.com/docs/classic/api/currency_codes/">HERE</a></p>
					<p>Get List Of Stripe SupportedCurrency Code <a href="https://stripe.com/docs/currencies">HERE</a></p>
                        </div>
                        
                      </div>
                      
                </div>

				<div id="invoice_section" <?php if($payment_detail[0]['eci_payment_detail_type'] == 1) { ?> style="display:none;" <?php } ?>>
				<?php
				if(isset($payment_detail[1]))
				{
					$invoicedetail = json_decode($payment_detail[1]['eci_payment_detail_email']);
				}
				
				?>
				<div class="alert alert-info">
                	<p>Please, Check the Fields which you want Customers should fill before getting the Invoice.</p>

              </div>
			  
					<div class="form-group">
                        <label for="eci_invoice_name" class="col-sm-2 control-label">Name :</label>
                      
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_invoice_name" name="show_invoice_name" type="checkbox" value="1" <?php if($invoicedetail->name == 1 ) { echo "checked";}?>>
                            <label for="show_invoice_name">Enable</label>
                          </div>
                        </div>
                      </div>
					  
					 <div class="form-group">
                        <label for="eci_invoice_email" class="col-sm-2 control-label">Email :</label>
                       
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_invoice_email" name="show_invoice_email" type="checkbox" value="1" <?php if($invoicedetail->email == 1 ) { echo "checked";}?>>
                            <label for="show_invoice_email">Enable</label>
                          </div>
                        </div>
                      </div>
					  
					 <div class="form-group">
                        <label for="eci_invoice_cntctno" class="col-sm-2 control-label">Contact Number :</label>
                        
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_invoice_cntctno" name="show_invoice_cntctno" type="checkbox" value="1" <?php if($invoicedetail->cntctno == 1 ) { echo "checked";}?>>
                            <label for="show_invoice_cntctno">Enable</label>
                          </div>
                        </div>
                      </div>
					  
					 <div class="form-group">
                        <label for="eci_invoice_add" class="col-sm-2 control-label">Address: </label>
                        
                        <div class="col-sm-2">
                        	<div class="checkbox">
                            <input id="show_invoice_add" name="show_invoice_add" type="checkbox" value="1" <?php if($invoicedetail->add == 1 ) { echo "checked";}?>>
                            <label for="show_invoice_add">Enable</label>
                          </div>
                        </div>
                      </div>
					  
					 <div class="form-group hide">
                        <label for="eci_invoice_add" class="col-sm-2 control-label">Currency Code: <span class="eci_req_star">*</span></label>
                        
						<div class="col-sm-6 ">
                     <input type="text" class="form-control" id="show_invoice_curency_code" name="show_invoice_curency_code" placeholder="INR" value="<?php if(!empty($payment_detail)){ echo $payment_detail[1]['eci_payment_detail_ccode']; }?>">
					<p class="help-block">Eg: INR, USD, EUR, and so on.</p>
                        </div>
                        
                      </div>
					  
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<input id="send_invoice_link" name="send_invoice_link" type="checkbox" value="1" <?php if($invoicedetail->link == 1 ) { echo "checked";}?>>
							<label for="send_invoice_link">Send Email to Customer<i class="fa fa-link fa-lg"></i></label>
						  </div>
						 
						</div>
					 </div>  
				
				</div>
				
                      <div class="form-group">
                        <label for="eci_payment_msg" class="col-sm-2 control-label">Thank You Message<span class="eci_req_star">*</span></label>
                        <div class="col-sm-6">
                       <textarea class="form-control" id="eci_payment_msg" name="eci_payment_msg" ><?php if(!empty($payment_detail)){ echo $payment_detail[0]['eci_payment_detail_msg']; }?></textarea>
                        </div>
                        
                      </div>
                      
         <div class="alert alert-warning">
                	<p>Use Below [ Variables ] in the template, and then Application will replace it with the Actual Detail</p>
					<p><strong>[cus_name]</strong> -- Customer's Name</p>
					<p><strong>[event_name]</strong> -- Event's Name</p>
					<p><strong>[no_tckt]</strong> -- Number of Tickets</p>
					<p><strong>[uniq_tckt]</strong> -- Unique Ticket Number</p>
					<p><strong>[break]</strong> -- Jump to Next Line</p>

              </div>               
                      <div class="form-group">
                        <label for="eci_payment_detail_em_temp" class="col-sm-2 control-label">Email Template After The Payment<span class="eci_req_star">*</span></label>
                        <div class="col-sm-6">
                       <textarea class="form-control" id="eci_payment_detail_em_temp" name="eci_payment_detail_em_temp" rows="8"><?php if(!empty($payment_detail)){ echo $payment_detail[0]['eci_payment_detail_em_temp']; }?></textarea>
                        </div>
                        
                      </div>
                      
                      
                      
                      
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default" onclick="return check_payment_form()" name="sub_payment_save">Save Changes</button>
                        </div>
                      </div>
                    </form>
                    </div>
                </div>
                
           </div>
		   
		   
		   
		   
        </div>
    </div>