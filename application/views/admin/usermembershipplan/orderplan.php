<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Order Plan</h4>
                    <hr>
                  </div>
                </div>
           	</div>

    <div class="row">
     
        <h4>Your order:</h4>
        <form id="editprofile" role="form" action="<?php echo base_url() ?>profile/orderplan/<?php echo $plandetail[0]['eci_plan_id'];?>" method="post">
                <div class="col-md-9 col-lg-9 "> 
               
                  <div class="form-group">
                    <p class="eci_error" id="eci_plan_name_err"> </p>
                    <label for="eci_plan_name">Plan Name <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_plan_name" name="eci_plan_name" placeholder="Plan Name" value="<?php echo $plandetail[0]['eci_plan_name'] ?>" readonly>
                    <input type="hidden" name="planid" id="planid" value="<?php echo $plandetail[0]['eci_plan_id'];?>">
                  </div>
                  
                  <div class="form-group">

                  <?php 
                  $planprice=$plandetail[0]['eci_plan_price'];
                  if($plandetail[0]['eci_plan_type']==1){
                    $planprice=0; 
                  }
                   ?>
                    <p class="eci_error" id="eci_plan_price_err"> </p>
                    <label for="eci_plan_price">Plan Price <span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_plan_price" name="eci_plan_price" placeholder="Plan Price" value="<?php echo $planprice;?>" readonly>
                  </div>
                  
                  <div class="form-group">
                    <p class="eci_error" id="eci_plan_duration_err"> </p>
                    <label for="eci_plan_duration">Plan Duration<span class="eci_req_star">*</span></label>
                    <input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Confirm Password" value="<?php echo $plandetail[0]['eci_plan_duration'] ?>" readonly >
                  </div>
                  
              <?php
              $paymentmode=select_single_data("eci_website_setting","where eci_website_setting_name='plan_payment_mode'","eci_website_setting_value"); 
               ?>			   <?php if($paymentmode=='offline' || $paymentmode=='both'){ ?>
                <div class="col-md-6">
                <?php
               $userid=$this->session->userdata("eci_super_id");
                 $usercontact=select_single_data("eci_admin_additional_info","where eci_admin_sno=$userid","eci_admin_contact");
                       if(empty($usercontact)){ ?>
                      <input type="button" class="btn btn-default btn-block cursor-no" name="offline"  id="offline"  value="Pay Offline">
                      <span class="label label-info">Please Update your profile before purchasing plan</span>
                 <?php } else { ?>
              <input type="submit" class="btn btn-default btn-block" name="offline" id="offline"  value="Pay Offline" >
                  <?php  }  ?>     
               
                  <?php if(isset($msg)){ ?>
                    <div class="<?php echo $class ?>">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <strong> <?php echo $msg ?>.</strong>
                    </div>
                   <?php } ?>
                   <?php echo validation_errors(); ?>
               

                </form>
                </div>			   <?php } ?>					<?php if($paymentmode=='online' || $paymentmode=='both'){ ?>
              <div class="col-md-6  ">
               <!-- <form name='_xclick' action='https://www.paypal.com/cgi-bin/webscr' method='post'> -->
          <div class="paypalform">
          <?php
          $loader=base_url().'assets/back/images/loader/ajax_loader_blue_350.gif';
           ?><a href="javascript:" class="btn btn-default btn-block" id="online"><i class="fa fa-spinner fa-spin fa-1x fa-fw hide " id="loader"></i>Pay Online</a>
         <form name="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypalform">

              
              </form>
            </div>        
            </div>			<?php } ?>
              </div>
                
            
    </div>


        
            
        </div>
    </div>

