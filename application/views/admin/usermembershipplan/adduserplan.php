
   <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
              <div class="col-lg-12">
                  <div class="eci_heading">
                    <h4>Add user Plan </h4>
                    <hr>
                    </div>
                </div>
       
    <form role="form" action="<?php echo base_url()?>event/adduserplan" method="post">
       
              
        
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_email_err"></p>
                    <label for="eci_service_name">User Email <span class="eci_req_star">*</span></label>
                    

                    <select class="form-control" name="eci_user_email" id="eci_user_email">
                    <option value="">Select User Email</option>
                      <?php foreach ($userlist as $user):?>
                     <option value="<?php echo $user['eci_admin_sno']?>" ><?php echo $user['eci_admin_email'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  </div>

                  

                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_plan_name_err"> </p>
                    <label for="eci_service_name">Plan Name <span class="eci_req_star">*</span></label>
                    <select class="form-control" name="eci_user_plan_name" id="eci_user_plan_name">
                    
                      <option value="">Select plan</option>
                      <?php foreach ($plan as $plan):?>
                     <option value="<?php echo $plan['eci_plan_id']?>" ><?php echo $plan['eci_plan_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  </div>

                  
             <div class="col-lg-12 text-center">
                  <input type="submit" class="btn btn-default" onclick="return check_add_user_membership_plan()" name="sub_service_save" value="Save" />
          
                  <button type="reset" class="btn btn-group">Reset</button>
                  </div>
                </form>
           </div>
        </div>
    </div>
