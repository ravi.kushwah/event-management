<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Purchase plan</h4>
                    <hr>
                  </div>
                </div>
           	</div>

     <?php $usereid=$this->session->userdata('eci_super_id');
        $userplan_id=select_single_data("eci_user_membership_plan","where eci_user_id=$usereid and eci_user_plan_status='active'","eci_membership_plan_id");
     ?>  
              
    <div class="row">
    <div class="col-md-6">
         <div class="panel panel-primary">
            <div class="panel-heading text-center">Timebased Plan</div>
         </div>
          <?php if(!empty($timeplans)): ?>
         <?php foreach($timeplans as $plan): ?>
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $plan['eci_plan_name']; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                       
                        
                    </div>
                    <table class="table">
                    <tr>
                            <td>
                                Price
                            </td>
                            <?php if($plan['eci_plan_type']==1){?>
                            <td>
                              <?php echo $plan['eci_plan_price'];?>% Commision
                              </td>
                            <?php } else { ?>
                            <td>
                              <?php echo $plan['eci_plan_price']; ?><span><b>:<?php echo $currency['eci_payment_detail_ccode']; ?></b></span>
                            </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>
                                Duration
                            </td>
                            <td>
                              <?php echo $plan['eci_plan_duration']; ?>:Days  
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Event Post
                            </td>
                            <td>
                               Unlimited
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="panel-footer">
                <?php if($plan['eci_plan_id']==$userplan_id){?>
                      <a href="<?php echo base_url();?>profile/ordercommisionplan/<?php echo $plan['eci_plan_id'];?>" class="btn btn-default" role="button">Purchased</a>
                <?php } else { ?>
                    <a href="<?php echo base_url();?>profile/orderplan/<?php echo $plan['eci_plan_id'];?>" class="btn btn-success" onclick="" role="button">Order</a>
                    <?php } ?>
                </div>
            </div>
        </div>
      <?php endforeach; ?>
    <?php endif;?>
       </div> 
       <div class="col-md-6">
         <div class="panel panel-primary">
            <div class="panel-heading text-center">Commision Plan</div>
         </div>
         <?php if(!empty($commisionplans)): ?>
          <?php foreach($commisionplans as $plan): ?>
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $plan['eci_plan_name']; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                       
                        
                    </div>
                    <table class="table">
                    <tr>
                            <td>
                                Price
                            </td>
                            <?php if($plan['eci_plan_type']==1){?>
                            <td>
                              <?php echo $plan['eci_plan_price'];?>% Commision
                              </td>
                            <?php } else { ?>
                            <td>
                              <?php echo $plan['eci_plan_price']; ?><span><b>:<?php echo $currency['eci_payment_detail_ccode']; ?></b></span>
                            </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>
                                Duration
                            </td>
                            <td>
                              <?php echo $plan['eci_plan_duration']; ?>:Days  
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Event Post
                            </td>
                            <td>
                               Unlimited
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="panel-footer">
                <?php if($plan['eci_plan_id']==$userplan_id){?>
                      <a href="<?php echo base_url();?>profile/ordercommisionplan/<?php echo $plan['eci_plan_id'];?>" class="btn btn-default" role="button" onclick="return confirm('Are you sure All your previous plan has been removed?');" >Purchased</a>
                <?php } else { ?>
                    <a href="<?php echo base_url();?>profile/ordercommisionplan/<?php echo $plan['eci_plan_id'];?>" class="btn btn-success" role="button" onclick="return confirm('Are you sure All your previous plan has been removed?');">Order</a>
                    <?php } ?>
                </div>
            </div>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
       </div> 
       
    </div>


        
            
        </div>
    </div>
    
   