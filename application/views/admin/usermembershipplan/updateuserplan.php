
   <div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
              <div class="col-lg-12">
                  <div class="eci_heading">
                    <h4>Update user Plan </h4>
                    <hr>
                    </div>
                </div>
        <?php if(!empty($eci_user_plan_id)) { ?>

    <form role="form" action="<?php echo base_url()?>event/updateuserplan/<?php echo $eci_user_plan_id; ?>" method="post">
        <?php } else { ?>
              <form role="form" action="<?php echo base_url()?>eventadmin/saveservices" method="post">
        <?php } ?>
                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_name_err"> </p>
                    <label for="eci_service_name">User Name <span class="eci_req_star">*</span></label>
                    <?php
                      $username="";
                      if($eci_user_id){
                       $username=select_single_data('eci_admin',"where eci_admin_sno=$eci_user_id",'eci_admin_name');
                      }
                     ?>

                    <input type="text" class="form-control" id="eci_user_name" name="eci_user_name" placeholder="Enter User Name" readonly="true" value="<?php echo $username; ?>">
                  </div>
                  </div>

                  

                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_plan_name_err"> </p>
                    <label for="eci_service_name">Plan Name  <span class="eci_req_star">*</span></label>
                    <select class="form-control" name="eci_user_plan_name" id="eci_user_plan_name">
                    
                      <option value="">Select plan</option>
                      <?php foreach ($plan as $plan):?>
                      <option value="<?php echo $plan['eci_plan_id']?>" <?php if($plan['eci_plan_id']==$eci_membership_plan_id) echo "selected" ;?> ><?php echo $plan['eci_plan_name'];?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  </div>

                  <div class="col-lg-8">
                  <div class="form-group">
                    <p class="eci_error" id="eci_user_plan_status_err"> </p>
                    <label for="eci_service_name">Plan Status <span class="eci_req_star">*</span></label>
                    <select class="form-control" name="eci_user_plan_status" id="eci_user_plan_status">
                      <option value="">Select Status</option>
                      <option <?php if($eci_user_plan_status=='Active') echo 'selected'?> value="Active">Active</option>
                      <option <?php if($eci_user_plan_status=='Inactive') echo 'selected'?> value="Inactive">Inactive</option>
                    </select>
                  </div>
                  </div>

                  
             <div class="col-lg-12 text-center">
                  <input type="submit" class="btn btn-default" onclick="return check_update_user_membership_plan()" name="sub_service_save" value="Save" />
          
                  <button type="reset" class="btn btn-group">Reset</button>
                  </div>
                </form>
           </div>
        </div>
    </div>
   <script type="text/javascript" src="<?php echo base_url();?>assets/back/javascript/my_custom.js"></script>

