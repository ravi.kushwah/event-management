<div class="eci_page_content_wrapper">
        <div class="eci_page_content">
           <div class="row">
           		<div class="col-lg-12">
                	<div class="eci_heading">
                    <h4>Booking Detail</h4>
                    <hr>
                    </div>
                </div>
			<div class="col-lg-12">
               <?php if($msg == '1'):?>
                 
                  <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Booking saved successfully.
                  </div>
                <?php endif; ?>
                </div>
	  <div class="col-lg-8">
	  
	  <div class="form-group">
	  
		<p class="eci_error" id="eci_event_name_err"> </p>
		<label for="eci_event_name">Choose An Event </label>
		<select class="form-control" id="booking_event_selection">
		<option value="0"> Choose One </option>
		<?php foreach ($event_list as $solo_event_list) { ?>
		<option value="<?php echo $solo_event_list['eci_event_list_sno'];?>"><?php echo $solo_event_list['eci_event_list_name'];?></option>
		<?php } ?>
		</select>
	  </div>
	  </div>
             
		 <div class="col-lg-12 text-center loading_img">
              	 <div class="eci_setimage_loading"><img src="<?php echo base_url();?>assets/back/images/setimage_loading.gif" /></div>
          </div>
              <div class="clearfix"></div>
    
           <div class="eci_setimage_wrapper">
               <div id="displaysec">
               
               </div>
           </div>
        </div>
        </div>
    </div>
