(function($) {
    'use strict';
function check_add_membership_plan(){

    $('#eci_plan_name_err').html('');
    $('#eci_plan_type_err').html('');
    $('#eci_plan_price_err').html('');
    $('#eci_plan_duration_err').html('');
    var planname=$('#eci_plan_name').val();
    var plantype=$('#eci_plan_type').val();
    var planprice=$('#eci_plan_price').val();

    if($('#eci_plan_duration').length == 1)
        var planduration = $('#eci_plan_duration').val();
    else
        var planduration = 1;

    if($.trim(planname) == "") {
        $('#eci_plan_name_err').html('Please enter plan name');
        return false;
    }
    if($.trim(plantype) == "") {
        $('#eci_plan_type_err').html('Please select plan type');
        return false;
    }
    if($.trim(planprice) === "") {
        $('#eci_plan_price_err').html('Please enter plan price');
        return false;
    }
    if(!/^[0-9]+$/.test(planprice)){
        $('#eci_plan_price_err').html('Please enter only numeric value');
        return false;
    }
    if($.trim(planduration) === "") {
        $('#eci_plan_duration_err').html('Please enter plan duration');
        return false;
    }
    if(!/^[0-9]+$/.test(planduration)){
        $('#eci_plan_duration_err').html('Please enter only numeric value');
        return false;
    }
}
    $(document).ready(function(){
        $('#eci_plan_type').change(function(){
            $('.plan_duration').html('');
            var plantype=$(this).val();
            if(plantype!=1 && plantype!=""){
                $('.plan_duration').html('<div class="col-lg-8"><div class="form-group"><p class="eci_error" id="eci_plan_duration_err"> </p><label for="eci_service_name">Plan Duration <span class="eci_req_star">*</span></label><input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Duration in days" value=""></div></div>');
            }
        })
    })

    $(document).ready(function(){
        $('#eci_plan_type').change(function(){
            $('.plan_duration').html('');
            var plantype=$(this).val();
            if(plantype!=1 && plantype!=""){
                $('.plan_duration').html('<div class="col-lg-8"><div class="form-group"><p class="eci_error" id="eci_plan_duration_err"> </p><label for="eci_service_name">Plan Duration <span class="eci_req_star">*</span></label><input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Duration In Days" value=""></div></div>');
            }
        })
    })
    $('document').ready(function(){
        $('input:radio[name="tcat"]').change(function(){
            $('.modal-content').show();
            jQuery.noConflict();
            var selection = $('input[name="tcat"]:checked').val();
            if(selection == 1){
                $('#withoutTicketCat').addClass('hide');
                $('#withoutTicketCat').html('');
                $('#ticketCat_popup').modal('show');
            }else{
                $('#withoutTicketCat').removeClass('hide');
                var data ='<div class="form-group col-lg-6 eci_max_user"><p class="eci_error" id="eci_no_member_err" ></p><label for="eci_no_service">Maximum Members<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12" id="eci_max_users" name="eci_max_users" placeholder="" value="" ><p class="help-block">Booking will be ON by default. </p></div><div class="form-group col-lg-6 eci_max_user"><p class="eci_error" id="eci_no_service_err" ></p><label for="eci_booking_amount">Amount to be paid by 1 member (USD)<span class="eci_req_star">*</span></label><input type="text" class="form-control col-md-12" id="eci_booking_amount" name="eci_booking_amount" placeholder="" value="" ><p class="help-block">Do not type currency symbol. </p></div><div class="col-lg-12"><div class="form-group"><div class="alert alert-warning"><p>To make an event free just type <i>FREE</i> in <strong>Amount to be paid by 1 member</strong> text box.</p></div></div></div>';

                $('#withoutTicketCat').html(data);
                $('.addTicketCat').html('');
                $('#nofcat').val('');
            }
        });
    });

    $('#proceed_to_category').on('click',function(){
		var noofcat = $('#ems_ticket_cat').val();
		var base_url = $('#baseUrl').val();
		$.ajax({
			url:base_url+'eventadmin/event_category',
			type:'post',
			datatype:'json',
			data: 'catNo='+noofcat,
		   success:function(resp){
			   resp = $.parseJSON(resp);
			   $('.addTicketCat').html(resp);
			   $('#nofcat').val(noofcat);
		   }
		});       
        $('#ticketCat_popup').modal('hide');
		
		
	}); 
    $('.closeBTN').on('click',function(){	
        $('.modal-backdrop').remove();
        $('.modal-content').hide();
        $('#tcatno').prop('checked', true);
    }); 
    $( document ).ready( function () {
        $('.eci_new_email').hide();
    });
    function check_update_user_membership_plan(){
        return true;

        $('#eci_user_name_err').html('');
        $('#eci_user_plan_name_err').html('');
        var username=$('#eci_user_name').val();
        var userplan=$('#eci_user_plan_name').val();


        if($.trim(username) == "") {
            $('#eci_user_name_err').html('Please enter user name');
            return false;
        }
        if($.trim(userplan) === "") {
            $('#eci_user_plan_name_err').html('Please select plan name');

            return false;
        }

    }
    Dropzone.options.myAwesomeDropzone = {
        paramName: "userfile",
        maxFilesize: 1000,
        addRemoveLinks:false,
        dictCancelUpload:'Cancel Upload',
        accept: function(file, done) {
            if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/jpg" || file.type == "image/gif") {
                done();
            }
            else {
                done("Please Upload Image only.");
            }
        }
    };
    $(document).ready(function(){

        $('#emailtemplateupdate').on('click',function(){
            emailtext=$('#registrationemail_text').val();
            emaillinktext=$('#registrationemail_linktext').val();
            $.ajax({
                url:"<?php echo base_url().'eventadmin/updatemailtemplate';?>",
                method:'post',
                data:{'email_text':emailtext,'email_link_text':emaillinktext},
                success:function(src){
                    $('#eupdatesuccess').removeClass('hide').delay(5000).queue(function(){
                        $(this).addClass('hide').clearQueue();
                    });
                    $('#eupdatesuccess #msg').html(src);
                    //$('#eupdatesuccess').addClass('hide').delay(5000);

                }

            })
        })

        $('#fmailtemplateupdate').on('click',function(){
            femailtext=$('#forgotpwdemail_text').val();
            femaillinktext=$('#forgotpwdemail_linktext').val();
            $.ajax({
                url:"<?php echo base_url().'eventadmin/updatemailtemplate';?>",
                method:'post',
                data:{'femail_text':femailtext,'femail_link_text':femaillinktext},
                success:function(src){
                    $('#fupdatesuccess').removeClass('hide').delay(5000).queue(function(){
                        $(this).addClass('hide').clearQueue();
                    });
                    $('#fupdatesuccess #msg').html(src);
                    //$('#eupdatesuccess').addClass('hide').delay(5000);

                }

            })
        })


        $('#pmailtemplateupdate').on('click',function(){
            pemailtext=$('#planpwdemail_text').val();
            $.ajax({
                url:"<?php echo base_url().'eventadmin/updatemailtemplate';?>",
                method:'post',
                data:{'pemail_text':pemailtext},
                success:function(src){
                    $('#pupdatesuccess').removeClass('hide').delay(5000).queue(function(){
                        $(this).addClass('hide').clearQueue();
                    });
                    $('#pupdatesuccess #msg').html(src);
                    //$('#eupdatesuccess').addClass('hide').delay(5000);

                }

            })
        })

        $('#ausermailtemplateupdate').on('click',function(){
            auseremailtext=$('#adduseremail_text').val();
            $.ajax({
                url:"<?php echo base_url().'eventadmin/updatemailtemplate';?>",
                method:'post',
                data:{'auseremail_text':auseremailtext},
                success:function(src){
                    $('#adduserupdatesuccess').removeClass('hide').delay(5000).queue(function(){
                        $(this).addClass('hide').clearQueue();
                    });
                    $('#adduserupdatesuccess #msg').html(src);
                    //$('#eupdatesuccess').addClass('hide').delay(5000);

                }

            })
        })
    })
    function generate_pass(){
        var password = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 8; i++ )
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        $('#eci_user_password').val(password)
    }
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }


})(jQuery);
