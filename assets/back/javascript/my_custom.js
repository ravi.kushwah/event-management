// add membership plan section
( function ( $ ) {
    'use strict';

function check_add_membership_plan(){
  
  $('#eci_plan_name_err').html('');
  $('#eci_plan_type_err').html('');
  $('#eci_plan_price_err').html('');
  $('#eci_plan_duration_err').html('');
  var planname=$('#eci_plan_name').val();
  var plantype=$('#eci_plan_type').val();
  var planprice=$('#eci_plan_price').val();

  if($('#eci_plan_duration').length == 1)
    var planduration = $('#eci_plan_duration').val();
  else
    var planduration = 1;

  if($.trim(planname) == "") {
  $('#eci_plan_name_err').html('Please enter plan name');
      return false;
  }
   if($.trim(plantype) == "") {
  $('#eci_plan_type_err').html('Please select plan type');
      return false;
  }
  if($.trim(planprice) === "") {
  $('#eci_plan_price_err').html('Plfease enter plan price');
      return false;
  }
  if(!/^[0-9]+$/.test(planprice)){
      $('#eci_plan_price_err').html('Please enter only numeric value');
      return false;
    }
    if($.trim(planduration) === "") {
      $('#eci_plan_duration_err').html('Please enter plan duration');
      return false;
  }
  if(!/^[0-9]+$/.test(planduration)){
      $('#eci_plan_duration_err').html('please neter only numeric value');
      return false;
    }
}

  $(document).ready(function(){
  $('#eci_plan_type').change(function(){
   $('.plan_duration').html('');
   var plantype=$(this).val();
   if(plantype!=1 && plantype!=""){
    $('.plan_duration').html('<div class="col-lg-8"><div class="form-group"><p class="eci_error" id="eci_plan_duration_err"> </p><label for="eci_service_name">Plan Duration <span class="eci_req_star">*</span></label><input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Duration in days" value=""></div></div>');
   }
  })
  })

  $(document).ready(function(){
  $('#eci_plan_type').change(function(){
   $('.plan_duration').html('');
   var plantype=$(this).val();
   if(plantype!=1 && plantype!=""){
    $('.plan_duration').html('<div class="col-lg-8"><div class="form-group"><p class="eci_error" id="eci_plan_duration_err"> </p><label for="eci_service_name">Plan Duration <span class="eci_req_star">*</span></label><input type="text" class="form-control" id="eci_plan_duration" name="eci_plan_duration" placeholder="Duration In Days" value=""></div></div>');
   }
  })
  })
 // update membership plan
 function check_update_membership_plan(){
  
  $('#eci_plan_name_err').html('');
  $('#eci_plan_price_err').html('');
  $('#eci_plan_duration_err').html('');
  var planname=$('#eci_plan_name').val();
  var planprice=$('#eci_plan_price').val();
  var planduration=$('#eci_plan_duration').val();
  if($.trim(planname) == "") {
  $('#eci_plan_name_err').html('Please enter plan name');
      return false;
  }
  if($.trim(planprice) === "") {
  $('#eci_plan_price_err').html('Please enter plan price');
      return false;
  }
  if(!/^[0-9]+$/.test(planprice)){
      $('#eci_plan_price_err').html('Please enter only numeric value');
      return false;
    }
    if($.trim(planduration) === "") {
  $('#eci_plan_duration_err').html('Please enter plan duration');
      return false;
  }
  if(!/^[0-9]+$/.test(planduration)){
      $('#eci_plan_duration_err').html('Please enter only numeric value');
      return false;
    }
}

// add user plan
	function check_add_user_membership_plan(){
	  
	  $('#eci_user_email_err').html('');
	  $('#eci_user_plan_name_err').html('');
	  var useremail=$('#eci_user_email').val();
	  var userplan=$('#eci_user_plan_name').val();
	  
	  
	  if($.trim(useremail) == "") {
	  $('#eci_user_email_err').html('Please select user email');
		  return false;
	  }
	  if($.trim(userplan) === "") {
	  $('#eci_user_plan_name_err').html('Please select user plan');
	 
		  return false;
	  }
	  
	}
	//update user plan
	function check_update_user_membership_plan(){
  
  $('#eci_user_name_err').html('');
  $('#eci_user_plan_name_err').html('');
  $('#eci_user_plan_status_err').html('');
  var username=$('#eci_user_name').val();
  var userplan=$('#eci_user_plan_name').val();
  var userplanstatus=$('#eci_user_plan_status').val();
  
  if($.trim(username) == "") {
  $('#eci_user_name_err').html('Please enter user name');
      return false;
  }
  if($.trim(userplan) === "") {
  $('#eci_user_plan_name_err').html('please select plan name');
 
      return false;
  }
  if($.trim(userplanstatus) === "") {
  $('#eci_user_plan_status_err').html('Please select plan status');
 
      return false;
  }
  
}
	// order plan
	 $(document).ready(function(){
        $('#editprofile #online').click(function(event){
        $(this).find('#loader').removeClass('hide');
      $(this).css('pointer-events','none');
       
        var basepath = $('#basepath').val();
        var planid   =$('#planid').val(); 
        $.ajax({
         url: basepath + 'Ajax/planpaypalform' ,
         data: {planid:planid },
         type: 'post',
         success: function(output) {
        $('#paypalform').html(output);
         $('#paypalform').submit();
       }
          });
        })

    })
  
  //add event
  function check_add_event(){
	  
       
       $('#eci_event_name_err').html('');
       $('#eci_event_date_err').html('');
       $('#eci_event_time_h_err').html('');
       $('#eci_no_service_err').html('');
       $('#eci_no_member_err').html('');
       $('#eci_event_desc_err').html('');
       $('#eci_yt_url_err').html('');
       $('#eci_event_namecat_err').html('');
       $('#eci_event_catmem_err').html('');
       $('#eci_event_catamt_err').html('');
	   
      
      var eventname=$('#eci_event_name').val();
      var eventdate=$('#eci_event_date').val();
      var eventtime=$('#eci_event_time_h').val();
      var eventservice=$('#eci_no_service').val();
      var eventmember=$('#eci_max_users').val();
      var eventdescription=$('#eci_event_desc').val();
      var eventdescription=editor.getData();
      var yt_url=$('#eci_yt_url').val();
	  var counter = 0;
	  
	 
        if($.trim(eventname)==""){
      $('#eci_event_name_err').html('Event name is required');
        $('#eci_event_name').focus();
         return false;
       }
       if($.trim(eventdate)==""){
      $('#eci_event_date_err').html('Event date is required');
        $('#eci_event_date').focus();
         return false;
       }
       if($.trim(eventtime)==""){
      $('#eci_event_time_h_err').html('Event time is required');
        $('#eci_event_time_h').focus();
         return false;
       }
       
       
       $('.catName').each(function(){
			var catname = $(this).val();
			if(catname == ''){
				$('#eci_event_namecat_err').html('Event Category Name is required');
				counter++;
			}
		});
		if( counter == 0 ){}
		else{
			return false;
		}
		
		$('.maxMember').each(function(){
			var catname = $(this).val();
			if(catname == ''){
				$('#eci_event_catmem_err').html('Event Members is required');
				counter++;
			}
		});
		if( counter == 0 ){}
		else{
			return false;
		}
		
		$('.bookAmt').each(function(){
			var catname = $(this).val();
			if(catname == ''){
				$('#eci_event_catamt_err').html('Event Amount is required');
				counter++;
			}
		});
		if( counter == 0 ){}
		else{
			return false;
		}
       
       
      
    if($.trim(eventmember)==""){
      $('#eci_no_member_err').html('Event member is required');
        $('#eci_max_users').focus();
         return false;
       }
       if(!/^[0-9]+$/.test(eventmember)){
      $('#eci_no_member_err').html('Please enter only numeric value');
      $('#eci_max_users').focus();
      return false;
    }
    if($.trim(eventdescription)==""){
      $('#eci_event_desc_err').html('Event description is required');
        $('#eci_event_desc').focus();
         return false;
    }
	if (yt_url != '') {        
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = yt_url.match(regExp);
        if (match && match[2].length == 11) {
           
        } else {
            
            $('#eci_yt_url_err').html('Please enter valid youtube url ');
            return false;
            
        }
    }
	
    //return true; 
}
	 
	
	  // validate youtube
	  function validateYouTubeUrl(){    
    var url = $('#youTubeUrl').val();
    if (url != undefined || url != '') {        
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line            
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
        } else {
            alert('not valid');
            // Do anything for not being valid
        }
    }
}

     //plan payment method
	 $(document).ready(function(){
	$('#planmethodform').submit(function(e){
		e.preventDefault();
		var basepath = $('#basepath').val();
		var method=$('#ppaymentmode').val();
		var purpose ='plan'; 
			$.ajax({
			 url: basepath +'Ajax/paymentmode' ,
			 data: {method:method,purpose:purpose },
			 type: 'post',
			 success: function(output) {
			  alert(output);
		   }
			  });
	  })


		 //event payment method
	  $('#eventmethodform').submit(function(e){
		e.preventDefault();
		var method='';
		var paypal =($('#paypal').is(':checked'))?'paypal': '';
		var offline =($('#offline').is(':checked'))?'offline': '';
		var stripe =($('#stripe').is(':checked'))?'stripe': '';
		if(paypal !=''){method+=','+paypal;	}
		if(offline !=''){method+=','+offline;}
		if(stripe !=''){method+=','+stripe;}
		var basepath = $('#basepath').val();
		//var method=$('#epaymentmode').val();
		var purpose ='event'; 
			$.ajax({
			 url: basepath +'Ajax/paymentmode' ,
			 data: {method:method,purpose:purpose },
			 type: 'post',
			 success: function(output) {
			  alert(output);
		   }
			  });
	  })
	 })
	  
	  // add coupon
	  function check_add_coupon_form(){
    $('#eci_coupon_name_err').html('');
    $('#eci_coupon_code_err').html('');
    $('#eci_coupon_desc_err').html('');
    $('#eci_coupon_amount_err').html('');
    $('#eci_coupon_expire_date').html('');
    $('#eci_coupon_usages_err').html('');
    var coupon_name=$('#eci_coupon_name').val();
    var coupon_code=$('#eci_coupon_code').val();
    var coupon_desc=$('#eci_coupon_desc').val();
    var coupon_amount=$('#eci_coupon_amount').val();
    var coupon_expire_date=$('#eci_coupon_expir_date').val();
    var coupon_usages=$('#eci_coupon_usages_limit').val();
	var coupon_event = $('#eci_coupon_event').val();
 
  if($.trim(coupon_name)==""){
        $('#eci_coupon_name_err').html('Coupon name is required');
    $('#eci_coupon_name').focus();
    return false;
  }
  if($.trim(coupon_code)==""){
    $('#eci_coupon_code_err').html('Coupon code is required');
    $('#eci_coupon_code').focus();
    return false;
  }
  if($.trim(coupon_desc)==""){
    $('#eci_coupon_desc_err').html('Coupon description is required');
    $('#eci_coupon_desc').focus();
    return false;
  }
  if($.trim(coupon_amount)==""){
        $('#eci_coupon_amount_err').html('Coupon amount is required');
        $('#eci_coupon_amount').focus();
        return false;
  }
  if(isNaN(coupon_amount)){
        $('#eci_coupon_amount_err').html('Coupon should be numeric');
        $('#eci_coupon_amount').focus();
        return false;
  }
  if($.trim(coupon_expire_date)==""){
        $('#eci_coupon_expire_date').html('Expire date is required');
        $('#eci_coupon_expir_date').focus();
        return false;
        }
    if($.trim(coupon_usages)==""){
        $('#eci_coupon_usages_err').html('Coupon limit is required');
        $('#eci_coupon_usages_limit').focus();
         return false;
  }
  if(isNaN(coupon_usages)){
      $('#eci_coupon_usages_limit').val('unlimited');
    }
  if(coupon_event ==''){
	  $('#eci_coupon_event_err').html('Event is required');
	  $('#eci_coupon_event').focus();
	  return false;
	  
  }
  
      }
	  
// add user

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function check_add_user(){
	   $('#eci_user_name_err').html('');
		$('#eci_user_email_err').html('');
	    $('#eci_user_password_err').html('');
        var name=$('#eci_user_name').val();
		var email=$('#eci_user_email').val();
		var password=$('#eci_user_password').val();
		
     
      if($.trim(name)==""){
      	$('#eci_user_name_err').html('Username is required');
		return false;
	}
	if($.trim(email)==""){
      	$('#eci_user_email_err').html('Email required');
		return false;
	}
	if(!isValidEmailAddress($.trim(email))){
		$('#eci_user_email_err').html('Please enter valid email');
					return false;
	}
	if($.trim(password)==""){
      	$('#eci_user_password_err').html('Password required');
		return false;
	}
	if($.trim(password).length<6){
				$('#eci_user_password_err').html('Your password should be six character long');
				return false;
			  }
	  
}	  

 // change password
  function check_confirm_password(){
      
     $('#eci_old_pass_err').html('');
    $('#eci_new_pass_err').html('');
      $('#eci_c_pass_err').html('');
    
        
    var oldpass=$('#eci_old_password').val();
    var npassword=$('#eci_new_password').val();
    var cpassword=$('#eci_cnew_password').val();
     
      if($.trim(oldpass)==""){
        $('#eci_old_pass_err').html('Old Passowrd  required');
    return false;
  }
  if($.trim(npassword)==""){
        $('#eci_new_pass_err').html('New password required');
    return false;
  }
  if($.trim(npassword).length<6){
        $('#eci_new_pass_err').html('Your password should be 6 character long');
        return false;
        }
    if($.trim(cpassword)==""){
        $('#eci_c_pass_err').html('<b>Confirm password required');
    return false;
  }
  if($.trim(npassword) != $.trim(cpassword) ){
    $('#eci_new_pass_err').html('Password and confirm password must be same');
    return false;
     }
     
}


// edit profle
$(document).ready(function(){
 $('#changepicture').submit(function(e){
       e.preventDefault();
	   var basepath = $('#basepath').val();
       userimage=$('#userfile').val();
       var esrc=$('#userimage').attr('src');
       if(userimage==''){
        alert('userimage is required');
        return false;
       }
       $(this).find('#loader').removeClass('hide');
       $(this).css('pointer-events','none');
        
       
      $.ajax({
         url:basepath+'profile/editprofilepicture',
         method:'post',
         data: new FormData(this),
         contentType: false,
         cache: false,
         processData: false,
         success:function(src){
          if(src==""){
           alert('Please Select Image File');
           $('#userimage').attr('src',esrc);
            $('#loader').addClass('hide');
          //  $('#imgupdate').prop("disabled",false);
          }else{
            
            $('#userimage').attr('src',src);
            $('#loader').addClass('hide');
           // $('#imgupdate').prop("disabled",false);
            
          }
         }

      })
	  
	})
  })
  function check_edit_profile(){
      $('#eci_event_name_err').html('');
      $('#eci_event_contact_err').html('');
      $('#eci_event_gender_err').html('');
      $('#eci_event_add_err').html('');
    var name=$('#eci_admin_name').val();
    var contact=$('#eci_admin_contact').val();
    var address=$('#eci_admin_address').val();

      if($.trim(name)==""){
        $('#eci_event_name_err').html('Username is required');
    return false;
  }
  if($.trim(contact)==""){
        $('#eci_event_contact_err').html('<b>Contact number is required<b>');
    return false;
  }
  if(isNaN($.trim(contact))){
    $('#eci_event_contact_err').html('<b>Enter only numeric value</b>');
          return false;
  }
  if ($("#eci_admin_gender:checked").length == 0){
                $('#eci_event_gender_err').html('<b>Gender is required</b>');
                return false;
            }
  
    if($.trim(address)==""){
        $('#eci_event_add_err').html('<b>Address is required</b>');
    return false;
  }
  
}

function showAmt_popup(arr){
	var basepath = $('#basepath').val();
	$('tbody#row').html('');
	 $.ajax({
         url: basepath+'eventadmin/get_amount',
         method:'post',
         data: {'eventid': arr} ,
         success:function(resp){
			 if(resp){
				$('tbody#row').html(resp); 
			 }
		 console.log(resp);
		 },
		 error:function(){}
	 });
	$('#amount_popup').modal('show');
	
	 
	
}
})(jQuery);

