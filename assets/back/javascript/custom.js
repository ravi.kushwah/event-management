/*
Copyright (c) 2014 Himanshu Softtech.
------------------------------------------------------------------
[Master Javascript]

Template Name: Event Management System - Perfect Day
Version: 1.0
Author: Kamleshyadav
Website: http://himanshusofttech.com/
Purchase: http://codecanyon.net/user/kamleshyadav
-------------------------------------------------------------------*/

(function($) {
	'use strict';

	
    $( document ).ready( function () {
     // time picker
     $("#eci_event_time_h").timepicki({defaultTime: 'value'});
  
var basepath = $('#basepath').val();
	$('.loading_img,.setimage_sec').hide();
		
		$('#datatable_tbl').dataTable();
		
	$('#eci_event_date').datepicker({ minDate: 0});
	$('#eci_coupon_expir_date').datepicker({ minDate: 0});
		//sidebar dropdown
		$('.eci_page_sidebar ul li').click(function(){
			$(this).children('ul').slideToggle().show();
			$(this).addClass('active');
			});

	/******************* set images for the events **********/
	
	$('#event_selection').change(function(){
		var selectedevent = $(this).val();
		
		$('#displaysec').html('');
		$('.setimage_sec').hide();
			
		if(selectedevent != '0')
		{
			$('.loading_img').show();
			
			$.post(basepath+"eventadmin/get_images/",
			{
				'selectedevent' 		: selectedevent,
				'type' 		: 'evnt',
			},
			  function(data){
				$('#displaysec').html(data);
				$('.setimage_sec').show();
				$('.loading_img').hide();
				
			  });
			 
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please, Choose An Event.',
				type     : 'error',
				stayTime:  3000, 
			});
			return false;
		}
	});
	
	/******************* set background images for the section **********/
	
	$('#section_selection').change(function(){
		var selectedsection = $(this).val();
		
		$('#displaysec').html('');
		$('.setimage_sec').hide();
			
		if(selectedsection != '0')
		{
			$('.loading_img').show();
			
			$.post(basepath+"eventadmin/get_images/",
			{
				'selectedsection' 		: selectedsection,
				'type' 		: 'bg',
			},
			  function(data){
				$('#displaysec').html(data);
				$('.setimage_sec').show();
				$('.loading_img').hide();
				
			  });
			 
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please, Choose An Event.',
				type     : 'error',
				stayTime:  3000, 
			});
			return false;
		}
	});
	
	/******************* booking details **********/
	
	$('#booking_event_selection').change(function(){
		var selectedevent = $(this).val();
		
		$('#displaysec').html('');
			
		if(selectedevent != '0')
		{
			$('.loading_img').show();
			
			$.post(basepath+"eventadmin/get_booking_details/",
			{
				'selectedevent' 		: selectedevent,
			},
			  function(data){
			  
				$('#displaysec').html(data);
				$('.loading_img').hide();
				
			  });
			 
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please, Choose An Event.',
				type     : 'error',
				stayTime:  3000, 
			});
			return false;
		}
	});
	
	/************* booking form manually *******************/
	$('#eci_book_event_list').change(function(){
		var selectedevent = $(this).val();
		$('#eci_book_cust_no_ticket').val('');
		$('#eci_book_amount').val('');
		$('.persons_name').html('');
		$('#cost_of_a_ticket').val('');
		$('#eci_seats_left').val('');
		$('#eci_book_no_ticket').text('');
		$('#booked_ticket_cat').val('');
		$('#ticket_cate_list').html('');
		if(selectedevent != '0')
		{
			$.post(basepath+"eventadmin/get_booking_count/",
			{
				'eventid' 		: selectedevent,
			},
			  function(data){
				var eventDetail = $.parseJSON(data);
				if(eventDetail[0].eci_event_list_max_user == 0 &&  eventDetail[0].eci_event_list_noofcat !=''){
					$('#ticket_cate_list').append(eventDetail['tableData']);
					$('#noofcat').val(eventDetail[0].eci_event_list_noofcat);
					$('#eci_book_cust_no_ticket').attr('readonly',true);
					 
					$('#ticket_cate_list .noofseat_count').keyup(function(){
						calc_with_cat_tickets(eventDetail[0].eci_event_list_noofcat);
					})
					var html_txt ='<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p><label for="eci_event_name">Number of Tickets</label><input type="text" class="form-control noofseat_count" placeholder="Number of tickets" name="eci_book_cust_no_ticket" id="eci_book_cust_no_ticket" value="" readonly/>';
					$('#count_total_seat').html(html_txt);
					
				}else{ 
					var html_txt ='<p class="eci_error" id="eci_book_cust_no_ticket_err"> </p><label for="eci_event_name">Number of Tickets [<span class="eci_req_star" id="eci_book_no_ticket">   </span> Ticket(s) Left ]</label><input type="text" class="form-control noofseat_count" placeholder="Number of tickets" name="eci_book_cust_no_ticket" id="eci_book_cust_no_ticket" value="" />';
					 
					$('#count_total_seat').html(html_txt);
					$("#noofcat").val(0);
					$('#eci_book_cust_no_ticket').attr('readonly',false);
					var em_seats_left = eventDetail[0].eci_event_list_max_user - eventDetail[1].tot_count;
					var cost_of_a_ticket = eventDetail[0].eci_event_list_amount;
					$('#cost_of_a_ticket').val(cost_of_a_ticket);
					$('#eci_seats_left').val(em_seats_left);
					$('#eci_book_no_ticket').text(em_seats_left);
					if(em_seats_left == 0)
					{
						$('#check_add_booking_manual_form').hide();
					}
					else
					{
						$('#check_add_booking_manual_form').show();
					}
					
					$('#eci_book_cust_no_ticket').keyup(function(){
						calc_without_cat_tickets();
					})
				}
			  });
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please, Choose An Event.',
				type     : 'error',
				stayTime:  3000, 
			});
			$('#check_add_booking_manual_form').hide();
			return false;
		}
	});
	
	$('#eci_img').on("change",function () {
		$('#eci_img_err').html('');
			var fileExtension = ['jpeg', 'jpg','png'];
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$('#eci_img_err').html("Only '.jpeg','.jpg' ,.png formats are allowed.");
				$('#eci_img').val('');
			}
   }) 
	
	//******************** virendra Js Start*********************************

	
	/* $('a#eventEdit').on('click',function(){
		var eventid = $(this).attr('data-valid').val();
		alert(eventid);
		
		var base_url = $('#baseUrl').val();
		alert(base_url);
		$.ajax({
			url:base_url+'eventadmin/event_category',
			type:'post',
			datatype:'json',
			data: 'eventNo='+eventid,
		   success:function(resp){
			   resp = $.parseJSON(resp);
			   console.log(resp);
			    $('.addTicketCat').html(resp);
			   $('#nofcat').val(noofcat);
		   }
		});
		$('#ticketCat_popup').modal('hide');
		
	});  */
	//******************** virendra Js End***********************************
	
	
 });
	/********************** add services start ******************/
	var count=1;
	function plus_service(id)
	{
		count++;
		var newid=id+1;
		$('#plus_service_'+id).hide();
		$('.eci_service_'+id).html('<div class="eci_service"><p>Service'+newid+'</p> <input class="service_name form-control" placeholder="Service Name" name="serv_name_'+id+'"> <input class="service_description form-control" placeholder="Service Description"  name="serv_desc_'+id+'"><a onclick="plus_service('+newid+')" id="plus_service_'+newid+'"  class="plus_service_style"><i class="fa fa-plus-circle fa-lg"></i></a></div><div class="eci_service_'+newid+'"></div>');
		$('#eci_no_serv').val(count);
	}
	/********************** add services ends ******************/
	/**************** add event form validation starts ****************/



	function check_add_event_form()
	{
		var eci_event_name=$('#eci_event_name').val();
		var eci_event_date=$('#eci_event_date').val();
		var eci_yt_url=$('#eci_yt_url').val();
		if($.trim(eci_yt_url) == "")
			eci_yt_url = 'http://youtube.com';
		var eci_event_time_h=$('#eci_event_time_h').val();
		var eci_no_service=$('#eci_no_service').val();
		var eci_max_users=$('#eci_max_users').val();
		var eci_event_desc=$('#eci_event_desc').val();
		var eci_booking_amount=$('#eci_booking_amount').val();

		if($.trim(eci_yt_url) != "" && $.trim(eci_event_name) != "" && $.trim(eci_event_date) != "" && $.trim(eci_event_time_h) != "" && $.trim(eci_no_service) != "" && $.trim(eci_max_users) != "" && $.trim(eci_event_desc) != "" && $.trim(eci_booking_amount) != "")
		{
			url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


			if($.isNumeric( eci_no_service ) && $.isNumeric( eci_max_users ))
			{
				if(url_validate.test(eci_yt_url)){
					return true;
				}
				else
				{
					$().toastmessage('showToast', {
						text     : 'URL should be correct.',
						type     : 'error',
						stayTime:         3000,
					});
					return false;
				}
			}
			else
			{
				$().toastmessage('showToast', {
					text     : 'Max Users and Number of Services Should be Numeric.',
					type     : 'error',
					stayTime:         5000,
				});
				return false;
			}



		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please fill * fields.',
				type     : 'error',
				stayTime:         3000,
			});
			return false;
		}
	}

	/******************* add event form validation ends ******************/
	/**************** contact setting form validation starts ****************/

	function check_contact_setting()
	{
		var eci_contact_email=$('#eci_contact_email').val();
		var eci_contact_phone=$('#eci_contact_phone').val();
		var eci_contact_address=$('#eci_contact_address').val();

		if($.trim(eci_contact_email) != "" && $.trim(eci_contact_phone) != "" && $.trim(eci_contact_address) != "")
		{
			var atpos = eci_contact_email.indexOf("@");
			var dotpos = eci_contact_email.lastIndexOf(".");
			if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=eci_contact_email.length) {

				$().toastmessage('showToast', {
					text     : 'Email should be correct.',
					type     : 'error',
					stayTime:         3000,
				});
				return false;
			}
			else
			{
				if($.isNumeric( eci_contact_phone )){
					var enter_email_val = $("input[name=send_contact_query]:checked").val();

					if(enter_email_val == 'no')
					{
						var eci_contact_new_email=$('#eci_contact_new_email').val();

						var atpos = eci_contact_new_email.indexOf("@");
						var dotpos = eci_contact_new_email.lastIndexOf(".");
						if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=eci_contact_new_email.length) {

							$().toastmessage('showToast', {
								text     : 'Email should be correct.',
								type     : 'error',
								stayTime:         3000,
							});
							return false;

						}
						else
						{
							return true;
						}

					}
					else
						return true;
				}
				else
				{
					$().toastmessage('showToast', {
						text     : 'Phone Number should be Valid.',
						type     : 'error',
						stayTime:         3000,
					});
					return false;
				}

			}
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please fill all fields.',
				type     : 'error',
				stayTime:         3000,
			});
			return false;
		}
	}

	var enter_email_count=0;
	function enter_new_email(){
		var enter_email_count = $("input[name=send_contact_query]:checked").val();
		if(enter_email_count == 'no')
			$('.eci_new_email').show();
		else
			$('.eci_new_email').hide();
	}

	/******************* add event form validation ends ******************/
	/**************** add service form validation starts ****************/

	function check_add_service_form()
	{
		$('#eci_service_name_err').html();
		$('#eci_service_desc_err').html();
		var eci_service_name=$('#eci_service_name').val();
		var eci_service_desc=$('#eci_service_desc').val();

		if($.trim(eci_service_desc) != "" && $.trim(eci_service_name) != "" )
		{
			return true;
		}
		else
		{
			if($.trim(eci_service_name) == ""){
				$('#eci_service_name_err').html('Service name is required');
				$('#eci_service_name').focus();
				return false;
			}
			if($.trim(eci_service_desc) == ""){
				$('#eci_service_desc_err').html('Service description is required');
				$('#eci_service_desc').focus();
				return false;
			}
		}
	}

	
	/************************** payment form validation ******************/

	function check_payment_form()
	{

		var payment_method = $("input[name=payment_method]:checked").val();
		if(payment_method == 'paypal'){
			var eci_payment_email=$('#eci_payment_email').val();
			var eci_payment_ccode=$('#eci_payment_ccode').val();
			var eci_payment_msg=$('#eci_payment_msg').val();
			var eci_payment_detail_em_temp=$('#eci_payment_detail_em_temp').val();

			if($.trim(eci_payment_msg) != "" && $.trim(eci_payment_ccode) != ""  && $.trim(eci_payment_email) != ""  && $.trim(eci_payment_detail_em_temp) != "" )
			{
				return true;
			}
			else
			{
				$().toastmessage('showToast', {
					text     : 'Please fill * fields.',
					type     : 'error',
					stayTime:         3000,
				});
				return false;
			}
		}
		else{
			var eci_payment_msg=$('#eci_payment_msg').val();
			var show_invoice_curency_code=$('#show_invoice_curency_code').val();
			var eci_payment_detail_em_temp=$('#eci_payment_detail_em_temp').val();
			if($.trim(eci_payment_msg) != "" && $.trim(show_invoice_curency_code) != "" && $.trim(eci_payment_detail_em_temp) != "")
			{
				return true;
			}
			else
			{
				$().toastmessage('showToast', {
					text     : 'Please fill * fields.',
					type     : 'error',
					stayTime:         3000,
				});
				return false;
			}
		}

	}


	function choose_payment_method(){
		var payment_method = $("input[name=payment_method]:checked").val();
		if(payment_method == 'paypal'){
			$('#paypal_section').show();
			$('#invoice_section').hide();
		}
		else{
			$('#paypal_section').hide();
			$('#invoice_section').show();
		}
	}
	/*********** add booking manually form *********************/

	function check_add_booking_manual_form_fun(){


		$('#eci_event_name_err').html('');
		$('#eci_book_cust_name_err').html('');
		$('#eci_book_cust_email_err').html('');
		$('#eci_book_cust_no_ticket_err').html('');
		$('#eci_book_payment_err').html('');
		$('#em_person_err').html("");

		var eci_book_event_list=$('#eci_book_event_list').val();
		var eci_book_cust_name=$('#eci_book_cust_name').val();
		var eci_book_cust_email=$('#eci_book_cust_email').val();
		var eci_book_cust_cntct=$('#eci_book_cust_cntct').val();
		var eci_book_cust_add=$('#eci_book_cust_add').val();
		var eci_book_cust_no_ticket=$('#eci_book_cust_no_ticket').val();
		var eci_book_cust_payment_sts=$('#eci_book_cust_payment_sts').val();
		var id_of_booking=$('#id_of_booking').val();
		var em_seats_left=$('#eci_seats_left').val();
		var noofcate = $('#noofcat').val();

		if(id_of_booking != '0')
		{
			var earlierTcktCount=$('#earlierTcktCount').val();
			var total_seats = parseInt(em_seats_left) + parseInt(earlierTcktCount);
			em_seats_left = total_seats;
		}


		var basepath = $('#basepath').val();

		if(eci_book_event_list != '0'){

			if(!isNaN(eci_book_cust_no_ticket) && eci_book_cust_no_ticket != '0')
			{
				if(noofcate == 0)
				{

					if(em_seats_left == 0)
					{
						$('#check_add_booking_manual_form').hide();
						$('#eci_book_cust_no_ticket_err').html('No tickets left.');
						$('#eci_book_cust_no_ticket').focus();
						return false;
					}
					else if((em_seats_left-eci_book_cust_no_ticket) < 0)
					{

						$('#eci_book_cust_no_ticket_err').html('You can not book more than the Tickets left');
						$('#eci_book_cust_no_ticket').focus();
						return false;
					}
					else
					{

						if($.trim(eci_book_cust_name) == ""){
							$('#eci_book_cust_name_err').html('Please enter customer name');
							$('#eci_book_cust_name').focus();
							return false;
						}
						if($.trim(eci_book_cust_email) == ""){
							$('#eci_book_cust_email_err').html('Please enter customer email');
							$('#eci_book_cust_email').focus();
							return false;
						}
						if($.trim(eci_book_cust_no_ticket) == ""){
							$('#eci_book_cust_no_ticket_err').html('Please enter ticket');
							$('#eci_book_cust_no_ticket').focus();
							return false;
						}
						var personsnames= document.getElementsByName("person[]");
						var count=personsnames.length;
						for(var s=0;s<count;s++){
							if(personsnames[s].value==""){
								$('#em_person_err').html("All field are required");
								return false;
							}
						}
						if($.trim(eci_book_cust_name) != "" && $.trim(eci_book_cust_email) != ""  && $.trim(eci_book_cust_no_ticket) != "" && eci_book_cust_payment_sts != 0)
						{
							return true;

						}else{
							$('#eci_book_payment_err').html('Please select payment status');
							$('#eci_book_cust_payment_sts').focus();
							return false;
						}
					}
				}else{
					if($.trim(eci_book_cust_name) == ""){
						$('#eci_book_cust_name_err').html('Please enter customer name');
						$('#eci_book_cust_name').focus();
						return false;
					}
					if($.trim(eci_book_cust_email) == ""){
						$('#eci_book_cust_email_err').html('Please enter customer email');
						$('#eci_book_cust_email').focus();
						return false;
					}
					if($.trim(eci_book_cust_no_ticket) == ""){
						$('#eci_book_cust_no_ticket_err').html('Please enter ticket');
						$('#ticket_cate_list').focus();
						return false;
					}
					var personsnames= document.getElementsByName("person[]");
					var count=personsnames.length;
					for(var s=0;s<count;s++){
						if(personsnames[s].value==""){
							$('#em_person_err').html("All field are required");
							return false;
						}
					}
					if($.trim(eci_book_cust_name) != "" && $.trim(eci_book_cust_email) != ""  && $.trim(eci_book_cust_no_ticket) != "" && eci_book_cust_payment_sts != 0)
					{
						return true;

					}else{
						$('#eci_book_payment_err').html('Please select payment status');
						$('#eci_book_cust_payment_sts').focus();
						return false;
					}
				}
			}
			else
			{

				$('#eci_book_cust_no_ticket_err').html('Number of Tickets should be a Numeric and can not be 0');
				$('#eci_book_cust_no_ticket').focus();
				return false;
			}
		}
		else
		{
			$('#eci_event_name_err').html('Please choose an event');
			$('#eci_book_event_list').focus();
			return false;
		}
	}

	/***************change payment status****************/

	function change_payment_status(id)
	{
		var basepath = $('#basepath').val();
		var newstatus = $('#payment_status_select_'+id).val();
		$.post(basepath+"eventadmin/change_payment_status/",
			{
				'pay_sno' 		: id,
				'newstatus' 		: newstatus,
			},
			function(data){
				$().toastmessage('showToast', {
					text     : 'Payment Status Updated Successfully.',
					type     : 'success',
					stayTime:  3000,
				});
			});

	}
	
	function check_add_booking_manual_form_fun1(){
		var eci_book_event_list=$('#eci_book_event_list').val();
		var eci_book_cust_name=$('#eci_book_cust_name').val();
		var eci_book_cust_email=$('#eci_book_cust_email').val();
		var eci_book_cust_cntct=$('#eci_book_cust_cntct').val();
		var eci_book_cust_add=$('#eci_book_cust_add').val();
		var eci_book_cust_no_ticket=$('#eci_book_cust_no_ticket').val();
		var eci_book_cust_payment_sts=$('#eci_book_cust_payment_sts').val();
		var id_of_booking=$('#id_of_booking').val();
		var em_seats_left=$('#eci_seats_left').val();

		if(id_of_booking != '0')
		{
			var earlierTcktCount=$('#earlierTcktCount').val();
			var total_seats = parseInt(em_seats_left) + parseInt(earlierTcktCount);
			em_seats_left = total_seats;
		}


		var basepath = $('#basepath').val();

		if(eci_book_event_list != '0'){

			if(!isNaN(eci_book_cust_no_ticket) && eci_book_cust_no_ticket != '0')
			{

				if(em_seats_left == 0)
				{
					$('#check_add_booking_manual_form').hide();
					$().toastmessage('showToast', {
						text     : 'No Tickets Left.',
						type     : 'error',
						stayTime:         3000,
					});
					return false;

				}
				else if((em_seats_left-eci_book_cust_no_ticket) < 0)
				{
					$().toastmessage('showToast', {
						text     : 'You can not book more than the Tickets left.',
						type     : 'error',
						stayTime:         3000,
					});
					return false;
				}
				else
				{
					if($.trim(eci_book_cust_name) != "" && $.trim(eci_book_cust_email) != ""  && $.trim(eci_book_cust_no_ticket) != "" && eci_book_cust_payment_sts != "0")
					{
						return true;

					}
					else
					{
						$().toastmessage('showToast', {
							text     : 'Please fill * fields.',
							type     : 'error',
							stayTime:         3000,
						});
						return false;
					}
				}

			}
			else
			{
				$().toastmessage('showToast', {
					text     : 'Number of Tickets should be a Numeric and can not be 0.',
					type     : 'error',
					stayTime:  3000,
				});
				return false;
			}
		}
		else
		{
			$().toastmessage('showToast', {
				text     : 'Please, Choose An Event.',
				type     : 'error',
				stayTime:  3000,
			});
			return false;
		}
	}

	/**************************** can not delete featured events *************/
	function show_no_dlt_msg(){
		$().toastmessage('showToast', {
			text     : "Sorry, You can't delete the FEATURED event.",
			type     : 'error',
			stayTime:  3000,
		});
	}

	$(document).ready(function(){
		$('#eci_book_cust_no_ticket').keyup(function(){
			calc_without_cat_tickets();
		})

		$('#ticket_cate_list .noofseat_count').keyup(function(){
			calc_with_cat_tickets();
		})
	})
//******************************* virendra js Start ****************************
	function calc_without_cat_tickets(){
		var noofcat = $('#noofcat').val();
		if(noofcat == 0){
			var amount = 0;
			$('.persons_name').html('');
			$('#eci_book_cust_no_ticket_err').html('');
			$('#booking_coupon_detail_span').html('');
			var ticketprice=$('#cost_of_a_ticket').val();
			var total_seats = $('#eci_seats_left').val();
			var tickecount = (isNaN(tickecount = parseInt($('#eci_book_cust_no_ticket').val(), 10)) ? 0 : tickecount);
			if(total_seats > tickecount ){
				if($.isNumeric(ticketprice)){
					amount=ticketprice*tickecount;
				}
				$('#eci_book_amount').val(amount);
				var booking_person="";
				var p=0;
				for (var i = 0; i <tickecount ; i++){
					p++;
					booking_person+='<p class="eci_error" id="em_person_err"> </p><input type="text" required="true" class="form-control" name="person[]" placeholder="'+p+' (Person Name)">';
				}
				$('.persons_name').html(booking_person);
			}else{
				$('.persons_name').html('');
				$('#eci_book_amount').val('');
				$('#eci_book_cust_no_ticket_err').html('You can not book more than the Tickets left');
				$('#eci_book_cust_no_ticket').focus();
			}
		}
	}

	function calc_with_cat_tickets(){
		var tickecount =0;
		var seat = '';
		var noofcat = $('#noofcat').val();
		$('#ticket_cate_list .noofseat_count').each(function() {
			var seat =  (isNaN(seat = parseInt($(this).val(), 10)) ? 0 : seat);
			tickecount +=  + seat;
		});
		var catName='';
		var catLeftSeat=0;
		var amount=0;
		var booked_seat =[];
		var arr_index = 0;
		for(var i=1;i<=noofcat;i++){
			$('.persons_name').html('');
			$('#booking_coupon_detail_span').html('');
			catName = $('.cat_name'+i).text();
			catLeftSeat = $('.cat_left_seat'+i).text();
			var book_tickecount = (isNaN(book_tickecount = parseInt($('#noofseats_'+i).val(), 10)) ? 0 : book_tickecount);
			var ticketprice = $('.cat_tic_price'+i).text();
			if($.isNumeric(ticketprice)){
				amount += + ticketprice*book_tickecount;
			}
			booked_seat[arr_index] = catName+','+book_tickecount+','+ticketprice;
			arr_index++;

			if(book_tickecount > catLeftSeat)
			{
				$().toastmessage('showToast', {
					text     : "Sorry, You can't book more than the number of seats left.",
					type     : 'error',
					stayTime:  3000,
				});
				$('#noofseats_'+i).focus();
				$('#noofseats_'+i).val('');
				$('#ticket_cate_list .noofseat_count').keyup();
				return false;
			}
		}

		$('#eci_book_cust_no_ticket').val(tickecount);
		$('#eci_book_amount').val(amount);
		$('#booked_ticket_cat').val(JSON.stringify(booked_seat));
		var booking_person="";
		var p=0;
		for (var i = 0; i <tickecount ; i++) {
			p++;
			booking_person+='<p class="eci_error" id="em_person_err"> </p><input type="text" required="true" class="form-control" name="person[]" placeholder="'+p+' (Person Name)">';
		}
		$('.persons_name').html(booking_person);
	}
})(jQuery);

/******************* add event form validation ends ******************/
	function updateeventssetting_fun(eventid,type){
	   // console.log(eventid);
		var basepath = $('#basepath').val();
		if(type == 'upcom') {
			$('.hidecheck_'+eventid).hide();
			$('#upcoming_'+eventid).show();

			$.post(basepath+"eventadmin/updateeventssetting/",
				{
					'eventid' 		: eventid,
					'type' 		: type,
				},
				function(data){

					$('.hidecheck_'+eventid).show();
					$('#upcoming_'+eventid).hide();

				});
		}
		else if(type == 'book')
		{
			$('.hidebookingcheck_'+eventid).hide();
			$('#booking_'+eventid).show();

			$.post(basepath+"eventadmin/updateeventssetting/",
				{
					'eventid' 		: eventid,
					'type' 		: type,
				},
				function(data){
					if(data == '1')
						$('#booking_text_'+eventid).text('ON');
					else
						$('#booking_text_'+eventid).text('OFF');

					$('.hidebookingcheck_'+eventid).show();
					$('#booking_'+eventid).hide();

				});
		}
		else
		{
			$('.hideradio_'+eventid).hide();
			$('#feature_'+eventid).show();

			$.post(basepath+"eventadmin/updateeventssetting/",
				{
					'eventid' 		: eventid,
					'type' 		: type,
				},
				function(data){

					$('.hideradio_'+eventid).show();
					$('#feature_'+eventid).hide();

				});
		}
	}






//******************************* virendra js End *******************************





















